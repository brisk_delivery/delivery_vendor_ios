//
//  Constants.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/2/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

let BaseUrl = "http://briskgroup.net/eats/api/"
    //"http://briskgroup.net/eats/api/"
//"http://brisk-drive.com/brisk_delivery/api/"
//"http://briskgroup.net/eats/api/"
let GoogleMapsKey = "AIzaSyATtPpYClwKSQ8rNrfA9qnBjyFtyrORa7c"
let aboutUsUrl = "\(BaseUrl)pages/about-us"
let termsUrl = "\(BaseUrl)pages/terms-and-conditions"

//MARK: - UserDefaults
let UserCredentials = "UserCredentials"
let noInternetConnection = "There's no internet connection"
let someThingWentWrong = "Something went wrong"
let addShopMessage = "Please add shop first"
let selectCityMessage = "Please select city first"
let updateProfileMessage = "Profile updated successfully"
let saveButtonTitle = "Save"
let saveStoreMessage = "Store updated successfully"
let productDeletedMessage = "Product is deleted successfully"
let productCreateMessage = "Product is created successfully"
let productUpdateMessage = "Product is updated successfully"
let productSizeAddedMessage = "Size is added successfully"
let productSizeUpdatedMessage = "Size is updated successfully"
let productAdditionAddedMessage = "Addition is added successfully"
let productAdditionUpdatedMessage = "Addition is updated successfully"
let orderStatusUpdatedMessage = "Order status is updated successfully"
let sessionExpiredMessage = "Session expired"

