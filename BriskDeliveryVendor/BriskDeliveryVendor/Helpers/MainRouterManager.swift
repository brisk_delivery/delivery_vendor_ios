//
//  MainRouterManager.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/1/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit


class MainRouterManager {
    static func pushViewController(_ viewController: String, storyBoard: String, navigationController: UINavigationController) {
        let viewController = UIStoryboard(name: storyBoard, bundle: nil).instantiateViewController(withIdentifier: viewController)
        navigationController.pushViewController(viewController, animated: true)
    }
    
    static func openAsRootViewController(stroyboardName: String, viewController: String) {
        let storyboard: UIStoryboard = UIStoryboard(name: stroyboardName, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: viewController)
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.window?.rootViewController = controller
            appDelegate.window?.makeKeyAndVisible()
        }
    }
    
    static func launchApplication(openLanguage: Bool) {
        if Global.user != nil {
            // open Home
            MainRouterManager.openAsRootViewController(stroyboardName: "Main", viewController: MainViewController.identifier)
        } else {
            
            if openLanguage {
                MainRouterManager.openAsRootViewController(stroyboardName: "Signup", viewController: "SignupNavigation")
            } else {
                let storyboard: UIStoryboard = UIStoryboard(name: "Signup", bundle: nil)
                if let navigatioController = storyboard.instantiateViewController(withIdentifier: "SignupNavigation") as? UINavigationController {
                    let controller = storyboard.instantiateViewController(withIdentifier: wellcomeViewController.identifier)
                    navigatioController.pushViewController(controller, animated: true)
                    if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                        appDelegate.window?.rootViewController = navigatioController
                        appDelegate.window?.makeKeyAndVisible()
                    }
                }
                
                
            }
            
            
            
        }
    }
    
    static func openOrders(with OrderId: Int) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if let controller = storyboard.instantiateViewController(withIdentifier: MainViewController.identifier) as? MainViewController {
            controller.orderId = OrderId
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                appDelegate.window?.rootViewController = controller
                appDelegate.window?.makeKeyAndVisible()
            }
        }
    }
    
}
