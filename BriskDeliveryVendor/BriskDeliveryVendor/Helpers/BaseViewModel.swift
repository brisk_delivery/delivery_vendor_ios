//
//  BaseViewModel.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/2/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation
import CFAlertViewController
import Localize_Swift

class BaseViewModel {
    
    var isLoading: Dynamic<Bool> = Dynamic(false)
    private var router: Router!
    
    init(isLoading: Bool) {
        self.isLoading.value = isLoading
    }
    
    init(router: Router, isLoading: Bool) {
        self.isLoading.value = isLoading
        self.router = router
    }
    
    func startLoading() {
        isLoading.value = true
    }
    
    func stopLoading() {
        isLoading.value = false
    }
    
    func showAlertMessage(title: String, message: String, ActionButtons: [(title: String, style: CFAlertAction.CFAlertActionStyle, alignment: CFAlertAction.CFAlertActionAlignment, bgColor: UIColor, textColor: UIColor, handler: (_ action: CFAlertAction) -> ())]) {
        router.showAlertMessage(title: title, message: message, ActionButtons: ActionButtons)
    }
    
    func generalErrorMessage(error: ErrorCode) {
        var message = "someThingWentWrong".localized()
        if error == .NoInternet {
            message = "noInternetConnection".localized()
        }
        self.showAlertMessage(title: "", message: message, ActionButtons:[(title: "ok".localized() ,style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
    }
    
    func checkForShopId() -> Int? {
        if let shopId = Global.shopId {
            return shopId
        }
        self.showAlertMessage(title: "", message: "addShopMessage".localized(), ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
        return nil
    }
    func checkForShopCategoryId() -> Int? {
        if let shopCategoryId = Global.shopCategoryId {
            return shopCategoryId
        }
        self.showAlertMessage(title: "", message: "addShopMessage".localized(), ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
        return nil
    }
    func checkPermission()-> Bool {
        guard let user = Global.user, let type = user.userType, type == "employee" else {
           return true
        }
        self.showAlertMessage(title: "", message: "noPermission".localized(), ActionButtons: [(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
        return false
    }
    
}
