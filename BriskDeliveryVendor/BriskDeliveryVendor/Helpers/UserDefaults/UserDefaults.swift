//
//  UserDefaults.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/11/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

enum UserDefaultsKeys: String {
    case accesstoken = "access_token"
    case countryCode = "country_code"
    case user = "user"
    case shopId = "shopId"
    case shopCategoryId = "shopCategoryId"
    case shop = "shop"
    case languageId = "languageId"
    case pushToken = "pushToken"
    case shopStatus = "shopStatus"
}

class UserDefaultsHelpers {
    
    static func set(value: Any, key: String) {
        
        if let defaults = UserDefaults(suiteName: "group.DeliveryShop") {
            defaults.set(value, forKey: key)
            defaults.synchronize()
            
        }
    }
    
    static func set<T>(object: T, key: String) where T: Encodable {
        
        if let defaults = UserDefaults(suiteName: "group.DeliveryShop") {
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(object) {
                defaults.set(encoded, forKey: key)
                defaults.synchronize()
            }
        }
    }
    
    static func remove(key: String) {
        if let defaults = UserDefaults(suiteName: "group.DeliveryShop") {
            defaults.removeObject(forKey: key)
            defaults.synchronize()
            
        }
    }
    
    static func get(key: String) -> Any? {
        if validate(key: key) {
            return UserDefaults(suiteName: "group.DeliveryShop")!.value(forKey: key)!
        }
        return nil
    }
    
    static func get<T>(object: T.Type, key: String) -> T? where T: Decodable {
        if validate(key: key) {
            let decoder = JSONDecoder()
            if let objectData = UserDefaults(suiteName: "group.DeliveryShop")!.data(forKey: key),
                let value = try? decoder.decode(object.self, from: objectData) {
                return value
            }
        }
        return nil
    }
    
    static func validate(key: String) -> Bool {
        if let defaults = UserDefaults(suiteName: "group.DeliveryShop") {
            if defaults.value(forKey: key) != nil {
                return true
            }
            return false
            
        }
        return false
    }
}
