//
//  LoginModel.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/4/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class Global {
    
    static var user: User! {
        get { return UserDefaultsHelpers.get(object: User.self, key: UserDefaultsKeys.user.rawValue) }
        set { UserDefaultsHelpers.set(object: newValue, key: UserDefaultsKeys.user.rawValue) }
    }
    
    static var countryCode: String! {
        get { return UserDefaultsHelpers.get(key: UserDefaultsKeys.countryCode.rawValue) as? String }
        set { UserDefaultsHelpers.set(value: newValue, key: UserDefaultsKeys.countryCode.rawValue) }
    }
    
    static var accesstoken: String! {
        get { return UserDefaultsHelpers.get(key: UserDefaultsKeys.accesstoken.rawValue) as? String ?? ""  }
        set { UserDefaultsHelpers.set(value: newValue, key: UserDefaultsKeys.accesstoken.rawValue) }
    }
    
    static var shopId: Int! {
        get { return UserDefaultsHelpers.get(key: UserDefaultsKeys.shopId.rawValue) as? Int }
        set { UserDefaultsHelpers.set(value: newValue, key: UserDefaultsKeys.shopId.rawValue) }
    }
    static var shopCategoryId: Int! {
        get { return UserDefaultsHelpers.get(key: UserDefaultsKeys.shopCategoryId.rawValue) as? Int }
        set { UserDefaultsHelpers.set(value: newValue, key: UserDefaultsKeys.shopCategoryId.rawValue) }
    }
    static var shop: Shop! {
        get { return UserDefaultsHelpers.get(object: Shop.self, key: UserDefaultsKeys.shop.rawValue) }
        set { UserDefaultsHelpers.set(object: newValue, key: UserDefaultsKeys.shop.rawValue) }
    }
    
    static func remove () { // we call this method when the user logs out
        UserDefaultsHelpers.remove(key: UserDefaultsKeys.user.rawValue)
        UserDefaultsHelpers.remove(key: UserDefaultsKeys.countryCode.rawValue)
        UserDefaultsHelpers.remove(key: UserDefaultsKeys.accesstoken.rawValue)
        UserDefaultsHelpers.remove(key: UserDefaultsKeys.shopId.rawValue)
        UserDefaultsHelpers.remove(key: UserDefaultsKeys.shop.rawValue)
    }
    
    static var languageId: Int! {
        get { return UserDefaultsHelpers.get(key: UserDefaultsKeys.languageId.rawValue) as? Int }
        set { UserDefaultsHelpers.set(value: newValue, key: UserDefaultsKeys.languageId.rawValue) }
    }
    
    static var pushToken: String! {
        get { return UserDefaultsHelpers.get(key: UserDefaultsKeys.pushToken.rawValue) as? String ?? ""  }
        set { UserDefaultsHelpers.set(value: newValue, key: UserDefaultsKeys.pushToken.rawValue) }
    }
    static var shopStatus = 0
    static var DeliveryStatus = 0

//    static var shopStatus: Int! {
//        get { return UserDefaultsHelpers.get(key: UserDefaultsKeys.shopStatus.rawValue) as? Int ?? 0  }
//        set { UserDefaultsHelpers.set(value: shopStatus, key: UserDefaultsKeys.shopStatus.rawValue) }
//    }
    
}


