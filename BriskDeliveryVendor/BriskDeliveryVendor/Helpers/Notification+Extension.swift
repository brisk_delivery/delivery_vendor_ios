//
//  Notification+Extension.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/25/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation
extension Notification.Name {
    static let shopAdded = Notification.Name("ShopAdded")
    static let refreshProducts = Notification.Name("RefreshProducts")
    static let languageChanged = Notification.Name("LanguageChanged")
    static let addShopPopup = Notification.Name("addShopPopup")
}
