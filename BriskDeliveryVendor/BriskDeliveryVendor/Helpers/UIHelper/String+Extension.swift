//
//  String+Extension.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/2/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

extension String {
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self) ? true : false
    }
    
    func formatAsAttributedString(fontSize: CGFloat, fontColor: UIColor, boldStringArray: [(string: String, fontSize: CGFloat, fontColor: UIColor)]) -> NSMutableAttributedString {
        let attrString = NSMutableAttributedString(string: self, attributes: [NSAttributedString.Key.font: UIFont.init(name: "Roboto-Bold", size: fontSize)!, NSAttributedString.Key.foregroundColor: fontColor])
        boldStringArray.forEach { str in
            let boldStringRange = (self as NSString).range(of: str.string)
            attrString.setAttributes([NSAttributedString.Key.font: UIFont.init(name: "Roboto-Bold", size: str.fontSize)!, NSAttributedString.Key.foregroundColor: str.fontColor], range: boldStringRange)
        }
        return attrString
    }
    
    func trimmedText()->String
    {
        return trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    func hasNoCharchters() -> Bool
    {
        return trimmedText().count == 0
    }
    
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    var isArabic: Bool {
        let predicate = NSPredicate(format: "SELF MATCHES %@", "(?s).*\\p{Arabic}.*")
        return predicate.evaluate(with: self)
    }
    func convertToEnglish() -> String {
        var str: String = self
        let numbersDictionary: Dictionary = [  "٠": "0", "١": "1", "٢": "2", "٣": "3", "٤": "4", "٥": "5", "٦": "6", "٧": "7", "٨": "8", "٩": "9"]
        numbersDictionary.forEach { str = str.replacingOccurrences(of: $0, with: $1) }
        return str
    }
    
    func toInt() -> Int {
        if self.isArabic {
            return Int(self.convertToEnglish()) ?? 0
        }
        return Int(self) ?? 0
    }
    
    var color: UIColor {
        let hex = self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            return UIColor.clear
        }
        return UIColor(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
