//
//  UITextField+Extension.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/5/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import Localize_Swift
import NKVPhonePicker

extension UITextField {
    func setTextFieldBorder(borderColor: UIColor, borderWidth: CGFloat) {
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
    }
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        if Localize.currentLanguage() == "ar", !self .isKind(of: NKVPhonePickerTextField.self) {
            if textAlignment == .natural {
                self.textAlignment = .right
            }
        }
    }
}
