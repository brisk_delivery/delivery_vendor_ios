//
//  TextView.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/15/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

class TextView: UITextView {
    
    @IBInspectable var rounded: Bool = false {
        didSet {
            updateFrames()
        }
    }
    
    private lazy var placeholderLabel: UILabel = {
        let label =  UILabel()
        addSubview(label)
        return label
    }()
    
    @IBInspectable var placeholder: String? {
        set {
            self.placeholderLabel.text = newValue
        }
        
        get {
            return self.placeholderLabel.text
        }
    }
    
    @IBInspectable var placeholderColor: UIColor! {
        set {
            self.placeholderLabel.textColor = newValue
        }
        
        get {
            return self.placeholderLabel.textColor
        }
    }
    
    override var text: String! {
        didSet {
            textDidChange()
        }
    }
    
    @IBInspectable var textInset: CGPoint = .zero {
        didSet {
            self.textContainerInset = UIEdgeInsets(top: textInset.y, left: textInset.x - 3, bottom: textInset.y, right: textInset.x - 3)
        }
    }
    
    override var frame: CGRect {
        didSet {
            updateFrames()
        }
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        updateFrames()
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    @IBInspectable var maximumCharacters: UInt = 0
    
    private func initialize() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.textDidChange), name: UITextView.textDidChangeNotification, object: self)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UITextView.textDidChangeNotification, object: self)
    }
    
    @objc private func textDidChange() {
        if text == "" {
            placeholderLabel.isHidden = false
        } else {
            placeholderLabel.isHidden = true
        }
        guard
            let prospectiveText = self.text,
            maximumCharacters > 0,
            prospectiveText.count > Int(maximumCharacters)
            else {
                return
        }
        
        let selection = selectedTextRange
        let maxCharIndex = prospectiveText.index(prospectiveText.startIndex, offsetBy: Int(maximumCharacters - 1))
        text = String(prospectiveText[...maxCharIndex])
        selectedTextRange = selection
    }
    
    override var font: UIFont? {
        didSet {
            placeholderLabel.font = font
        }
    }
    
    override var textAlignment: NSTextAlignment {
        didSet {
            placeholderLabel.textAlignment = textAlignment
        }
    }
    
    private(set) var loaded: Bool = false
    func viewDidLoad() {
        loaded = true
        layer.borderWidth = borderWidth
        placeholderLabel.font = self.font
        placeholderLabel.textAlignment = textAlignment
        self.textContainerInset = UIEdgeInsets(top: textInset.y, left: textInset.x - 3, bottom: textInset.y, right: textInset.x - 3)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if !loaded {
            self.viewDidLoad()
        }
    }
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        if !loaded {
            self.viewDidLoad()
        }
    }
    
    func updateFrames() {
        if rounded {
            layer.cornerRadius = min(frame.size.width/2.0, frame.size.height/2.0)
        } else {
            layer.cornerRadius = cornerRadius
        }
        
        placeholderLabel.frame = CGRect(x: textInset.x + 4, y: textInset.y + 5, width: bounds.width - 2 * textInset.x, height: 0)
        placeholderLabel.frame.size.height = placeholderLabel.intrinsicContentSize.height
    }
}
