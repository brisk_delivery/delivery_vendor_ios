//
//  UIImage+extension.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/15/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

enum ImageType {
    case cover
    case profile
    case product
}

extension UIImage {
    
    func isSizeValid(type: ImageType) -> Bool {
        let heightInPoints = self.size.height
        let heightInPixels = heightInPoints * self.scale

        let widthInPoints = self.size.width
        let widthInPixels = widthInPoints * self.scale
        switch type {
        case .cover:
            return (widthInPixels >= 750) && (heightInPixels >= 400)
        case .profile:
           return (widthInPixels >= 200) && (heightInPixels >= 200)
        case .product:
            return (widthInPixels >= 420) && (heightInPixels >= 420)
        }
    }
    
    
    func resizeImage(newWidth: CGFloat) -> UIImage? {

        let withSize = CGSize(width: 712, height: 712)
        var actualHeight: CGFloat = self.size.height
        var actualWidth: CGFloat = self.size.width
        let maxHeight: CGFloat = withSize.width
        let maxWidth: CGFloat = withSize.height
        var imgRatio: CGFloat = actualWidth/actualHeight
        let maxRatio: CGFloat = maxWidth/maxHeight
        let compressionQuality = 1.0//100 percent compression
        
        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if(imgRatio < maxRatio) {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            } else if(imgRatio > maxRatio) {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            } else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        
        let rect: CGRect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        self.draw(in: rect)
        let image: UIImage  = UIGraphicsGetImageFromCurrentImageContext()!
        let imageData = image.jpegData(compressionQuality: CGFloat(compressionQuality))
        UIGraphicsEndImageContext()
        
        let resizedImage = UIImage(data: imageData!)
        return resizedImage
    }
}
