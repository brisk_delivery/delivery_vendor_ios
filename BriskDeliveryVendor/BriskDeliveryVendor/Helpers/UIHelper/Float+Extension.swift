//
//  Float+Extension.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/30/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

extension Float
{
    var cleanValue: String
    {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}
