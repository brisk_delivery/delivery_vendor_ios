//
//  CustomView.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/1/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

class UIViewWithDashedLineBorder: UIView {
    
    @IBInspectable var strokeColor: UIColor?
    
    override func draw(_ rect: CGRect) {
        let path = UIBezierPath(roundedRect: rect, cornerRadius:0)
        UIColor.white.setFill()
        path.fill()
        let color = strokeColor != nil ? strokeColor : #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
        color!.setStroke()
        path.lineWidth = 3
        let dashPattern: [CGFloat] = [6, 6]
        path.setLineDash(dashPattern, count: 2, phase: 0)
        path.stroke()
    }
    
}
