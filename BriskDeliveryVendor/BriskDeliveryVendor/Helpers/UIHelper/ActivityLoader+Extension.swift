//
//  ActivityLoader+Extension.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/3/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
extension UIActivityIndicatorView {
    func startSpinner() {
        startAnimating()
        isHidden = false
        isUserInteractionEnabled = false
    }
    
    func stopSpinner() {
        stopAnimating()
        isHidden = true
        isUserInteractionEnabled = true
    }
}
