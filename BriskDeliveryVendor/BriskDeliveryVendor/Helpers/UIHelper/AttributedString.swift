//
//  AttributedString.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/2/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

extension NSMutableAttributedString {
    
    public func setAsLink(textToFind: String) {
        
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(.link, value: "", range: foundRange)
        }
    }
}
