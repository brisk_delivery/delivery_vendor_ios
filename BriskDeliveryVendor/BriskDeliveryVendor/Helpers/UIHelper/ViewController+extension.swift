//
//  ViewController+extension.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/23/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import CFAlertViewController
import ZVProgressHUD

extension UIViewController {
    
    func alert(title: String, message: String, ActionButtons: [(title: String, style: CFAlertAction.CFAlertActionStyle, alignment: CFAlertAction.CFAlertActionAlignment, bgColor: UIColor, textColor: UIColor, handler: (_ action: CFAlertAction) -> ())]) {
        // Create Alet View Controller
        let alertController = CFAlertViewController(title: title,
                                                    message: message,
                                                    textAlignment: .center,
                                                    preferredStyle: .alert,
                                                    didDismissAlertHandler: nil)
        for btn in ActionButtons {
                    // Create Upgrade Action
        let defaultAction = CFAlertAction(title: btn.title,
                                          style: btn.style,
                                          alignment: btn.alignment,
                                          backgroundColor: btn.bgColor,
                                          textColor: btn.textColor,
                                          handler: btn.handler)
        
        // Add Action Button Into Alert
        alertController.addAction(defaultAction)
        }
        // Present Alert View Controller
        present(alertController, animated: true, completion: nil)
    }
    
    func generalSharing(shareText: String) {
        let activityViewController =
            UIActivityViewController(activityItems: [shareText],
                                     applicationActivities: nil)
        present(activityViewController, animated: true) {
        }
    }
    
    func showIndicator(){
        ProgressHUD.shared.displayStyle = .custom(backgroundColor: "6B46C1".color, foregroundColor: UIColor.white)
            self.view.isUserInteractionEnabled = true
            DispatchQueue.global(qos: .default).async(execute: {
                       // time-consuming task
                       DispatchQueue.main.async(execute: {
                           ProgressHUD.shared.show()
                       })
                   })
        }
        
        func hideIndicator()
        {
            self.view.isUserInteractionEnabled = true
            DispatchQueue.global(qos: .default).async(execute: {
                // time-consuming task
                DispatchQueue.main.async(execute: {
                    ProgressHUD.shared.dismiss()
                })
            })
        }
    
    func checkPermission()-> Bool {
        guard let user = Global.user, let type = user.userType, type == "employee" else {
           return true
        }
        alert(title: "", message: "noPermission".localized(), ActionButtons: [(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
        return false
    }
}
