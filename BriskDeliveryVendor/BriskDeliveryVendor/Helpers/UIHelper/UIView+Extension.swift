//
//  UIView+Extension.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/4/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

extension UIView {
    
    @IBInspectable var borderColor: UIColor? {
        get { return layer.borderColor.map(UIColor.init) }
        set { layer.borderColor = newValue?.cgColor }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get { return layer.borderWidth }
        set { layer.borderWidth = newValue }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get { return layer.cornerRadius}
        set { layer.cornerRadius =  newValue
            layer.masksToBounds = newValue > 0 }
    }
    
    func dropShadow(opacity: Float, offSet: CGSize, radius: CGFloat) {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = offSet
        layer.shadowOpacity = opacity
        layer.shadowRadius = radius
        layer.masksToBounds =  false
    }
    
    func dropShadowWithPath(opacity: Float, offSet: CGSize, radius: CGFloat, frame: CGRect) {
        dropShadow(opacity: opacity, offSet: offSet, radius: radius)
        layer.shadowPath = UIBezierPath(roundedRect: frame, cornerRadius: cornerRadius).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
    }
    
    func setXIBConstraints(xib: UIView) {
        xib.translatesAutoresizingMaskIntoConstraints = false
        xib.heightAnchor.constraint(equalTo: (self.heightAnchor), constant: 0).isActive = true
        xib.widthAnchor.constraint(equalTo: (self.widthAnchor), constant: 0).isActive = true
        xib.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        xib.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
    }
    
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}

class RoundView: UIView {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.bounds.size.height / 2.0
        self.layer.masksToBounds = true
    }
}

class CustomView: UIView {
    
    var cornersArray: [Int] = [Int]()
    var gradientColors: [UIColor] = [UIColor]()
    var gradientStartPoint: [CGFloat] = [CGFloat()]
    var gradientEndPoint: [CGFloat] = [CGFloat()]
    
    var cornersWidthValue: CGFloat?
    var borderLayer: CAShapeLayer?
    var gradientLayer: CAGradientLayer?
    
    @IBInspectable var cornersWidth: CGFloat {
        get { return cornersWidthValue ?? 0.0 }
        set { cornersWidthValue = newValue }
    }
    
    @IBInspectable var corners: String {
        get { return cornersArray.map({$0.description}).joined(separator: " ") }
        set( corners) { self.cornersArray = corners.components(separatedBy: " ").map({NumberFormatter().number(from: $0) as? Int ?? 0})}
    }
    
    @IBInspectable var startPoint: String {
        get { return gradientStartPoint.map({$0.description}).joined(separator: " ") }
        set( point) { self.gradientStartPoint = point.components(separatedBy: " ").map({NumberFormatter().number(from: $0) as? CGFloat ?? 0.0})}
    }
    
    @IBInspectable var endPoint: String {
        get { return gradientEndPoint.map({$0.description}).joined(separator: " ") }
        set( point) { self.gradientEndPoint = point.components(separatedBy: " ").map({NumberFormatter().number(from: $0) as? CGFloat ?? 0.0})}
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.updateUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.updateUI()
    }
    
    func roundCorner() {
        var rectCorner: UIRectCorner = UIRectCorner()
        for corner in cornersArray {
            if corner == 1 {
                rectCorner.insert(.topRight)
            }else if corner == 2 {
                rectCorner.insert(.topLeft)
            }else if corner == 3 {
                rectCorner.insert(.bottomRight)
            }else if corner == 4 {
                rectCorner.insert(.bottomLeft)
            }
        }
        
        layer.round(corners: rectCorner, width: cornersWidthValue ?? 18.0, height: 0.0)
    }
    
    func gradient() {
        gradientLayer?.removeFromSuperlayer()
        gradientLayer = layer.gradient(colors: gradientColors.map({$0.cgColor}),
                                       locations: [0, 1],
                                       startPoint: CGPoint(x: gradientStartPoint[0], y: gradientStartPoint[1]),
                                       endPoint: CGPoint(x: gradientEndPoint[0], y: gradientEndPoint[1]))
    }
    
    func updateUI() {
        
        if cornersArray.count > 0 {
            roundCorner()
        }
        
        if gradientColors.count > 0 {
            gradient()
        }
    }
}


