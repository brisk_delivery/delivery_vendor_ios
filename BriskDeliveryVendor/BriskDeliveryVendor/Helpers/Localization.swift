//
//  Localization.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 5/10/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

protocol Localizable {
    var localized: String { get }
}

extension String: Localizable {
    var localized: String {
        return self.localized()
    }
}

protocol XIBLocalizable {
    var xibLocKey: String? {get set }
}

class LocalizeLabel: UILabel, XIBLocalizable {
    var localizeKey: String?
    
    @IBInspectable var xibLocKey: String? {
        get { return nil }
        set(key) {
            localizeKey = key
            text = key?.localized
        }
    }
}

extension UIButton: XIBLocalizable {
    @IBInspectable var xibLocKey: String? {
        get { return nil }
        set(key) {
            setTitle(key?.localized, for: .normal)
        }
   }
}

extension UITextField: XIBLocalizable {
    @IBInspectable var xibLocKey: String? {
        get { return nil }
        set(key) {
            placeholder = key?.localized
        }
   }
}
