//
//  CameraHandler.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/8/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation
import UIKit

class CameraHandler: NSObject {
    
    static let shared = CameraHandler()
    fileprivate var currentVC: UIViewController!
    var imagePickedBlock: ((UIImage) -> Void)?
    
    func camera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .camera
            myPickerController.allowsEditing = true
            currentVC.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    func photoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .photoLibrary
            myPickerController.allowsEditing = true
            currentVC.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    func showActionSheet(vc: UIViewController) {
        currentVC = vc
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "camera".localized(), style: .default, handler: {(_: UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "gallery".localized(), style: .default, handler: { (_: UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        actionSheet.addAction(UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil))
        
        if let presenter = actionSheet.popoverPresentationController {
            presenter.permittedArrowDirections = []
            presenter.sourceRect = CGRect(x: ((vc.view.frame.width)/2) - 150, y: ((vc.view.frame.height)/2) - 150, width: 300, height: 300)
            presenter.sourceView = vc.view
        }
        
        vc.present(actionSheet, animated: true, completion: nil)
    }
    
}

extension CameraHandler: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        currentVC.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            self.imagePickedBlock?(image)
        } else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.imagePickedBlock?(image)
        } else {
            print("Something went wrong")
        }
        currentVC.dismiss(animated: true, completion: nil)
    }

}

