//
//  ImageData.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/15/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

class ImageData : Equatable {
    static func == (firstObj: ImageData, secondObj: ImageData) -> Bool {
        return firstObj.fileName == secondObj.fileName
    }
    var mediaType : Int!
    var fileName :String!
    var mimeType :String!
    var mediaFile : UIImage!
    var thumbnailFile : UIImage!
    var destinationId :String!
    var destinationType : Int!
    var actionType = 1
    var mediaData :Data?
    var thumbData :Data?
    var faildToUpload = true
    var imageFileWithoutResizing : UIImage!
}
