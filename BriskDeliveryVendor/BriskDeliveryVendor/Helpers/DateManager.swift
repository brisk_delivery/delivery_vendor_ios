//
//  DateManager.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/30/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class DateManager {

    static var generalFotmat = "yyyy-MM-dd HH:mm:ss"

    static func append(component: Calendar.Component, value: Int, to startDate: Date, format: String) -> String {
        let calendar = Calendar.current
        let date = calendar.date(byAdding: component, value: value, to: startDate)!
        return getString(date: date, format: format)
    }

    static func convertMinutes(_ dateString: String) -> String {

        let date = convertStringToLocal(dateString: dateString)

        if !equal(component: .year, date1: date, date2: Date()) {
            return convert(dateString: dateString, newFormat: "d MMM yyyy h:mm a")
        } else if !equal(component: .month, date1: date, date2: Date()) {
            return convert(dateString: dateString, newFormat: "d MMM h:mm a")
        } else if !equal(component: .day, date1: date, date2: Date()) {
            if Date().days(from: date) == 1 {
                return "yesterday"
            }
            return convert(dateString: dateString, newFormat: "d MMM h:mm a")
        } else {
            let minutes = Date().minutes(from: date)
            if minutes < 1 {
                return "now"
            } else if minutes < 60 {
                return "\(minutes) min ago"
            } else {
                return convert(dateString: dateString, newFormat: "h:mm a")
            }
        }
    }


     static func convert(dateString: String, newFormat: String) -> String {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = generalFotmat
        let date = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = newFormat
        return dateFormatter.string(from: date!)
    }

    static func get(component: Calendar.Component, date: Date) -> Int {
        return Calendar.current.component(component, from: date)
    }

    private static func equal(component: Calendar.Component, date1: Date, date2: Date) -> Bool {
        return get(component: component, date: date1) == get(component: component, date: date2)
    }

    static func convertToISO(date: Date) -> String {
        return getString(date: date, format: "yyyy-MM-dd'T'HH:mm:ss.SSS") + "+00:00"
    }

    static func getDate(_ dateString: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = generalFotmat
        return dateFormatter.date(from: dateString) ?? Date()
    }

    static func getDate(_ dateString: String, format: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: dateString) ?? Date()
    }

    static func getString(date: Date, format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }

    private static func convertToLocal(dateString: String) -> String {

        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = generalFotmat
        let date = dateFormatter.date(from: dateString)
        dateFormatter.timeZone = TimeZone.current
        return dateFormatter.string(from: date!)
    }

    private static func convertStringToLocal(dateString: String) -> Date {

        let dtf = DateFormatter()
        dtf.timeZone = TimeZone.current
        dtf.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dtf.date(from: dateString)!
    }

    static func getCurrentServer() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = generalFotmat
        return dateFormatter.string(from: Date().addingTimeInterval(20.0))
    }

    static func sortServerDates(dateString1: String, dateString2: String) -> Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = generalFotmat
        let date1 = dateFormatter.date(from: dateString1)!
        let date2 = dateFormatter.date(from: dateString2)!
        return date1 < date2
    }

    static func getDateRange(start: Int, end: Int, date: Date) -> DateRange {
        return DateRange(start: Calendar.current.date(bySettingHour: start, minute: 0, second: 0, of: date)!,
                         end: Calendar.current.date(bySettingHour: end, minute: 0, second: 0, of: date)!)
    }

    static func getDateWithISOFormat(time: String) -> Date {
        let formatter = ISO8601DateFormatter()
        if let date = formatter.date(from: time.replacingOccurrences(of: "\\.\\d+", with: "", options: .regularExpression)) {
            return date
        } else {
            return getDate(time)
        }
    }

    static func getTimeStringWithISOFormat(tripTime: String, format: String = "hh:mm a") -> String {

        let date = getDateWithISOFormat(time: tripTime)
        let stringDate = getString(date: date, format: generalFotmat)
        return convert(dateString: stringDate, newFormat: format)
    }

    

    static func isDateInRange(date: Date, dateRange: DateRange) -> Bool {
        return date >= dateRange.start && date <= dateRange.end
    }

    static func getToday() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEE MMM d"
        return formatter.string(from: Date())
    }

    static func getTomorrow() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEE MMM d"
        return formatter.string(from: Date().tomorrow)
    }

    static func getYesterday(withFormat format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: Date().yesterday)
    }

    static func getDateFromISOString(_ dateString: String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        return formatter.date(from: String(dateString.dropLast(10)))
    }

    static func getTimeComponentsFromDate(_ date: Date) -> (hour: Int, minute: Int, second: Int) {
        let hour = get(component: .hour, date: date)
        let minute = get(component: .minute, date: date)
        let second = get(component: .second, date: date)
        return(hour, minute, second)
    }

    static func getTodayInSpecificTime(_ hour: Int, _ minute: Int, _ second: Int) -> Date {
        return Calendar.current.date(bySettingHour: hour, minute: minute, second: second, of: Date())!
    }

    static func getCurrentHour() -> Int {
        return get(component: .hour, date: Date())
    }

    static func currentDateByAdding(_ amount: Int, _ component: Calendar.Component) -> Date {
        return Calendar.current.date(byAdding: component, value: amount, to: Date())!
    }

    static func getDateWithSpecificTimeFromISO(_ iso: String, in day: Date) -> String {
        let isoDate = getDateFromISOString(iso) ?? Date()
        let (hour, minute, second) = getTimeComponentsFromDate(isoDate)
        let date = Calendar.current.date(bySettingHour: hour, minute: minute, second: second, of: day)!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = generalFotmat
        return dateFormatter.string(from: date)
    }

    static func getValidTripTimeFor(_ tripTime: String, withLimit limit: Int) -> String? {
        let limitToday = DateManager.getTodayInSpecificTime(limit, 0, 0)
        let limitWithHourAdded = Calendar.current.date(byAdding: .hour, value: 1, to: limitToday)
        if Date() <= limitWithHourAdded! {
            return tripTime
        }
        return nil
    }

    static func getSliderCurrent() -> Float {
        let hour = get(component: .hour, date: Date())
        let minutes = get(component: .minute, date: Date())
        if minutes < 30 {
            return Float(hour) + 0.5
        } else {
            return Float(hour + 1)
        }
    }

    static func getTripHour(iso: String) -> Float {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let isoDate = formatter.date(from: iso)!
        return Float(get(component: .hour, date: isoDate))
    }

    static func getRangesInSameDayTo(date: Date, minHour: Int, maxHour: Int) -> (min: Date, max: Date) {
        return(Calendar.current.date(bySettingHour: minHour, minute: 0, second: 0, of: date)!, Calendar.current.date(bySettingHour: maxHour, minute: 0, second: 0, of: date)!)
    }

    static func getTimeFormatAsAmPM(fromDate date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "a"
        return dateFormatter.string(from: date).uppercased()
    }

    static func dateFromTimeStamp(timeStamp: Int64) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(timeStamp/1000))
        return DateManager.getString(date: date, format: generalFotmat)
    }
}

struct DateRange {
    var start: Date
    var end: Date
}

enum Day {
    case today
    case tomorrow

    func toDate() -> Date {
        switch self {
        case .today:
            return Date().today
        case .tomorrow:
            return Date().tomorrow
        }
    }
}
extension Date {
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }

    var today: Date {
        return Date()
    }

    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month, from: self)
    }
    var isLastDayOfMonth: Bool {
        return tomorrow.month != month
    }

    var weekdayName: String {
        let formatter = DateFormatter(); formatter.dateFormat = "E"
        return formatter.string(from: self as Date)
    }
}

extension Date {

    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }

    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }

    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }

    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }

    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }

    func minutes(from date: Date) -> Int {
        let currentDate = Date().currentTimeZoneDate()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
       let date1 = dateFormatter.date(from: currentDate) ?? Date()
        return Calendar.current.dateComponents([.minute], from: date, to: date1).minute ?? 0
    }

    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }

    func nanoseconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.nanosecond], from: date, to: self).nanosecond ?? 0
    }
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        if nanoseconds(from: date) > 0 { return "\(nanoseconds(from: date))ns" }
        return ""
    }

    func currentTimeZoneDate() -> String {
        let dtf = DateFormatter()
        dtf.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateString: String = dtf.string(from: Date())
        return dateString
    }
}
