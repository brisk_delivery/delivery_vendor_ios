//
//  GeneralMethods.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/5/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import Localize_Swift

struct GeneralMethods {
    static func getAppVersion() -> String {
        if let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String {
            return appVersion
        }
        return ""
    }
    
    static func handleUnauthenticated(data: Any) {
        guard isUnauthenticated(data: data) else {
            return
        }
        Global.remove()
        if let vc = UIApplication.shared.visibleViewController {
            vc.alert(title: "", message: "sessionExpiredMessage".localized(), ActionButtons: [(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in
                MainRouterManager.launchApplication(openLanguage: false)
            }) ])
        } else {
            MainRouterManager.launchApplication(openLanguage: false)
        }
    }
    
    static func isUnauthenticated(data: Any) -> Bool {
        guard let params = data as? [String: Any], let code = params["Code"] as? Int, code == 401 else {
            return false
        }
        return true
    }
    
    static func isArabicCurrentLanguage() -> Bool {
        return Localize.currentLanguage() == "ar"
    }
}
