//
//  LocationManager.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/11/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation
import GoogleMaps


enum LocationType {
    case current
    case location
}

class LocationManager {
    static var manager: LocationManagerHelper = LocationManagerHelper()
}

class LocationManagerHelper: NSObject, CLLocationManagerDelegate {

    private var locationManager = CLLocationManager()
    private var currentLocation: CLLocation?


    func initLocationManager() {

        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location: CLLocation = locations.last {
            currentLocation = location
        }
    }

    // authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways, .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
    }

    func getLocation(type: LocationType, data: Any, completionHandler: @escaping (CLLocation) -> Void, failHandler: @escaping () -> Void) {

        switch type {
        case .location:
            if let data = data as? CLLocation {
                completionHandler(data)
            } else {
                failHandler()
            }
        case .current:
            if let currentLocation = currentLocation {
                completionHandler(currentLocation)
            } else {
                failHandler()
            }
    
        }
    }

    func getCurrentStaticMapUrl() -> String? {

        if let currentLocation = currentLocation {
            let lat = currentLocation.coordinate.latitude
            let long = currentLocation.coordinate.longitude
            return "\(lat),\(long)"
        }
        return nil
    }
}
