//
//  SettingManager.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/22/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class SettingManager {
    
    func logout(complation: @escaping (GeneralResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let params = ["token_mobile_push": token, "device_type":"iOS"]
            
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: SettingResources.logoutResource, paramters: params, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func updateProfile(firstName: String ,lastName: String , mobileNumber: String, email: String,password: String, oldPassword: String,profileImage: Data,mediaMimeType: String, complation: @escaping (UpdateProfileResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let params = ["first_name": firstName, "last_name": lastName, "mobile_number" : mobileNumber, "email": email, "password": password, "old_password": oldPassword] as [String : Any]
            
            let sm = ServerManager ()
            _ = sm.httpMultiPartFormConnect(resource: SettingResources.updateProfileResource, authentication: token,AdditionalHeaders: headers, paramters: params, mediaData: profileImage, mediaMimeType: mediaMimeType, thumbnailData: nil, mediaParamName: "profile", complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func getAboutUs(complation: @escaping (AboutUsResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: SettingResources.aboutUsResource, paramters: nil, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func getTerms(complation: @escaping (AboutUsResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        
            let countryCode = Global.countryCode ?? "971"
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: SettingResources.termsResource, paramters: nil, authentication: nil, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    
    func getWorkingHours(shopId: String, complation: @escaping (WorkingHoursResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken
        {
            let countryCode = Global.countryCode ?? "971"
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: SettingResources.getWorkingHoursResource(shopId: shopId), paramters: nil, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func getDays(complation: @escaping (DaysResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken
        {
            let countryCode = Global.countryCode ?? "971"
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            
            let sm = ServerManager ()
        _ = sm.httpConnect(resource: SettingResources.DaysResource, paramters: nil, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func saveTime(time: TimeRequest, complation: @escaping (GeneralResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken
        {
            let countryCode = Global.countryCode ?? "971"
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let params = time.asDictionary()
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: SettingResources.saveTimeResource, paramters: params, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func deleteTime(shopId: String, timeId: String, complation: @escaping (GeneralResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken
        {
            let countryCode = Global.countryCode ?? "971"
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: SettingResources.deleteTimeResource(shopId: shopId, timeId: timeId), paramters: nil, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
}
