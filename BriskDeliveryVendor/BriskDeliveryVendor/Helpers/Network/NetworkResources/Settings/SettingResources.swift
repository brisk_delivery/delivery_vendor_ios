//
//  SettingResources.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/22/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class SettingResources {
    
    static var logoutResource = Resource<GeneralResponse>(url: "\(BaseUrl)logout", httpmethod: .post)
    {(jsonData) -> GeneralResponse? in
        do {
            let response = try JSONDecoder().decode(GeneralResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static var updateProfileResource = Resource<UpdateProfileResponse>(url: "\(BaseUrl)profile", httpmethod: .post)
    {(jsonData) -> UpdateProfileResponse? in
        do {
            let response = try JSONDecoder().decode(UpdateProfileResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static var aboutUsResource = Resource<AboutUsResponse>(url: "\(BaseUrl)pages/about-us", httpmethod: .get)
    {(jsonData) -> AboutUsResponse? in
        do {
            let response = try JSONDecoder().decode(AboutUsResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static var termsResource = Resource<AboutUsResponse>(url: "\(BaseUrl)pages/terms-and-conditions", httpmethod: .get)
    {(jsonData) -> AboutUsResponse? in
        do {
            let response = try JSONDecoder().decode(AboutUsResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static var workingHoursResource = Resource<WorkingHoursResponse>(url: "\(BaseUrl)shop/", httpmethod: .get)
    {(jsonData) -> WorkingHoursResponse? in
        do {
            let response = try JSONDecoder().decode(WorkingHoursResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static func getWorkingHoursResource(shopId: String) -> Resource<WorkingHoursResponse> {
        workingHoursResource.url = "\(BaseUrl)shop/" + shopId + "/time"
        return workingHoursResource
    }
    
    static var DaysResource = Resource<DaysResponse>(url: "\(BaseUrl)days", httpmethod: .get)
    {(jsonData) -> DaysResponse? in
        do {
            let response = try JSONDecoder().decode(DaysResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static var saveTimeResource = Resource<GeneralResponse>(url: "\(BaseUrl)shop/time/work", httpmethod: .post)
    {(jsonData) -> GeneralResponse? in
        do {
            let response = try JSONDecoder().decode(GeneralResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static var deleteTimeResource = Resource<GeneralResponse>(url: "\(BaseUrl)shop", httpmethod: .delete)
    {(jsonData) -> GeneralResponse? in
        do {
            let response = try JSONDecoder().decode(GeneralResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static func deleteTimeResource(shopId: String, timeId: String) -> Resource<GeneralResponse> {
        deleteTimeResource.url = "\(BaseUrl)shop/" + shopId + "/time/" + timeId
        return deleteTimeResource
    }
    
}
