//
//  AuthenticationExtention.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/3/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class AuthenticationManager {
    func signup(firstName: String ,lastName: String ,country : String, phoneCode: Int , mobileNumber: String, email: String,password: String, complation: @escaping (LoginResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        let sm = ServerManager ()
        let params = ["first_name": firstName, "last_name": lastName, "country":country, "phone_code":phoneCode, "mobile_number" : mobileNumber, "email":email, "password": password, "user_type": "shop"] as [String : Any]
        _ = sm.httpConnect(resource: AuthenticationResources.signupResource, paramters: params, authentication: nil, complation:
            { (json, data) in
                if let result = json
                {
                    complation(result)
                }
        })
        { (error, msg) in
            
            errorHandler(error)
        }
    }
    
    func verificationCode(mobileNumber : String ,code : Int, complation: @escaping (LoginResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        let sm = ServerManager ()
        let params = ["mobile_number": mobileNumber, "verification_code": code, "user_type": "shop" ] as [String : Any]
        _ = sm.httpConnect(resource: AuthenticationResources.verificationCodeResource, paramters: params, authentication: nil, complation:
            { (json, data) in
                if let result = json
                {
                    complation(result)
                }
        })
        { (error, msg) in
            
            errorHandler(error)
        }
    }
    
    func resendVerificationCode(mobileNumber : String, complation: @escaping (ResendVerificationResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        let sm = ServerManager ()
//        AuthenticationResources.resendVerificationCodeResource.url = AuthenticationResources.resendVerificationCodeResource.url + "\(mobileNumber)/shop/Content-Type=application/json&Accept-Language=1&app_version=\(GeneralMethods.getAppVersion())&mobile_version=iPhone"
        
        _ = sm.httpConnect(resource: AuthenticationResources.updatedResendVerificationCodeResource(mobileNumber : mobileNumber), paramters: nil, authentication: nil, complation:
            { (json, data) in
                if let result = json
                {
                    complation(result)
                }
        })
        { (error, msg) in
            
            errorHandler(error)
        }
    }
    
    func performLogin(countryCode: String, mobileNumber : String ,password : String, complation: @escaping (LoginResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        let sm = ServerManager ()
        let params = ["number": mobileNumber, "password": password, "user_type": "shop", "device_type":"ios", "push_token": Global.pushToken ?? "" ] as [String : Any]
        let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
        
        _ = sm.httpConnect(resource: AuthenticationResources.loginResource, paramters: params, authentication: nil,AdditionalHeaders: headers, complation:
            { (json, data) in
                if let result = json
                {
                    complation(result)
                }
        })
        { (error, msg) in
            
            errorHandler(error)
        }
    }
    
    func forgetPassword(email : String, complation: @escaping (GeneralResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        let sm = ServerManager ()
        _ = sm.httpConnect(resource: AuthenticationResources.updatedForgetPasswordResource(email : email), paramters: nil, authentication: nil, complation:
            { (json, data) in
                if let result = json
                {
                    complation(result)
                }
        })
        { (error, msg) in
            
            errorHandler(error)
        }
    }
    
    func updateToken(complation: @escaping (GeneralResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
               {
                   let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
        let params = ["device_type":"ios", "push_token": Global.pushToken ?? "" ] as [String : Any]
        let sm = ServerManager ()
        _ = sm.httpConnect(resource: AuthenticationResources.updateTokenResource, paramters: params, authentication: token, AdditionalHeaders: headers, complation:
            { (json, data) in
                if let result = json
                {
                    complation(result)
                }
        })
        { (error, msg) in
            
            errorHandler(error)
        }
        }
    }
    
}
