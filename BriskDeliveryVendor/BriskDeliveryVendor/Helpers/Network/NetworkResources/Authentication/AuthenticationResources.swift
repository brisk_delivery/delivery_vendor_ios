//
//  AuthenticationResources.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/2/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class AuthenticationResources {
    static var signupResource = Resource<LoginResponse>(url: "\(BaseUrl)register", httpmethod: .post)
    {(jsonData) -> LoginResponse? in
        do {
            let response = try JSONDecoder().decode(LoginResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static var verificationCodeResource = Resource<LoginResponse>(url: "\(BaseUrl)verification", httpmethod: .post)
    {(jsonData) -> LoginResponse? in
        do {
            let response = try JSONDecoder().decode(LoginResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static var resendVerificationCodeResource = Resource<ResendVerificationResponse>(url: "\(BaseUrl)/code/", httpmethod: .get)
    {(jsonData) -> ResendVerificationResponse? in
        do {
            let response = try JSONDecoder().decode(ResendVerificationResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static func updatedResendVerificationCodeResource(mobileNumber : String) -> Resource<ResendVerificationResponse> {
        let url =  "\(BaseUrl)resend/code/\(mobileNumber)/shop"
        AuthenticationResources.resendVerificationCodeResource.url = url
        return AuthenticationResources.resendVerificationCodeResource
    }
    
    static var loginResource = Resource<LoginResponse>(url: "\(BaseUrl)login", httpmethod: .post)
    {(jsonData) -> LoginResponse? in
        do {
            let response = try JSONDecoder().decode(LoginResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static var forgetPasswordResource = Resource<GeneralResponse>(url: "\(BaseUrl)password/forget/", httpmethod: .get)
    {(jsonData) -> GeneralResponse? in
        do {
            let response = try JSONDecoder().decode(GeneralResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static func updatedForgetPasswordResource(email : String) -> Resource<GeneralResponse> {
        let url =  "\(BaseUrl)password/forget/\(email)/shop"
        AuthenticationResources.forgetPasswordResource.url = url
        return AuthenticationResources.forgetPasswordResource
    }
    
    static var updateTokenResource = Resource<GeneralResponse>(url: "\(BaseUrl)update/token", httpmethod: .post)
    {(jsonData) -> GeneralResponse? in
        do {
            let response = try JSONDecoder().decode(GeneralResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    
}
