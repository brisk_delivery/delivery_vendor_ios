//
//  OrderManager.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/20/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class OrderManager {
    
    func getOrders(shopId: Int, orderStatus: String, complation: @escaping (OrdersResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: OrderResources.updateGetOrdersUrl(shopId: String(shopId), orderStatus: orderStatus), paramters: nil, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            }
            )
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func getOrderDetails(shopId: Int, orderId: Int, complation: @escaping (OrderDetailsResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: OrderResources.updateOrderDetails(shopId: String(shopId), orderId: String(orderId)), paramters: nil, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                print(error)
                print(msg)

                errorHandler(error)
            }
        }
    }
    
    
    
    func updateOrderStatus(shopId: Int, orderId: Int,status: String, complation: @escaping (GeneralResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let params = ["order_status": status]
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: OrderResources.updateOrderStatus(shopId: shopId, orderId: orderId), paramters: params, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
}
