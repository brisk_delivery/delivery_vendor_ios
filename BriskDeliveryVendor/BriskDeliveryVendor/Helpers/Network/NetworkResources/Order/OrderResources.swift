//
//  OrderResources.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/20/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class OrderResources {
    
    static var getOrdersResource = Resource<OrdersResponse>(url: "\(BaseUrl)shop/", httpmethod: .get)
    {(jsonData) -> OrdersResponse? in
        do {
            let response = try JSONDecoder().decode(OrdersResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static func updateGetOrdersUrl(shopId: String, orderStatus: String) -> Resource<OrdersResponse> {
        let url = "\(BaseUrl)shop/" + shopId + "/orders"
        getOrdersResource.url = url + (orderStatus == "" ? "" :  "?order_status=\(orderStatus)")
        return getOrdersResource
    }
    
    static var orderDetailsResource = Resource<OrderDetailsResponse>(url: "\(BaseUrl)shop/", httpmethod: .get)
    {(jsonData) -> OrderDetailsResponse? in
        do {
            let response = try JSONDecoder().decode(OrderDetailsResponse.self, from: jsonData)
            return response
        } catch let jerror{
            print(jerror)
            return nil
        }
    }
    
    static func updateOrderDetails(shopId: String, orderId: String) -> Resource<OrderDetailsResponse> {
        let url =  "\(BaseUrl)shop/" + shopId + "/order/" + orderId
        orderDetailsResource.url = url
        return orderDetailsResource
    }
    
    static var updateOrderResource = Resource<GeneralResponse>(url: "\(BaseUrl)update/status/order/", httpmethod: .put)
    {(jsonData) -> GeneralResponse? in
        do {
            let response = try JSONDecoder().decode(GeneralResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static func updateOrderStatus(shopId: Int, orderId: Int) -> Resource<GeneralResponse> {
        let url = "\(BaseUrl)update/status/order/\(orderId)/shop/\(shopId)"
        OrderResources.updateOrderResource.url = url
        return OrderResources.updateOrderResource
    }
    
    
}
