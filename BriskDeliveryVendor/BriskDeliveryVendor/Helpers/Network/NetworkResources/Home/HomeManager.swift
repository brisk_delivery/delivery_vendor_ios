//
//  HomeManager.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/8/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class HomeManager {
    
    func getCategories(complation: @escaping (CategoryResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: HomeResources.categoriesResource, paramters: nil, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func getCityDistrict(complation: @escaping (CityDistrictResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "app-version": GeneralMethods.getAppVersion()]
            
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: HomeResources.cityDistrictResource, paramters: nil, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func getPaymentTypes(complation: @escaping (PaymentTypesResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken
        {
            let countryCode = Global.countryCode ?? "971"
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: HomeResources.paymentTypesResource, paramters: nil, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func getTagsTypes(categoryId: Int?, complation: @escaping (TagsResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken
        {
            let countryCode = Global.countryCode ?? "971"
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: HomeResources.getTagsResource(categoryId: categoryId), paramters: nil, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func addShop(categoryId: Int, cityId: Int, districtId: Int,shopLat: Double, shopLng: Double,timeDelivery: Int,tags: [Int],coverImage: Data,mediaMimeType: String, logoImage: Data, complation: @escaping (CreateShopResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken
        {
            let countryCode = Global.countryCode ?? "971"
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion(), "Content-Type": "multipart/form-data"]
            let params = ["cat_id": categoryId, "city_id": cityId, "district_id": districtId, "shop_lat":shopLat, "shop_lng": shopLng, "time_delivery":timeDelivery, "tags": tags] as [String : Any]
            
            let sm = ServerManager ()
            _ = sm.httpMultiPartFormConnect(resource: HomeResources.addShopResource, authentication: token,AdditionalHeaders: headers, paramters: params, mediaData: coverImage, mediaMimeType: mediaMimeType, thumbnailData: logoImage, mediaParamName: "cover", complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func translateShopData(shopId: Int, translateArr: [ShopTranslate],complation: @escaping (CreateShopResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let params = translateArr.map{$0.asDictionary()}
            let sm = ServerManager ()
            sm.httpConnectWithArrayOfParams(resource: HomeResources.updatedTranslateShopResource(shopId: shopId), paramters: params, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    
    
    func getShop(shopId: Int, complation: @escaping (ShopResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: HomeResources.getUpdatedShopResource(shopId: shopId), paramters: nil, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func updateShop(shopId: Int, categoryId: Int, cityId: Int, districtId: Int,shopLat: Double, shopLng: Double,timeDelivery: Int, tags: [Int],coverImage: Data,mediaMimeType: String, logoImage: Data, complation: @escaping (CreateShopResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken
        {
            let countryCode = Global.countryCode ?? "971"
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion(), "Content-Type": "multipart/form-data"]
            let params = ["cat_id": categoryId, "city_id": cityId, "district_id": districtId, "shop_lat":shopLat, "shop_lng": shopLng, "time_delivery":timeDelivery, "tags": tags] as [String : Any]
            
            let sm = ServerManager ()
            _ = sm.httpMultiPartFormConnect(resource: HomeResources.updateShopResource(shopId: shopId), authentication: token,AdditionalHeaders: headers, paramters: params, mediaData: coverImage, mediaMimeType: mediaMimeType, thumbnailData: logoImage, mediaParamName: "cover", complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func getShopStatus(shopId: Int, complation: @escaping (ShopStatusResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: HomeResources.getShopStatusResource(shopId: shopId), paramters: nil, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func updateShopStatus(shopId: Int, complation: @escaping (GeneralResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let busy = Global.shopStatus == 0 ? 1 : 0
            let params = ["busy": busy] as [String : Any]
            print(params)
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: HomeResources.updateStatusResource(shopId: shopId), paramters: params, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    func updateDeliveryStatus(shopId: Int, complation: @escaping (GeneralResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let deliver = Global.DeliveryStatus == 0 ? 1 : 0
            let params = ["deliver_by_shop": deliver] as [String : Any]
            print(params)
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: HomeResources.updateStatusResource(shopId: shopId), paramters: params, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
}
