//
//  HomeResources.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/8/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class HomeResources {
    
    static var categoriesResource = Resource<CategoryResponse>(url: "\(BaseUrl)categories", httpmethod: .get)
    {(jsonData) -> CategoryResponse? in
        do {
            let response = try JSONDecoder().decode(CategoryResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static var cityDistrictResource = Resource<CityDistrictResponse>(url: "\(BaseUrl)city_district", httpmethod: .get)
    {(jsonData) -> CityDistrictResponse? in
        do {
            let response = try JSONDecoder().decode(CityDistrictResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static var paymentTypesResource = Resource<PaymentTypesResponse>(url: "\(BaseUrl)payment", httpmethod: .get)
    {(jsonData) -> PaymentTypesResponse? in
        do {
            let response = try JSONDecoder().decode(PaymentTypesResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static var tagsResource = Resource<TagsResponse>(url: "\(BaseUrl)tags", httpmethod: .get)
    {(jsonData) -> TagsResponse? in
        do {
            let response = try JSONDecoder().decode(TagsResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static func getTagsResource(categoryId: Int?) -> Resource<TagsResponse> {
        let url = categoryId == nil ? "\(BaseUrl)tags" : "\(BaseUrl)tags?cat_id=\(categoryId ?? 0)"
        HomeResources.tagsResource.url = url
        return HomeResources.tagsResource
    }
    
    static var addShopResource = Resource<CreateShopResponse>(url: "\(BaseUrl)shop", httpmethod: .post)
    {(jsonData) -> CreateShopResponse? in
        do {
            let response = try JSONDecoder().decode(CreateShopResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static var translateShopResource = Resource<CreateShopResponse>(url: "\(BaseUrl)shop/", httpmethod: .post)
    {(jsonData) -> CreateShopResponse? in
        do {
            let response = try JSONDecoder().decode(CreateShopResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static func updatedTranslateShopResource(shopId: Int) -> Resource<CreateShopResponse> {
        let url =  "\(BaseUrl)shop/\(shopId)/translate"
        HomeResources.translateShopResource.url = url
        return HomeResources.translateShopResource
    }
    
    static var getShopResource = Resource<ShopResponse>(url: "\(BaseUrl)shop", httpmethod: .get)
    {(jsonData) -> ShopResponse? in
        do {
            let response = try JSONDecoder().decode(ShopResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static func getUpdatedShopResource(shopId: Int) -> Resource<ShopResponse> {
        let url =  "\(BaseUrl)shop/\(shopId)"
        HomeResources.getShopResource.url = url
        return HomeResources.getShopResource
    }
    
    static var updateShopResource = Resource<CreateShopResponse>(url: "\(BaseUrl)shop/", httpmethod: .post)
    {(jsonData) -> CreateShopResponse? in
        do {
            let response = try JSONDecoder().decode(CreateShopResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static func updateShopResource(shopId: Int) -> Resource<CreateShopResponse> {
        let url =  "\(BaseUrl)shop/\(shopId)"
        HomeResources.updateShopResource.url = url
        return HomeResources.updateShopResource
    }
    
    static var shopStatusResource = Resource<ShopStatusResponse>(url: "\(BaseUrl)status/shop/", httpmethod: .get)
    {(jsonData) -> ShopStatusResponse? in
        do {
            let response = try JSONDecoder().decode(ShopStatusResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static func getShopStatusResource(shopId: Int) -> Resource<ShopStatusResponse> {
        let url =  "\(BaseUrl)status/shop/\(shopId)"
        HomeResources.shopStatusResource.url = url
        return HomeResources.shopStatusResource
    }
    
    static var updateStatusResource = Resource<GeneralResponse>(url: "\(BaseUrl)status/shop/", httpmethod: .put)
    {(jsonData) -> GeneralResponse? in
        do {
            let response = try JSONDecoder().decode(GeneralResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static func updateStatusResource(shopId: Int) -> Resource<GeneralResponse> {
        let url =  "\(BaseUrl)status/shop/\(shopId)"
        HomeResources.updateStatusResource.url = url
        return HomeResources.updateStatusResource
    }
    
}
