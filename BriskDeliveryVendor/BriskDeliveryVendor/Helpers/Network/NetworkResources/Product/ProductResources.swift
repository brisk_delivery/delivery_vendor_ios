//
//  ProductResources.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/18/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class ProductResources {
    
    static var addProductResource = Resource<CreateProductResponse>(url: "\(BaseUrl)product", httpmethod: .post)
    {(jsonData) -> CreateProductResponse? in
        do {
            let response = try JSONDecoder().decode(CreateProductResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static var getProductsResource = Resource<ProductsResponse>(url: "\(BaseUrl)shop/", httpmethod: .get)
    {(jsonData) -> ProductsResponse? in
        do {
            let response = try JSONDecoder().decode(ProductsResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    static var getCategoryOptionResource = Resource<OptionCategoryResponse>(url: "\(BaseUrl)options/", httpmethod: .get)
    {(jsonData) -> OptionCategoryResponse? in
        do {
            let response = try JSONDecoder().decode(OptionCategoryResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    static func updatedGetProductsResource(shopId: Int) -> Resource<ProductsResponse> {
        let url =  "\(BaseUrl)shop/\(shopId)/products"
        ProductResources.getProductsResource.url = url
        return ProductResources.getProductsResource
    }
    static func getCategoryOptionResource(categoryId: Int) -> Resource<OptionCategoryResponse> {
        let url =  "\(BaseUrl)options/category/\(categoryId)"
        ProductResources.getCategoryOptionResource.url = url
        return ProductResources.getCategoryOptionResource
    }
    static var shopCategoriesResource = Resource<CategoryResponse>(url: "\(BaseUrl)category/", httpmethod: .get)
    {(jsonData) -> CategoryResponse? in
        do {
            let response = try JSONDecoder().decode(CategoryResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static func updatedShopCategoriesResource(categoryId: Int) -> Resource<CategoryResponse> {
        let url =  "\(BaseUrl)category/\(categoryId)/sub"
        ProductResources.shopCategoriesResource.url = url
        return ProductResources.shopCategoriesResource
    }
    
    static var productImageResource = Resource<CreateProductResponse>(url: "\(BaseUrl)product/", httpmethod: .post)
    {(jsonData) -> CreateProductResponse? in
        do {
            let response = try JSONDecoder().decode(CreateProductResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static func updatedProductImageResource(productId: Int) -> Resource<CreateProductResponse> {
        let url =  "\(BaseUrl)product/\(productId)/image"
        ProductResources.productImageResource.url = url
        return ProductResources.productImageResource
    }
    
    static var deleteProductResource = Resource<GeneralResponse>(url: "\(BaseUrl)product/", httpmethod: .delete)
    {(jsonData) -> GeneralResponse? in
        do {
            let response = try JSONDecoder().decode(GeneralResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static func deleteProductResource(productId: Int) -> Resource<GeneralResponse> {
        let url =  "\(BaseUrl)product/\(productId)"
        ProductResources.deleteProductResource.url = url
        return ProductResources.deleteProductResource
    }
    
    static var getProductResource = Resource<ProductDetailsResponse>(url: "\(BaseUrl)shop/", httpmethod: .get)
    {(jsonData) -> ProductDetailsResponse? in
        do {
            let response = try JSONDecoder().decode(ProductDetailsResponse.self, from: jsonData)
            return response
        } catch let jerror{
            print( jerror)

            return nil
        }
    }
    static var getProductStatus = Resource<ProductStatusResponse>(url: "\(BaseUrl)/status", httpmethod: .get)
    {
        (jsonData) -> ProductStatusResponse? in
        do {
            let response = try JSONDecoder().decode(ProductStatusResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    static var updateProductStatus = Resource<GeneralResponse>(url: "\(BaseUrl)/status", httpmethod: .put)
    {
        (jsonData) -> GeneralResponse? in
        do {
            let response = try JSONDecoder().decode(GeneralResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    static func getProductResource(shopId: Int, productId: Int) -> Resource<ProductDetailsResponse> {
        let url =  "\(BaseUrl)shop/\(shopId)/product/\(productId)/data"
        ProductResources.getProductResource.url = url
        return ProductResources.getProductResource
    }
    static func getProductStatus(shopId: Int, productId: Int) -> Resource<ProductStatusResponse> {
        let url =  "\(BaseUrl)status/product/\(productId)"
        print(url)
        ProductResources.getProductStatus.url = url
        return ProductResources.getProductStatus
    }
    static func updateProductStatus(shopId: Int, productId: Int) -> Resource<GeneralResponse> {
        let url =  "\(BaseUrl)status/product/\(productId)"
        print(url)
        ProductResources.updateProductStatus.url = url
        return ProductResources.updateProductStatus
    }
    static var updateProductResource = Resource<CreateProductResponse>(url: "\(BaseUrl)product/", httpmethod: .put)
    {(jsonData) -> CreateProductResponse? in
        do {
            let response = try JSONDecoder().decode(CreateProductResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static func updateProductResource(productId: Int) -> Resource<CreateProductResponse> {
        let url =  "\(BaseUrl)product/\(productId)/update"
        ProductResources.updateProductResource.url = url
        return ProductResources.updateProductResource
    }
    
    static var addProductPriceResource = Resource<CreateProductResponse>(url: "\(BaseUrl)product/", httpmethod: .post)
    {(jsonData) -> CreateProductResponse? in
        do {
            let response = try JSONDecoder().decode(CreateProductResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static func addProductPriceResource(productId: Int) -> Resource<CreateProductResponse> {
        let url =  "\(BaseUrl)product/\(productId)/price"
        ProductResources.addProductPriceResource.url = url
        return ProductResources.addProductPriceResource
    }
    
    static var updateProductPriceResource = Resource<CreateProductResponse>(url: "\(BaseUrl)product/", httpmethod: .put)
    {(jsonData) -> CreateProductResponse? in
        do {
            let response = try JSONDecoder().decode(CreateProductResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static func updateProductPriceResource(productId: Int, priceRequestId: Int) -> Resource<CreateProductResponse> {
        let url =  "\(BaseUrl)product/\(productId)/price/\(priceRequestId)/update"
        ProductResources.updateProductPriceResource.url = url
        return ProductResources.updateProductPriceResource
    }
    
    static var addProductAdditionResource = Resource<CreateProductResponse>(url: "\(BaseUrl)product/", httpmethod: .post)
    {(jsonData) -> CreateProductResponse? in
        do {
            let response = try JSONDecoder().decode(CreateProductResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    static var addProductOptionResource = Resource<CreateProductResponse>(url: "\(BaseUrl)product/", httpmethod: .post)
    {(jsonData) -> CreateProductResponse? in
        do {
            let response = try JSONDecoder().decode(CreateProductResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static func addProductAddtionResource(productId: Int) -> Resource<CreateProductResponse> {
        let url =  "\(BaseUrl)product/\(productId)/addition"
        ProductResources.addProductAdditionResource.url = url
        return ProductResources.addProductAdditionResource
    }
    static func addProductOptionResource(productId: Int) -> Resource<CreateProductResponse> {
        let url =  "\(BaseUrl)product/\(productId)/option"
        ProductResources.addProductOptionResource.url = url
        return ProductResources.addProductOptionResource
    }
    static var updateAdditionResource = Resource<CreateProductResponse>(url: "\(BaseUrl)product/", httpmethod: .put)
    {(jsonData) -> CreateProductResponse? in
        do {
            let response = try JSONDecoder().decode(CreateProductResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    static var updateOptionResource = Resource<CreateProductResponse>(url: "\(BaseUrl)product/", httpmethod: .put)
    {(jsonData) -> CreateProductResponse? in
        do {
            let response = try JSONDecoder().decode(CreateProductResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static func updateProductAdditionResource(productId: Int, additionId: Int) -> Resource<CreateProductResponse> {
        let url =  "\(BaseUrl)product/\(productId)/addition/\(additionId)/update"
        ProductResources.updateAdditionResource.url = url
        return ProductResources.updateAdditionResource
    }
    static func updateProductOptionResource(productId: Int, optionId: Int) -> Resource<CreateProductResponse> {
        let url =  "\(BaseUrl)product/\(productId)/option/\(optionId)/update"
        ProductResources.updateOptionResource.url = url
        return ProductResources.updateOptionResource
    }
    static var deletePriceResource = Resource<GeneralResponse>(url: "\(BaseUrl)product/", httpmethod: .delete)
    {(jsonData) -> GeneralResponse? in
        do {
            let response = try JSONDecoder().decode(GeneralResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static func deletePriceResource(productId: Int, priceId: Int) -> Resource<GeneralResponse> {
        let url =  "\(BaseUrl)product/\(productId)/price/\(priceId)"
        ProductResources.deletePriceResource.url = url
        return ProductResources.deletePriceResource
    }
    
    static var deleteAdditionResource = Resource<GeneralResponse>(url: "\(BaseUrl)product/", httpmethod: .delete)
    {(jsonData) -> GeneralResponse? in
        do {
            let response = try JSONDecoder().decode(GeneralResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    static var deleteOptionResource = Resource<GeneralResponse>(url: "\(BaseUrl)product/", httpmethod: .delete)
    {(jsonData) -> GeneralResponse? in
        do {
            let response = try JSONDecoder().decode(GeneralResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static func deleteAdditionResource(productId: Int, additionId: Int) -> Resource<GeneralResponse> {
        let url =  "\(BaseUrl)product/\(productId)/addition/\(additionId)"
        ProductResources.deleteAdditionResource.url = url
        return ProductResources.deleteAdditionResource
    }
    static func deleteOptionResource(productId: Int, optionId: Int) -> Resource<GeneralResponse> {
        let url =  "\(BaseUrl)product/\(productId)/options/\(optionId)"
        ProductResources.deleteOptionResource.url = url
        return ProductResources.deleteOptionResource
    }
    
    static var getOffersResource = Resource<GetOffersResponse>(url: "\(BaseUrl)product/", httpmethod: .get)
    {(jsonData) -> GetOffersResponse? in
        do {
            let response = try JSONDecoder().decode(GetOffersResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static func getOffersResource(productId: Int) -> Resource<GetOffersResponse> {
        let url =  "\(BaseUrl)product/\(productId)/offer"
        ProductResources.getOffersResource.url = url
        return ProductResources.getOffersResource
    }
    
    static var deleteOfferResource = Resource<GeneralResponse>(url: "\(BaseUrl)offers/", httpmethod: .delete)
    {(jsonData) -> GeneralResponse? in
        do {
            let response = try JSONDecoder().decode(GeneralResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static func deleteOfferResource(offerId: Int) -> Resource<GeneralResponse> {
        let url =  "\(BaseUrl)offers/\(offerId)"
        ProductResources.deleteOfferResource.url = url
        return ProductResources.deleteOfferResource
    }
    
    static var saveOfferResource = Resource<GeneralResponse>(url: "\(BaseUrl)save/product/offer", httpmethod: .post)
    {(jsonData) -> GeneralResponse? in
        do {
            let response = try JSONDecoder().decode(GeneralResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static var updateOfferResource = Resource<GeneralResponse>(url: "\(BaseUrl)save/product/offer/", httpmethod: .post)
    {(jsonData) -> GeneralResponse? in
        do {
            let response = try JSONDecoder().decode(GeneralResponse.self, from: jsonData)
            return response
        } catch {
            return nil
        }
    }
    
    static func updateOfferResource(offerId: Int) -> Resource<GeneralResponse> {
        let url =  "\(BaseUrl)save/product/offer/\(offerId)"
        ProductResources.updateOfferResource.url = url
        return ProductResources.updateOfferResource
    }
}
