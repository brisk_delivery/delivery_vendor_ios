//
//  ProductManager.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/18/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class ProductManager {
    
    func addProduct(product: AddProductRequest, complation: @escaping (CreateProductResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let params = product.asDictionary()
            print(params)

            let sm = ServerManager ()
            _ = sm.httpConnect(resource: ProductResources.addProductResource, paramters: params, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func getProducts(shopId: Int, complation: @escaping (ProductsResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: ProductResources.updatedGetProductsResource(shopId: shopId), paramters: nil, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    func getCategoryOptions(categoryId: Int, complation: @escaping (OptionCategoryResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: ProductResources.getCategoryOptionResource(categoryId: categoryId), paramters: nil, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    func getSubCategories(categoryId: Int, complation: @escaping (CategoryResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: ProductResources.updatedShopCategoriesResource(categoryId: categoryId), paramters: nil, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func updateProductImage(productId: Int, productImage: Data, mediaMimeType: String,complation: @escaping (CreateProductResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let sm = ServerManager ()
            _ = sm.httpMultiPartFormConnect(resource: ProductResources.updatedProductImageResource(productId: productId), authentication: token,AdditionalHeaders: headers, paramters: nil, mediaData: productImage, mediaMimeType: mediaMimeType, thumbnailData: nil, mediaParamName: "image", complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func deleteProduct(productId: Int, complation: @escaping (GeneralResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: ProductResources.deleteProductResource(productId: productId), paramters: nil, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func getProduct(shopId: Int, productId: Int, complation: @escaping (ProductDetailsResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: ProductResources.getProductResource(shopId: shopId, productId: productId), paramters: nil, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    func getProductStatus(shopId: Int, productId: Int, complation: @escaping (ProductStatusResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: ProductResources.getProductStatus(shopId: shopId, productId: productId), paramters: nil, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    func updateProductStatus(shopId: Int, productId: Int,available: Int, complation: @escaping (GeneralResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: ProductResources.updateProductStatus(shopId: shopId, productId: productId), paramters: ["not_available": available], authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    func updateProduct(productId: Int,product: UpdateProductRequest, complation: @escaping (CreateProductResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let params = product.asDictionary()
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: ProductResources.updateProductResource(productId: productId), paramters: params, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func addProductPrice(productId: Int,price: Price, complation: @escaping (CreateProductResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let params = price.asDictionary()
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: ProductResources.addProductPriceResource(productId: productId), paramters: params, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func updateProductPrice(priceRequestId: Int, productId: Int, priceRequest: UpdatePriceRequest, complation: @escaping (CreateProductResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let params = priceRequest.asDictionary()
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: ProductResources.updateProductPriceResource(productId: productId, priceRequestId: priceRequestId), paramters: params, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func addProductAddition(productId: Int,addition: Addition, complation: @escaping (CreateProductResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let params = addition.asDictionary()
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: ProductResources.addProductAddtionResource(productId: productId), paramters: params, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    func addProductOption(productId: Int,option: ProductOptions, complation: @escaping (CreateProductResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let params = option.asDictionary()
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: ProductResources.addProductOptionResource(productId: productId), paramters: params, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func updateProductAddition(additionId: Int, productId: Int, addition: Addition, complation: @escaping (CreateProductResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let params = addition.asDictionary()
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: ProductResources.updateProductAdditionResource(productId: productId, additionId: additionId), paramters: params, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    func updateProductOption(optionId: Int, productId: Int, option: ProductOptions, complation: @escaping (CreateProductResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let params = option.asDictionary()
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: ProductResources.updateProductOptionResource(productId: productId, optionId: optionId), paramters: params, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func deleteProductPrice(productId: Int, priceId: Int, complation: @escaping (GeneralResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: ProductResources.deletePriceResource(productId: productId, priceId: priceId), paramters: nil, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func deleteProductAddition(productId: Int, additionId: Int, complation: @escaping (GeneralResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: ProductResources.deleteAdditionResource(productId: productId, additionId: additionId), paramters: nil, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    func deleteProductOption(productId: Int, optionId: Int, complation: @escaping (GeneralResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: ProductResources.deleteOptionResource(productId: productId, optionId: optionId), paramters: nil, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func getOffers(productId: Int, complation: @escaping (GetOffersResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: ProductResources.getOffersResource(productId: productId), paramters: nil, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func deleteOffer(offerId: Int, complation: @escaping (GeneralResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: ProductResources.deleteOfferResource(offerId: offerId), paramters: nil, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func saveOffer(shopId: Int, productId: Int,priceId: Int, discount: Int, startDate: String, endDate: String, complation: @escaping (GeneralResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let params = ["shop_id": shopId, "product_id": productId, "price_id": priceId, "discount": discount, "start_date": startDate, "end_date": endDate] as [String : Any]
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: ProductResources.saveOfferResource, paramters: params, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
    
    func updateOffer(offerId: Int, shopId: Int, productId: Int,priceId: Int, discount: Int, startDate: String, endDate: String, complation: @escaping (GeneralResponse?) -> (), errorHandler: @escaping (ErrorCode) -> ())
    {
        if let token = Global.accesstoken, let countryCode = Global.countryCode
        {
            let headers = ["country-code": countryCode, "mobile-version": "iPhone", "app-version": GeneralMethods.getAppVersion()]
            let params = ["shop_id": shopId, "product_id": productId, "price_id": priceId, "discount": discount, "start_date": startDate, "end_date": endDate] as [String : Any]
            let sm = ServerManager ()
            _ = sm.httpConnect(resource: ProductResources.updateOfferResource(offerId: offerId), paramters: params, authentication: token, AdditionalHeaders: headers, complation:
                { (json, data) in
                    if let result = json
                    {
                        complation(result)
                    }
            })
            { (error, msg) in
                
                errorHandler(error)
            }
        }
    }
}
