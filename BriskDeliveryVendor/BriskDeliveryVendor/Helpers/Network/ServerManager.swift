//
//  ServerManager.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/1/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import Alamofire

enum ErrorCode:Int
{
    case Caneled = -999
    case NoInternet = -1009
    case UnKnown = 000
}

struct Resource<A>
{
    var url: String
    let httpmethod: Alamofire.HTTPMethod
    let parse: (Data) -> A?
}

class ServerManager: NSObject
{
    var request:DataRequest?
    //Authentication
    private var headers = ["Content-Type": "application/json", "Accept": "application/json", "Accept-Language": "\(Global.languageId ?? 1)"]
    //MARK: - HTTPHandling -
    func httpConnect<A>(resource:Resource<A>, paramters:[String:Any]?, authentication:String?,AdditionalHeaders : [String:String]? = [:] , complation: @escaping (A?, Any?) -> (), errorHandler: @escaping (ErrorCode, Any?) -> ()  ) -> DataRequest?
    {
        
        let url = resource.url
        print(url)
        if let auth = authentication
        {
            headers["Authorization"] = "Bearer \(auth)"
        }
        if AdditionalHeaders != nil {
            
            for header in AdditionalHeaders!{
                
                headers[header.key] = header.value
            }
        }
        request = Alamofire.request(url, method: resource.httpmethod, parameters: paramters, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<500).responseJSON
            { response in
                switch response.result
                {
                case .success:
                    if let result = response.response, (200 ... 299).contains(result.statusCode) {
                        
                   
                    let value = response.result.value
                    print(value)
                    GeneralMethods.handleUnauthenticated(data: value)
                    guard let responseData = response.data else {
                        complation(nil, value)
                        return
                    }
                    let parse = resource.parse
                    let result = parse(responseData)
                    
                    DispatchQueue.main.async
                        {
                            complation(result, value)
                    }
                    } else {
                        errorHandler(ErrorCode(rawValue: 000)!, someThingWentWrong)
                    }
                case .failure(let error):
                    //print(error._code)
                    print(error.localizedDescription)
                    DispatchQueue.main.async
                        {
                            if let errorEnum = ErrorCode(rawValue: error._code)
                            {
                                errorHandler(errorEnum, error)
                            }
                            else
                            {
                                errorHandler(ErrorCode(rawValue: 000)!, error)
                            }
                    }
                }
        }
        return request
    }
    
    func httpConnectWithArrayOfParams<A>(resource:Resource<A>, paramters:[[String:Any]]?, authentication:String?,AdditionalHeaders : [String:String]? = [:] , complation: @escaping (A?, Any?) -> (), errorHandler: @escaping (ErrorCode, Any?) -> ()) {
        
        if let auth = authentication
        {
            headers["Authorization"] = "Bearer \(auth)"
        }
        if AdditionalHeaders != nil {
            
            for header in AdditionalHeaders!{
                
                headers[header.key] = header.value
            }
        }
        
        var request = URLRequest(url: URL(string: resource.url)!)
       
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = resource.httpmethod.rawValue
        request.httpBody = try! JSONSerialization.data(withJSONObject: paramters, options: [])
        request.allHTTPHeaderFields = headers
        Alamofire.request(request).validate(statusCode: 200..<500).responseJSON{ response in
            switch response.result
            {
            case .success:
                let value = response.result.value
                GeneralMethods.handleUnauthenticated(data: value)
                guard let responseData = response.data else {
                    complation(nil, value)
                    return
                }
                let parse = resource.parse
                let result = parse(responseData)
                
                DispatchQueue.main.async
                    {
                        complation(result, value)
                }
            case .failure(let error):
                //print(error._code)
                print(error.localizedDescription)
                DispatchQueue.main.async
                    {
                        if let errorEnum = ErrorCode(rawValue: error._code)
                        {
                            errorHandler(errorEnum, error)
                        }
                        else
                        {
                            errorHandler(ErrorCode(rawValue: 000)!, error)
                        }
                }
            }
        }
    }
    
    func httpMultiPartFormConnect<A>(resource:Resource<A>, authentication : String?,AdditionalHeaders : [String:String]? = [:], paramters:[String:Any]?, mediaData: Data?, mediaMimeType : String, thumbnailData: Data? ,thumbMemiType: String = "image/jpeg", mediaParamName: String,complation: @escaping (A?, Any?) -> (), errorHandler: @escaping (ErrorCode, Any?) -> ()  )
    {
        
        let url = resource.url
        if let auth = authentication
        {
            headers["Authorization"] = "Bearer \(auth)"
        }
        if AdditionalHeaders != nil {
            
            for header in AdditionalHeaders!{
                
                headers[header.key] = header.value
            }
        }
        Alamofire.upload(multipartFormData: { (multiPartFormData) in
            
            if let params = paramters{
                for (key,val) in params{
                    multiPartFormData.append("\(val)".data(using: String.Encoding.utf8)!, withName: key)
                }
                
            }
            
            if let data = mediaData{
                multiPartFormData.append(data, withName: mediaParamName, fileName: "image1.jpg", mimeType: mediaMimeType)
            }
            
            if let data = thumbnailData {
                multiPartFormData.append(data, withName: "logo", fileName: "image2.jpg", mimeType: thumbMemiType)
            }
            
        }, to: url, method: resource.httpmethod, headers : headers ) { (encodedObject) in
            
            switch encodedObject {
                
            case .failure(let error):
                
                print(error.localizedDescription)
                DispatchQueue.main.async
                    {
                        if let errorEnum = ErrorCode(rawValue: error._code)
                        {
                            errorHandler(errorEnum, error)
                        }
                        else
                        {
                            errorHandler(ErrorCode(rawValue: 000)!, error)
                        }
                }
                
            case .success(request: let request, streamingFromDisk: _, streamFileURL: _ ):
                
                request.responseJSON(completionHandler:
                    
                    { (response) in
                        var statusCode = 0
                        print(response.value)
                GeneralMethods.handleUnauthenticated(data: response.result.value)
                        guard let responseData = response.data else {
                            complation(nil, 0)
                            return
                        }
                        let parse = resource.parse
                        let result = parse(responseData)
                        if let code = response.response?.statusCode
                        {
                            statusCode = code
                        }
                        DispatchQueue.main.async
                            {
                                complation(result, statusCode)
                        }
                })
            }
        }
    }
    
    func cancelRequest(dataRequest : DataRequest)
    {
        dataRequest.cancel()
    }
}

