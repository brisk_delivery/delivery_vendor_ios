//
//  RouterManager.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/23/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import CFAlertViewController

protocol Router {
    func popBack()
    func dismiss()
    func showAlertMessage(title: String, message: String, ActionButtons: [(title: String, style: CFAlertAction.CFAlertActionStyle, alignment: CFAlertAction.CFAlertActionAlignment, bgColor: UIColor, textColor: UIColor, handler: (_ action: CFAlertAction) -> ())])
}

class RouterManager: Router {
    
    var currentView: UIViewController
    
    init(_ currentView: UIViewController) {
        self.currentView = currentView
    }
    
    func popBack() {
        currentView.navigationController?.popViewController(animated: true)
    }
    
    func showAlertMessage(title: String, message: String, ActionButtons: [(title: String, style: CFAlertAction.CFAlertActionStyle, alignment: CFAlertAction.CFAlertActionAlignment, bgColor: UIColor, textColor: UIColor, handler: (_ action: CFAlertAction) -> ())]) {
        currentView.alert(title: title, message: message, ActionButtons: ActionButtons)
    }
    func dismiss() {
        currentView.dismiss(animated: true, completion: nil)
        
    }
}
