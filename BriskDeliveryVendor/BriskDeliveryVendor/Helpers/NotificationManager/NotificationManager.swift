//
//  NotificationManager.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 5/17/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class NotificationManager {
    static func handle(notification: [AnyHashable: Any]) {        
        print("NOTIFIDICT", notification)
        guard let notificationDictionary = notification as? [String: Any],
            let data = notificationDictionary["aps"] as? [String: Any],
            let payload = data["extraPayLoad"] as? [String: Any],
        let orderId = payload["order_id"] as? Int else {
            MainRouterManager.launchApplication(openLanguage: false)
            return
        }
        MainRouterManager.openOrders(with: orderId)
    }
}
