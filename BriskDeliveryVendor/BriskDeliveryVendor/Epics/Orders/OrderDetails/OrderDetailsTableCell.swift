//
//  OrderDetailsTableCell.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/21/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

class OrderDetailsTableCell: UITableViewCell {
    static let identifier = "OrderDetailsTableCell"
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    
    func configure(orderDetails: OrderProduct) {
        nameLabel.text = orderDetails.productName ?? ""
        sizeLabel.text = orderDetails.productSize ?? ""
        numberLabel.text = "\(orderDetails.count ?? 0)"
        priceLabel.text = "\(orderDetails.productPrice ?? "")AED"
    }

}



