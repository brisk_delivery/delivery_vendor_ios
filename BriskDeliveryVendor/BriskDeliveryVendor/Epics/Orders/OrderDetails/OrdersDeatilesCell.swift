//
//  OrdersDeatilesCell.swift
//  Brisk
//
//  Created by  Ahmed’s MacBook Pro on 3/17/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class OrdersDeatilesCell: UITableViewCell {
    
    @IBOutlet weak var optionsheightConstrain: NSLayoutConstraint!
    @IBOutlet weak var addOnesHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var optionsTableView: UITableView!
    @IBOutlet weak var addOnsTableView: UITableView!
    @IBOutlet weak var imgOrder: UIImageView!
    @IBOutlet weak var lbname: UILabel!
    @IBOutlet weak var lbQuantity: UILabel!
    @IBOutlet weak var lbPrice: UILabel!
    @IBOutlet weak var imageHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var imageWidthConstrain: NSLayoutConstraint!
    var VC = OrderDetailsViewController()
    var options = [OptionCategory]()
    var additions = [OrderAddition]()
    var optionsHeight: CGFloat {
        optionsTableView.layoutIfNeeded()
        
        return optionsTableView.contentSize.height
    }
    var addonesHeight: CGFloat {
        addOnsTableView.layoutIfNeeded()
        
        return addOnsTableView.contentSize.height
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    
    
}

extension OrdersDeatilesCell: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == optionsTableView{
            return options.count
        }else{
            print(additions.count)
            return additions.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == optionsTableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderOptionsTableViewCell", for: indexPath) as! orderOptionsTableViewCell
            let obj = options[indexPath.row]
            cell.titleLbl.text = obj.name
            print(obj.options!.count)
            cell.options = obj.options ?? [OrderOptions]()
            cell.tableViewHeight.constant = 50
            
            cell.layoutIfNeeded()
            cell.tableView.reloadData()
            
            cell.tableViewHeight.constant = cell.optionsHeight
            cell.layoutIfNeeded()

            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrdersAddOnsCell", for: indexPath) as! OrdersAddOnsCell
            let obj = additions[indexPath.row]
            cell.lbname.text = obj.productAdditionName
            cell.lbprice.text = "\(obj.productAdditionPrice ?? "0")" + " "
            //+ "\(self.ordersDeatiles?.currency ?? "")"

            return cell
            
        }
    }
    
}
