//
//  AddOnsTableCell.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/21/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

class AddOnsTableCell: UITableViewCell {
    static let identifier = "AddOnsTableCell"
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    
    func configure(addOns: OrderAddition) {
        nameLabel.text = addOns.productAdditionName ?? ""
        priceLabel.text = "\(addOns.productAdditionPrice ?? "")AED"
    }
    
}
