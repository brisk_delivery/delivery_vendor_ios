//
//  OrderDetailsViewController.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/21/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import Localize_Swift

protocol OrderDetailsDelegate: class {
    func refreshOrder()
}

class OrderDetailsViewController: UIViewController {
    static let identifier = "OrderDetailsViewController"
    @IBOutlet weak var noteLbl: UILabel!
    private var viewModel: OrderDetailsViewModel!
    @IBOutlet weak var totalPriceLbl: LocalizeLabel!
    var orderId: Int!
    var orderDetails: OrderDetails!
    weak var delegate: OrderDetailsDelegate!
    
    @IBOutlet weak var tableView: UITableView!
    var products = [OrderProduct]()
    var addOns = [OrderAddition]()
    var options = [OptionCategory]()
    @IBOutlet weak var tableViewHeightConstrain: NSLayoutConstraint!
    

    @IBOutlet  var actionButtons: [UIButton]!
  
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var paymentMethodLabel: UILabel!
    @IBOutlet var linesViews: [UIView]!
    @IBOutlet var statusViews: [UIView]!
    @IBOutlet weak var actionsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var backImage: UIImageView!
    
    @IBOutlet weak var containerScrollView: UIScrollView!
    
    var tableContentHeight: CGFloat {
        tableView.layoutIfNeeded()

        return tableView.contentSize.height
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = OrderDetailsViewModel(router: RouterManager(self), orderManager: OrderManager())
        initObservables()
        self.tableViewHeightConstrain.constant = CGFloat(1000)
        self.updateViewConstraints()
        viewModel.getOrderDetails(shopId: Global.shopId, orderId: orderId)
        setStyle()
    }
    
    private func setStyle()  {
        backImage.image = Localize.currentLanguage() == "ar"  ? #imageLiteral(resourceName: "ic_right_back"): #imageLiteral(resourceName: "ic_left_arrow")
    }
    
    private func initObservables() {
        viewModel.isLoading.bindAndFire { shouldLoad in
            shouldLoad ? self.showIndicator() : self.hideIndicator()
        }
        viewModel.enableEnteraction.bindAndFire{ isEnabled in
            self.view.isUserInteractionEnabled = isEnabled
        }
        viewModel.orderDetailsResponse.bind{ details in
            self.containerScrollView.isHidden = false
            self.orderDetails = details
            self.update()
        }
        viewModel.statusUpdated.bindAndFire{ isUpdated in
            if isUpdated {
                self.alert(title: "", message: "orderStatusUpdatedMessage".localized(), ActionButtons: [(title: "ok".localized(),style:.Default, alignment: .center, bgColor: #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1), textColor: .white, handler:{(action) in
                    self.viewModel.getOrderDetails(shopId: Global.shopId, orderId: self.orderId)
                }) ])
            }
        }
    }
    
    func update() {
        self.paymentMethodLabel.text = orderDetails.orderPaymentMethod ?? ""
        self.addressLabel.text = orderDetails.userAddress ?? ""
        self.totalPriceLbl.text = String(orderDetails.totalPrice ?? 0.00)
        addressLabel.sizeToFit()
        noteLbl.text = orderDetails.userComment ?? ""
        if let products = orderDetails.products {
            self.products = products
            self.addOns = products.filter{$0.addition != nil}.flatMap({$0.addition!})

//            self.addOns = products.filter{$0.addition != nil}.flatMap({$0.addition!})

//            self.tableViewHeightConstrain.constant = CGFloat(self.addOns.count * 30)
          
           
            self.tableView.reloadData()
            self.tableViewHeightConstrain.constant = self.tableContentHeight
            self.updateViewConstraints()
        }
        setOrderStatus()
        updateActions()
    }
    
    func setOrderStatus() {
        guard let orderStatus = orderDetails.orderStatus, let statusName = orderStatus.name else {
            return
        }
        if statusName == OrderStatusEnum.waiting.rawValue {
            UIView.animate(withDuration: 0.9) {
                self.statusViews.forEach {$0.backgroundColor = $0.tag == 0 ? #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1):#colorLiteral(red: 0.9150560498, green: 0.9150775075, blue: 0.9150659442, alpha: 1)}
                self.linesViews.forEach {$0.backgroundColor = $0.tag == 0 ? #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1):#colorLiteral(red: 0.9150560498, green: 0.9150775075, blue: 0.9150659442, alpha: 1)}
            }
        }else if statusName == OrderStatusEnum.process.rawValue {
            UIView.animate(withDuration: 0.9) {
                self.statusViews.forEach {$0.backgroundColor = $0.tag == 0 || $0.tag == 1 ? #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1):#colorLiteral(red: 0.9150560498, green: 0.9150775075, blue: 0.9150659442, alpha: 1)}
                self.linesViews.forEach {$0.backgroundColor = $0.tag == 0 || $0.tag == 1 ? #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1):#colorLiteral(red: 0.9150560498, green: 0.9150775075, blue: 0.9150659442, alpha: 1)}
            }
        }else if statusName == OrderStatusEnum.shipping.rawValue {
            UIView.animate(withDuration: 0.9) {
                self.statusViews.forEach {$0.backgroundColor = $0.tag == 0 || $0.tag == 1 || $0.tag == 2 ? #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1):#colorLiteral(red: 0.9150560498, green: 0.9150775075, blue: 0.9150659442, alpha: 1)}
                self.linesViews.forEach {$0.backgroundColor = $0.tag == 0 || $0.tag == 1 || $0.tag == 2 ? #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1):#colorLiteral(red: 0.9150560498, green: 0.9150775075, blue: 0.9150659442, alpha: 1)}
            }
        }else if statusName == OrderStatusEnum.closed.rawValue {
            UIView.animate(withDuration: 0.9) {
                self.statusViews.forEach {$0.backgroundColor = ($0.tag == 0 || $0.tag == 1 || $0.tag == 2 || $0.tag == 3) ? #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1):#colorLiteral(red: 0.9150560498, green: 0.9150775075, blue: 0.9150659442, alpha: 1)}
                self.linesViews.forEach {$0.backgroundColor = #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1)}
            }
        }else {
            UIView.animate(withDuration: 0.9) {
                self.statusViews.forEach {$0.backgroundColor =  #colorLiteral(red: 0.9150560498, green: 0.9150775075, blue: 0.9150659442, alpha: 1)}
                self.linesViews.forEach {$0.backgroundColor = #colorLiteral(red: 0.9150560498, green: 0.9150775075, blue: 0.9150659442, alpha: 1)}
            }
        }
    }
    
    func updateActions(){
        guard let order = orderDetails, let statusObj = order.orderStatus, let status = statusObj.name else {return}
        if status.lowercased() == OrderStatusEnum.waiting.rawValue {
            actionButtons.forEach{$0.isHidden = ($0.tag == 0 || $0.tag == 1) ? false : true}
            actionsViewHeight.constant = 60
        }else if status.lowercased() == OrderStatusEnum.process.rawValue {
            actionButtons.forEach{$0.isHidden = ($0.tag == 2) ? false : true}
            actionsViewHeight.constant = 60
        }else if status.lowercased() == OrderStatusEnum.shipping.rawValue {
            actionButtons.forEach{$0.isHidden = ($0.tag == 3) ? false : true}
            actionsViewHeight.constant = 60
        }else {
            actionButtons.forEach{$0.isHidden = true}
            actionsViewHeight.constant = 0
        }
        
    }

    @IBAction func backButtonAction(_ sender: UIButton) {
        delegate.refreshOrder()
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func acceptButtonAction(_ sender: UIButton) {
        viewModel.updateOrderStatus(shopId: Global.shopId, orderId: orderId, status: OrderStatusEnum.process.rawValue)
    }
    @IBAction func refuseButtonAction(_ sender: UIButton) {
        viewModel.updateOrderStatus(shopId: Global.shopId, orderId: orderId, status: OrderStatusEnum.refuse.rawValue)
    }
    
    @IBAction func onWayButtonAction(_ sender: UIButton) {
        viewModel.updateOrderStatus(shopId: Global.shopId, orderId: orderId, status: OrderStatusEnum.shipping.rawValue)
    }
    
    @IBAction func orderDeliveredButtonAction(_ sender: UIButton) {
        viewModel.updateOrderStatus(shopId: Global.shopId, orderId: orderId, status: OrderStatusEnum.closed.rawValue)
    }
    
}

extension OrderDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return products.count

  
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrdersDeatilesCell", for: indexPath) as! OrdersDeatilesCell
            let obj = products[indexPath.row]
            cell.lbname.text = obj.productName
            cell.lbPrice.text = "\(obj.productPrice ?? "0")" + " " + "\(orderDetails.currency ?? "")"
            cell.lbQuantity.text = "\(obj.count ?? 0)"
            cell.imgOrder.kf.setImage(with: URL(string: obj.imgPath ?? ""), placeholder: #imageLiteral(resourceName: "ic_product_default"))

        if obj.imgPath == "" || obj.imgPath == nil {
            cell.imageWidthConstrain.constant = 0
            cell.imageHeightConstrain.constant = 0
        }else{
            cell.imageWidthConstrain.constant = 120
            cell.imageHeightConstrain.constant = 92
        }
        print(obj.addition!.count)
        cell.options = obj.options ?? [OptionCategory]()
        cell.additions = obj.addition ?? [OrderAddition]()
        cell.optionsheightConstrain.constant = 500
        cell.addOnesHeightConstrain.constant = CGFloat(20 * obj.addition!.count)
        cell.layoutIfNeeded()

        cell.addOnsTableView.reloadData()
        cell.optionsTableView.reloadData()

        cell.addOnesHeightConstrain.constant = CGFloat(20 * obj.addition!.count)

        if cell.options.count == 0{
            cell.optionsheightConstrain.constant = 0

        }
        else{
//            cell.optionsheightConstrain.constant = cell.optionsHeight

            cell.optionsheightConstrain.constant = CGFloat(cell.optionsHeight)
            cell.layoutIfNeeded()


        }
        cell.layoutIfNeeded()
       
            return cell
       
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 30
//    }
}
