//
//  OrderDetailsViewModel.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/21/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class OrderDetailsViewModel: BaseViewModel {
    let orderManager: OrderManager
    var enableEnteraction: Dynamic<Bool> = Dynamic(true)
    var orderDetailsResponse: Dynamic<OrderDetails> = Dynamic(OrderDetails())
    var statusUpdated: Dynamic<Bool> = Dynamic(false)

    
    init(router: Router, orderManager: OrderManager) {
        self.orderManager = orderManager
        super.init(router: router, isLoading: false)
    }
    
    
    func getOrderDetails(shopId: Int, orderId: Int) {
        enableEnteraction.value = false
        startLoading()
        orderManager.getOrderDetails(shopId: Global.shopId, orderId: orderId,complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
            if let result = response, let orderDetails = result.data {
                self.orderDetailsResponse.value = orderDetails
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
    
    func updateOrderStatus(shopId: Int, orderId: Int, status: String) {
        enableEnteraction.value = false
        startLoading()
        orderManager.updateOrderStatus(shopId: shopId, orderId: orderId,status: status, complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
            if let result = response, let status = result.status, status == 1{
                self.getOrderDetails(shopId: shopId, orderId: orderId)
                self.statusUpdated.value = true
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
}

