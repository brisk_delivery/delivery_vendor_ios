//
//  OrdersViewController.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/20/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import Localize_Swift

@available(iOS 11.0, *)
class OrdersViewController: UIViewController {
    weak var listVC: OrdersListViewController!
    
    @IBOutlet weak var statusContainerView: CustomView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(setSemantic), name: .languageChanged, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSemantic()
    }
    
    @objc func setSemantic() {
        UIView.appearance().semanticContentAttribute = Localize.currentLanguage() == "ar"  ? .forceRightToLeft : .forceLeftToRight
        statusContainerView.corners = Localize.currentLanguage() == "ar"  ? "2" : "1"
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "OrderStatusListSegue", let vc = segue.destination as? OrderStatusViewController {
            vc.delegate = self
        } else if segue.identifier == "OrdersListSegue", let vc = segue.destination as? OrdersListViewController {
            self.listVC = vc
            vc.currentStatus = OrderStatusEnum.waiting.rawValue
        } 
    }
    
}

@available(iOS 11.0, *)
extension OrdersViewController: OrderStatusDelegate{
    func didSelectStatus(status: OrderStatusEnum) {
        listVC.updateList(status: status.rawValue)
    }
}

