//
//  OrderTableCell.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/20/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

class OrderTableCell: UITableViewCell {
    static let identifier = "OrderTableCell"
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var orderImage: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var imageContainerView: UIView!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        imageContainerView.dropShadow(opacity: 0.16, offSet: CGSize(width: 1, height: 1), radius: 1)
    }
    
    func configure(order: Order) {
        nameLabel.text = order.userName ?? ""
        let date = DateManager.convertMinutes(order.createdAt ?? "")
        timeLabel.text = "\("since".localized()) \(date)"
        idLabel.text = "\("orderNo".localized()): \(order.orderId ?? 0)"
         orderImage.kf.setImage(with: URL(string: order.image ?? ""), placeholder: #imageLiteral(resourceName: "ic_product_default"))
        guard let status = OrderStatusEnum.getOrderStatus(order.orderStatus ?? "") else {
            statusLabel.text = order.orderStatus ?? ""
            statusLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            return
        }
        statusLabel.text = status.rawValue.localized()
        statusLabel.textColor = status.statusColor
    }
    
   
    
}


