//
//  OrdersListViewModel.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/20/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class OrdersListViewModel: BaseViewModel {
    let orderManager: OrderManager
    var enableEnteraction: Dynamic<Bool> = Dynamic(true)
    var ordersResponse: Dynamic<[Order]> = Dynamic([Order]())
    var shouldPullToRefresh: Dynamic<Bool> = Dynamic(false)
    
    init(router: Router, orderManager: OrderManager) {
        self.orderManager = orderManager
        super.init(router: router, isLoading: false)
    }
    
    func getOrders(orderStatus: String) {
        guard let shopId = checkForShopId() else  {
            self.ordersResponse.value = [Order]()
            return
        }
        enableEnteraction.value = false
        startLoading()
        orderManager.getOrders(shopId: shopId, orderStatus: orderStatus,complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.shouldPullToRefresh.value = false
            if let result = response, let orders = result.data {
                self.ordersResponse.value = orders
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
        
}

