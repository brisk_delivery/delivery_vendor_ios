//
//  OrdersListViewController.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/20/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import ZVProgressHUD

class OrdersListViewController: UIViewController {
    private var viewModel: OrdersListViewModel!
    private var orders = [Order]() {
        didSet {
            tableView.isHidden = orders.count == 0 ? true : false
            noOrdersView.isHidden = orders.count == 0 ? false : true
        }
    }
    var currentStatus: String = ""
    
    
    @IBOutlet weak var tableView: UITableView!    
    @IBOutlet weak var noOrdersView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = OrdersListViewModel(router: RouterManager(self), orderManager: OrderManager())
        initObservables()
        viewModel.getOrders(orderStatus: currentStatus)
        tableView.es.addPullToRefresh {
            self.view.isUserInteractionEnabled = false
            self.viewModel.getOrders(orderStatus: self.currentStatus)
        }
    }
    
    private func initObservables() {
        viewModel.isLoading.bindAndFire { shouldLoad in
            shouldLoad ? self.showIndicator() : self.hideIndicator()
        }
        viewModel.enableEnteraction.bindAndFire{ isEnabled in
            self.view.isUserInteractionEnabled = isEnabled
        }
        viewModel.ordersResponse.bind{ orders in
            self.orders = orders
            self.tableView.reloadData()
        }
        viewModel.shouldPullToRefresh.bind { (shouldPullToRefresh) in
            if !shouldPullToRefresh {
                self.tableView.es.stopPullToRefresh()
            }
        }
    }
    
    func updateList(status: String) {
        orders.removeAll()
        tableView.reloadData()
        currentStatus = status
        viewModel.getOrders(orderStatus: status)
    }
    
    func openOrderDetails(orderId: Int) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: OrderDetailsViewController.identifier) as? OrderDetailsViewController {
            vc.delegate = self
            vc.orderId = orderId
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true, completion: nil)
        }
    }
    
}

extension OrdersListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: OrderTableCell.identifier, for: indexPath) as? OrderTableCell {
            cell.configure(order: orders[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        openOrderDetails(orderId: orders[indexPath.row].orderId ?? 0)
    }
}

extension OrdersListViewController: OrderDetailsDelegate {
    func refreshOrder() {
        viewModel.getOrders(orderStatus: currentStatus)
    }
}

enum OrderStatusEnum: String, CaseIterable {
    case closed
    case refuse
    case shipping
    case process
    case waiting
    
    var statusString: String {
        switch self {
        case .waiting:
            return "Waiting"
        case .process:
            return "Preparing"
        case .shipping:
            return "On way"
        case .closed:
            return "Completed"
        case .refuse:
            return "Canceled"
        }
    }
        
        var statusColor: UIColor {
            switch self {
            case .waiting:
                return #colorLiteral(red: 0, green: 0.6526059508, blue: 0.1863065362, alpha: 1)
            case .process:
                return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            case .shipping:
                return #colorLiteral(red: 0, green: 0.5966409445, blue: 0.1552188098, alpha: 1)
            case .closed:
                return #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1)
            case .refuse:
                return UIColor.red
            }
        }
    
    static func getOrderStatus(_ status: String)-> OrderStatusEnum? {
        guard let value = OrderStatusEnum.allCases.filter({$0.rawValue == status.lowercased()}).first else {
            return nil
        }
        return value
    }
    
    static func getOrderStatus(from senderTag: Int)-> OrderStatusEnum? {
        guard let value = OrderStatusEnum.allCases.enumerated().filter({(index, element) in index == senderTag}).first else {
            return nil
        }
        return value.element
    }
}

