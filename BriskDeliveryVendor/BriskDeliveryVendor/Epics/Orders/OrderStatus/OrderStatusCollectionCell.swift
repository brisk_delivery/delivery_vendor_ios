//
//  OrderStatusCollectionCell.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/20/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

class OrderStatusCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        nameLabel.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
    }
    
    func configure(status: String) {
        nameLabel.text = status
    }
    
}
