//
//  OrderStatusViewController.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/20/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import Localize_Swift

protocol OrderStatusDelegate: class {
    func didSelectStatus(status: OrderStatusEnum)
}

class OrderStatusViewController: UIViewController {
    static let identifier = "OrderStatusViewController"
    var orderStatusList = ["Completed","Canceled","On way","Preparing","Waiting"]
    var selectedOrderStatus: String!
    weak var delegate: OrderStatusDelegate?

    @IBOutlet var statusLabels: [UILabel]!
    @IBOutlet var statusSelectedViews: [UIView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
    }
    
    func initUI() {
        statusLabels.forEach{$0.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)}
        updateUI(with: 4)
    }
    
    func updateUI(with senderTag: Int) {
        statusLabels.forEach{$0.textColor = $0.tag == senderTag ? #colorLiteral(red: 0, green: 0.6526059508, blue: 0.1863065362, alpha: 1): #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)}
        statusSelectedViews.forEach{$0.isHidden = $0.tag == senderTag ? false: true }
    }
    
    @IBAction func selectStatusButtonAction(_ sender: UIButton) {
        updateUI(with: sender.tag)
        guard let status = OrderStatusEnum.getOrderStatus(from: sender.tag) else {
            return
        }
        delegate?.didSelectStatus(status: status)
    }
    
}
