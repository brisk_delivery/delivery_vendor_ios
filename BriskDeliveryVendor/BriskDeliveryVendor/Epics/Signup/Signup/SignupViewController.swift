//
//  SignupViewController.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/1/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import NKVPhonePicker
import Localize_Swift

class SignupViewController: UIViewController {
    static var identifier = "SignupViewController"
    var viewModel: SignupViewModel!
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: NKVPhonePickerTextField!
    @IBOutlet weak var termsTextView: UITextView!
    @IBOutlet var textFields: [UITextField]!
    @IBOutlet weak var backImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        viewModel = SignupViewModel(router: RouterManager(self), authenticationManager: AuthenticationManager())
        initObservables()
        phoneTextField.phonePickerDelegate = self
    }
    
    //MARK:- UI
    func initUI() {
        termsTextView.attributedText = setTermsAttributedString()
        let country = Country(countryCode: "AE", phoneExtension: "971")
        phoneTextField.country = country
        UIView.appearance().semanticContentAttribute = Localize.currentLanguage() == "ar"  ? .forceRightToLeft : .forceLeftToRight
        termsTextView.semanticContentAttribute = Localize.currentLanguage() == "ar" ? .forceRightToLeft : .forceLeftToRight
        backImage.image = Localize.currentLanguage() == "ar"  ? #imageLiteral(resourceName: "ic_right_back"): #imageLiteral(resourceName: "ic_left_arrow")
    }
    
    func setTermsAttributedString() -> NSMutableAttributedString {
        let attributedText = "terms".localized().formatAsAttributedString(fontSize: 13, fontColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), boldStringArray: [(string: "Terms&Conditions".localized(), fontSize: 13, fontColor: #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1))])
        attributedText.setAsLink(textToFind: "Terms&Conditions".localized())
        return attributedText
    }
    
     //MARK:- Observables
    func initObservables() {
        viewModel.isLoading.bindAndFire { shouldLoad in
            shouldLoad ? self.showIndicator() : self.hideIndicator()
        }
        viewModel.enableEnteraction.bindAndFire{ isEnabled in
            self.view.isUserInteractionEnabled = isEnabled
        }
        viewModel.toVerificationCode.bindAndFire{ toVerificationCode in
            let phoneComponent = self.getPhoneComponent()
            if toVerificationCode, let vc = VerificationCodeViewController.create(phoneNumber: phoneComponent.phone, countryCode: String(phoneComponent.code)) {
                self.navigationController?.pushViewController(vc, animated: true)}
        }
        viewModel.toHome.bindAndFire { toHome in
            if toHome {
                MainRouterManager.openAsRootViewController(stroyboardName: "Main", viewController: MainViewController.identifier)
            }
        }
    }
    
    //MARK:- IBActions
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signupButtonAction(_ sender: UIButton) {
        view.endEditing(true)
        if isValidInputs() {
            let phoneComponent = getPhoneComponent()
            viewModel.performSignup(firstName: firstNameTextField.text!, lastName: lastNameTextField.text!, country: phoneComponent.country, phoneCode: phoneComponent.code, mobileNumber: phoneComponent.phone, email: emailTextField.text!, password: passwordTextField.text!)
        }
    }
    
    //MARK:- ValidInputs
    func isValidInputs() -> Bool {
        var isValid = true
        if let name = firstNameTextField.text, name.isEmpty {
            firstNameTextField.setTextFieldBorder(borderColor: .red, borderWidth: 1)
            isValid = false
        }
        
        if let name = lastNameTextField.text, name.isEmpty {
            lastNameTextField.setTextFieldBorder(borderColor: .red, borderWidth: 1)
            isValid = false
        }
        
        if let phoneNumber = phoneTextField.phoneNumber, let code = phoneTextField.code, phoneNumber == code {
            phoneTextField.setTextFieldBorder(borderColor: .red, borderWidth: 1)
            isValid = false
        }
        
        if (emailTextField.text!.isEmpty) || (!emailTextField.text!.isValidEmail()) {
            emailTextField.setTextFieldBorder(borderColor: .red, borderWidth: 1)
            isValid = false
        }
        
        if let password = passwordTextField.text, password.isEmpty {
            passwordTextField.setTextFieldBorder(borderColor: .red, borderWidth: 1)
            isValid = false
        }
        return isValid
    }
    
    func getPhoneComponent()-> (phone: String, country: String, code: Int){
        if let phoneWithCode = phoneTextField.phoneNumber, let code = phoneTextField.code , let country = phoneTextField.country {
            let phoneNumber = phoneWithCode.components(separatedBy: code)[1]
            return (phoneNumber, country.countryCode, Int(code) ?? 0)
        }
        return ("","",0)
    }
}

extension SignupViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.setTextFieldBorder(borderColor: #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1), borderWidth: 1)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.setTextFieldBorder(borderColor: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), borderWidth: 1)
    }
}

extension SignupViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        let vc = GeneralWebView.create(type: .termsConditions)
        self.navigationController?.pushViewController(vc, animated: true)
        return false
    }
}
