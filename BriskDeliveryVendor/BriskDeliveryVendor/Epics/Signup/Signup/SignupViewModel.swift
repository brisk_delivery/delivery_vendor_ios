//
//  SignupViewModel.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/2/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class SignupViewModel: BaseViewModel {
    let authenticationManager: AuthenticationManager
    var enableEnteraction: Dynamic<Bool> = Dynamic(true)
    var toVerificationCode: Dynamic<Bool> = Dynamic(false)
    var toHome: Dynamic<Bool> = Dynamic(false)
    
    init(router: Router, authenticationManager: AuthenticationManager) {
        self.authenticationManager = authenticationManager
        super.init(router: router, isLoading: false)
    }
    
 
    func performSignup(firstName : String, lastName: String ,country : String, phoneCode: Int , mobileNumber: String, email: String,password: String) {
        enableEnteraction.value = false
        startLoading()
        authenticationManager.signup(firstName: firstName, lastName: lastName, country: country, phoneCode: phoneCode, mobileNumber: mobileNumber, email: email, password: password, complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
            if let responseData = response, responseData.status == 1 {
                if let authUser = responseData.data, let user = authUser.user , let token = authUser.token {
                Global.user = user
                Global.accesstoken = token
                Global.countryCode = "\(phoneCode)"
                    self.toHome.value = true
                } else {
                    self.toVerificationCode.value = true
                }
            } else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
}
