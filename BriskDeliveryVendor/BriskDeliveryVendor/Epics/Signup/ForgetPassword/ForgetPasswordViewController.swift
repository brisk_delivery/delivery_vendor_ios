//
//  ForgetPasswordViewController.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/5/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import Localize_Swift

class ForgetPasswordViewController: UIViewController {
    static var identifier = "ForgetPasswordViewController"
    private var viewModel: ForgetPasswordViewModel!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var backImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ForgetPasswordViewModel(router: RouterManager(self),authenticationManager: AuthenticationManager())
        initObservables()
        setStyle()
    }
    
    private func setStyle()  {
        backImage.image = Localize.currentLanguage() == "ar"  ? #imageLiteral(resourceName: "ic_right_back"): #imageLiteral(resourceName: "ic_left_arrow")
    }
    
    //MARK:- Observables
    func initObservables() {
        viewModel.isLoading.bindAndFire { shouldLoad in
            shouldLoad ? self.showIndicator() : self.hideIndicator()
        }
        viewModel.enableEnteraction.bindAndFire{ isEnabled in
            self.view.isUserInteractionEnabled = isEnabled
        }
    }
    
    //MARK:- ValidInputs
    func isValidInputs() -> Bool {
        var isValid = true
        if (emailTextField.text!.isEmpty) || (!emailTextField.text!.isValidEmail()) {
            emailTextField.setTextFieldBorder(borderColor: .red, borderWidth: 1)
            isValid = false
        }
        return isValid
    }
    
    //MARK:- IBActions
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func continueButtonAction(_ sender: UIButton) {
        if isValidInputs() {
            viewModel.performForgetPassword(email: emailTextField.text!)
        }        
    }
    
}

extension ForgetPasswordViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.setTextFieldBorder(borderColor: #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1), borderWidth: 1)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.setTextFieldBorder(borderColor: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), borderWidth: 1)
    }
}
