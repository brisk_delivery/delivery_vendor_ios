//
//  ForgetPasswordViewModel.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/5/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class ForgetPasswordViewModel: BaseViewModel {
    let authenticationManager: AuthenticationManager
    var enableEnteraction: Dynamic<Bool> = Dynamic(true)
    
    init(router: Router, authenticationManager: AuthenticationManager) {
        self.authenticationManager = authenticationManager
        super.init(router: router, isLoading: false)
    }
    
    
    func performForgetPassword(email: String) {
        enableEnteraction.value = false
        startLoading()
        authenticationManager.forgetPassword(email: email, complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
            if let responseData = response, responseData.status == 1 {
//                self.toVerificationCode.value = true
//                print(responseData.data?.code)
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
}
