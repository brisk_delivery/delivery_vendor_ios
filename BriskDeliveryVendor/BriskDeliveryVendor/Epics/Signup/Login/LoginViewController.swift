//
//  LoginViewController.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/1/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import NKVPhonePicker
import Localize_Swift

class LoginViewController: UIViewController {
    static var identifier = "LoginViewController"
    var viewModel: LoginViewModel!
    
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var phoneTextField: NKVPhonePickerTextField!
    @IBOutlet var textFields: [UITextField]!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var backImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneTextField.phonePickerDelegate = self
        viewModel = LoginViewModel(router: RouterManager(self),authenticationManager: AuthenticationManager(), homeManager: HomeManager())
        initObservables()
         initUI()
    }
    
    func initUI() {
        signupButton.underline()
        let country = Country(countryCode: "AE", phoneExtension: "971")
        phoneTextField.country = country
            backImage.image = Localize.currentLanguage() == "ar"  ? #imageLiteral(resourceName: "ic_right_back"): #imageLiteral(resourceName: "ic_left_arrow")
    }
    
    //MARK:- Observables
    func initObservables() {
        viewModel.isLoading.bindAndFire { shouldLoad in
            shouldLoad ? self.showIndicator() : self.hideIndicator()
        }
        viewModel.enableEnteraction.bindAndFire{ isEnabled in
            self.view.isUserInteractionEnabled = isEnabled
        }
        viewModel.toHome.bindAndFire { toHome in
            if toHome {
                MainRouterManager.openAsRootViewController(stroyboardName: "Main", viewController: MainViewController.identifier)
            }
        }
    }
    
    //MARK:- ValidInputs
    func isValidInputs() -> Bool {
        var isValid = true
        if let phoneNumber = phoneTextField.phoneNumber, let code = phoneTextField.code, phoneNumber == code {
            phoneTextField.setTextFieldBorder(borderColor: .red, borderWidth: 1)
            isValid = false
        }
        
        if let password = passwordTextField.text, password.isEmpty {
            passwordTextField.setTextFieldBorder(borderColor: .red, borderWidth: 1)
            isValid = false
        }
        return isValid
    }
    
    func getPhoneComponent()-> (phone: String, country: String, code: String){
        if let phoneWithCode = phoneTextField.phoneNumber, let code = phoneTextField.code , let country = phoneTextField.country {
            let phoneNumber = phoneWithCode.components(separatedBy: code)[1]
            return (phoneNumber, country.countryCode, code ?? "")
        }
        return ("","","")
    }
    
    //MARK:- IBActions
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func loginButtonAction(_ sender: Any) {
        view.endEditing(true)
        if isValidInputs() {
            let phoneComponents = getPhoneComponent()
            viewModel.performLogin(countryCode: phoneComponents.code,mobileNumber: phoneComponents.phone, password: passwordTextField.text!)
        }
    }
    
    @IBAction func signupButtonAction(_ sender: Any) {
        MainRouterManager.pushViewController(SignupViewController.identifier, storyBoard: "Signup", navigationController: navigationController!)
    }
    
    @IBAction func forgetButtonAction(_ sender: Any) {
        MainRouterManager.pushViewController(ForgetPasswordViewController.identifier, storyBoard: "Signup", navigationController: navigationController!)
    }
    
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.setTextFieldBorder(borderColor: #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1), borderWidth: 1)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.setTextFieldBorder(borderColor: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), borderWidth: 1)
    }
}

