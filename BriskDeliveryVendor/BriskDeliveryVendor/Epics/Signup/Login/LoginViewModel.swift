//
//  LoginViewModel.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/5/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class LoginViewModel: BaseViewModel {
    let authenticationManager: AuthenticationManager
    let homeManager: HomeManager
    var enableEnteraction: Dynamic<Bool> = Dynamic(true)
    var toHome: Dynamic<Bool> = Dynamic(false)
    
    init(router: Router, authenticationManager: AuthenticationManager, homeManager: HomeManager) {
        self.authenticationManager = authenticationManager
        self.homeManager = homeManager
        super.init(router: router, isLoading: false)
    }
    
    
    func performLogin(countryCode: String, mobileNumber: String,password: String) {
        enableEnteraction.value = false
        startLoading()
        authenticationManager.performLogin(countryCode: countryCode, mobileNumber: mobileNumber, password: password, complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
            if let responseData = response, responseData.status == 1, let authUser = responseData.data, let user = authUser.user , let token = authUser.token {
                Global.user = user
                Global.accesstoken = token
                Global.countryCode = countryCode
                if let shopId = user.shopId, shopId != 0 {
                    Global.shopId = shopId
                    self.getShopData()
                }
                self.toHome.value = true
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
            
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
    
    func getShopData() {
           guard let shopId = Global.shopId  else {
               return
           }
           DispatchQueue.global(qos: .background).async {
               self.homeManager.getShop(shopId: shopId, complation: { response in
                   if let result = response, let shop = result.data {
                     Global.shop = shop
                   }
               }) { error in }
           }
       }
}

