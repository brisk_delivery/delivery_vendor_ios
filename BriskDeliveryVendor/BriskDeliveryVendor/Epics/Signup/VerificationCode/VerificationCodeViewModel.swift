//
//  VerificationCodeViewModel.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/4/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class VerificationCodeViewModel: BaseViewModel {
    let authenticationManager: AuthenticationManager
    var enableEnteraction: Dynamic<Bool> = Dynamic(true)
    var toHome: Dynamic<Bool> = Dynamic(false)
    
    init(router: Router, authenticationManager: AuthenticationManager) {
        self.authenticationManager = authenticationManager
        super.init(router: router, isLoading: false)
    }
    
    
    func applyVerificationCode(mobileNumber : String ,code : Int, countryCode: String) {
        enableEnteraction.value = false
        startLoading()
        authenticationManager.verificationCode(mobileNumber: mobileNumber, code: code, complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
            if let responseData = response, responseData.status == 1, let authUser = responseData.data, let user = authUser.user , let token = authUser.token {
                Global.user = user
                Global.accesstoken = token
                Global.countryCode = countryCode
                self.toHome.value = true
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
    
    func resendVerificationCode(mobileNumber : String) {
        enableEnteraction.value = false
        startLoading()
        authenticationManager.resendVerificationCode(mobileNumber: mobileNumber, complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
            if let responseData = response, responseData.status == "Success" {
//                LoginModel.getInstance.saveUser(authUser)
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
    
}

