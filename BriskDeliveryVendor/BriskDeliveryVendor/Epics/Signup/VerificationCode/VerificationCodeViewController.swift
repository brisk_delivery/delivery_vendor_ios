//
//  VerificationCodeViewController.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/4/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import KWVerificationCodeView
import Localize_Swift

class VerificationCodeViewController: UIViewController {
    static var identifier = "VerificationCodeViewController"
    private var viewModel: VerificationCodeViewModel!
    private var phoneNumber: String!
    private var countryCode: String!
    
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var verificationCodeView: KWVerificationCodeView!
    @IBOutlet weak var backImage: UIImageView!
    
    
    static func create(phoneNumber: String, countryCode: String) -> VerificationCodeViewController? {
        if let viewController = UIStoryboard(name: "Signup", bundle: nil).instantiateViewController(withIdentifier: identifier) as? VerificationCodeViewController {
            viewController.phoneNumber = phoneNumber
            viewController.countryCode = countryCode
            return viewController
        }
        return nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = VerificationCodeViewModel(router: RouterManager(self), authenticationManager: AuthenticationManager())
        initObservables()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        initUI()
    }
    
    private func initUI() {
        resendButton.dropShadow(opacity: 0.36, offSet: CGSize(width: 5, height: 5), radius: 5)
        backImage.image = Localize.currentLanguage() == "ar"  ? #imageLiteral(resourceName: "ic_right_back"): #imageLiteral(resourceName: "ic_left_arrow")
    }
    
    //MARK:- Observables
    func initObservables() {
        viewModel.isLoading.bindAndFire { shouldLoad in
            shouldLoad ? self.showIndicator() : self.hideIndicator()
        }
        viewModel.enableEnteraction.bindAndFire{ isEnabled in
            self.view.isUserInteractionEnabled = isEnabled
        }
        
        viewModel.toHome.bindAndFire { toHome in
            if toHome {
                MainRouterManager.openAsRootViewController(stroyboardName: "Main", viewController: MainViewController.identifier)
            }
        }
    }
    
    //MARK:- IBActions
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func continueButtonAction(_ sender: UIButton) {
        viewModel.applyVerificationCode(mobileNumber: phoneNumber, code: Int(verificationCodeView!.getVerificationCode()) ?? 0, countryCode: countryCode)
    }
    
    @IBAction func resendButtonAction(_ sender: UIButton) {
        viewModel.resendVerificationCode(mobileNumber: phoneNumber)
    }
    
}
