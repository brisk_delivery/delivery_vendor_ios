//
//  LanguageViewController.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/14/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import Localize_Swift
import Siren

enum CommingFrom {
    case settings
    case signup
}

class LanguageViewController: UIViewController {
    @IBOutlet var languageLabels: [LocalizeLabel]!
    @IBOutlet var languageImages: [UIImageView]!
    @IBOutlet var backImage: UIImageView!
    @IBOutlet var backButton: UIButton!
    
    var commingFrom: CommingFrom = .signup
    static let identifier = "LanguageViewController"

    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
    }
    
    func initUI() {
        backImage.isHidden = commingFrom == .signup ? true: false
        backImage.isUserInteractionEnabled = commingFrom == .signup ? false: true
        setLanguage()
        setStyle()
    }
    
    func setStyle()  {
        backImage.image = Localize.currentLanguage() == "ar"  ? #imageLiteral(resourceName: "ic_right_back"): #imageLiteral(resourceName: "ic_left_arrow")
        UIView.appearance().semanticContentAttribute = Localize.currentLanguage() == "ar"  ? .forceRightToLeft : .forceLeftToRight
        languageLabels.forEach {$0.text = $0.localizeKey?.localized()}
        
    }
    
    func setLanguage() {
        guard let languageId = Global.languageId else {
            return
        }
        languageLabels.forEach{$0.textColor = languageId == $0.tag ? #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1):#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
         }
        languageImages.forEach{$0.image = languageId == $0.tag ? #imageLiteral(resourceName: "radio_selected"):#imageLiteral(resourceName: "radio")
        }
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func changeLanguageButtonAction(_ sender: UIButton) {
        Global.languageId = sender.tag
        Localize.setCurrentLanguage(sender.tag == 1 ? "en": "ar")
        Siren.shared.presentationManager = PresentationManager(forceLanguageLocalization: Localize.currentLanguage() == "ar" ? .arabic : .english)
        NotificationCenter.default.post(name: .languageChanged, object: nil)
        languageLabels.forEach{$0.textColor = sender.tag == $0.tag ? #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1):#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
         }
        languageImages.forEach{$0.image = sender.tag == $0.tag ? #imageLiteral(resourceName: "radio_selected"):#imageLiteral(resourceName: "radio")
        }
        if commingFrom == .signup {
    MainRouterManager.pushViewController(wellcomeViewController.identifier, storyBoard: "Signup", navigationController: navigationController!)
        } else {
            MainRouterManager.launchApplication(openLanguage: false)
        }
    }
    
}


