//
//  wellcomeViewController.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 3/31/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import Localize_Swift

class wellcomeViewController: UIViewController {
    static let identifier = "wellcomeViewController"
    override func viewDidLoad() {
        super.viewDidLoad()
        setStyle()
    }
    
    func setStyle()  {
        UIView.appearance().semanticContentAttribute = Localize.currentLanguage() == "ar"  ? .forceRightToLeft : .forceLeftToRight
    }
    

    @IBAction func loginButtonAction(_ sender: UIButton) {
        MainRouterManager.pushViewController(LoginViewController.identifier, storyBoard: "Signup", navigationController: navigationController!)
        
    }
    
    
    @IBAction func SignupButtonAction(_ sender: UIButton) {
        MainRouterManager.pushViewController(SignupViewController.identifier, storyBoard: "Signup", navigationController: navigationController!)
    }
    
    @IBAction func emailButtonAction(_ sender: UIButton) {
        let email = "info@briskgroup.net"
        if let url = URL(string: "mailto:\(email)") {
          if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
          } else {
            UIApplication.shared.openURL(url)
          }
        }
    }
    
    @IBAction func facebookButtonAction(_ sender: UIButton) {
        if let facebookURL = URL(string: "https://www.facebook.com/briskfamily.uae/?modal=admin_todo_tour"){
            UIApplication.shared.openURL(facebookURL)}
    }
    
    @IBAction func whatsappButtonAction(_ sender: UIButton) {
       
            let whatsappLink = "https://wa.me/+971504408583?text= "
            guard let url = URL(string: whatsappLink.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!), UIApplication.shared.canOpenURL(url) else {
                return
            }
            DispatchQueue.main.async {
                UIApplication.shared.open(url)
            }
    }
    
    @IBAction func instagramButtonAction(_ sender: UIButton) {
        guard let instagram = URL(string: "https://www.instagram.com/briskeats.uae/") else { return }
        UIApplication.shared.open(instagram)
    }
    
    @IBAction func phoneButtonAction(_ sender: UIButton) {
       guard let number = URL(string: "tel://" + "971504408583") else { return }
        UIApplication.shared.open(number)
    }
    
}

