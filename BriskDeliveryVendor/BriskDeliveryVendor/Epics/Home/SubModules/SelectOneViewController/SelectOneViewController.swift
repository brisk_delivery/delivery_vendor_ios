//
//  SelectOneViewController.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/8/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

class GeneralItem: Equatable {
    var id: Int
    var title: String
    
    init(id: Int, title: String) {
        self.id = id
        self.title = title
    }
    
    public static func == (lhs: GeneralItem, rhs: GeneralItem) -> Bool {
        return lhs.id == rhs.id
    }
}

class SelectOneViewController: UIViewController {
    static var identifier = "SelectOneViewController"
    @IBOutlet weak var tableView: UITableView!
    
    var selectItemHandler: ((GeneralItem) -> ())!
    private var list: [GeneralItem]!
    private var selectedItem: GeneralItem!
    private var listTitle: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.allowsSelection = true
    }
    
    static func create(_ list: [GeneralItem], selectItemHandler: @escaping (GeneralItem) -> ()) -> SelectOneViewController {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: identifier) as! SelectOneViewController
        vc.list = list
        vc.selectItemHandler = selectItemHandler
        return vc
    }
    
}
    
extension SelectOneViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = list[indexPath.row]
        var identifier = ""
        if let selected = selectedItem, item == selected {
            identifier = "SelectedItemCell"
        } else {
            identifier = "NotSelectedCell"
        }
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? SelectOneTableViewCell {
            cell.configure(option: list[indexPath.row].title)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedItem = list[indexPath.row]
        tableView.reloadData()
        selectItemHandler(selectedItem)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
}
