//
//  SelectOneTableViewCell.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/8/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

class SelectOneTableViewCell: UITableViewCell {
    
    @IBOutlet weak var optionName: UILabel!
    
    func configure(option: String) {
        optionName.text = option
    }
}
