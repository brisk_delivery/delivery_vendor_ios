//
//  HomeTabsWithAddView.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/13/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import Localize_Swift

enum HomeTabsType: String {
    case HomeTabsWithAddView
    case HomeTabs
}

protocol HomeTabsDelegate: class {
    func addShop()
    func openOrders()
    func openProducts()
    func openSettings()
    func openHome()
}

class HomeTabsWithAddView: UIView {
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet var labels: [LocalizeLabel]!
    @IBOutlet  var images: [UIImageView]!
    
    weak var delegate: HomeTabsDelegate!
   var selectedImages: [UIImage] = [#imageLiteral(resourceName: "ic_home"),#imageLiteral(resourceName: "ic_order"),#imageLiteral(resourceName: "ic_product"),#imageLiteral(resourceName: "ic_settings")]
    var blackImages: [UIImage] = [#imageLiteral(resourceName: "ic_home_black"),#imageLiteral(resourceName: "ic_order_black"),#imageLiteral(resourceName: "ic_product_black"),#imageLiteral(resourceName: "ic_settings_black")]
    var colors: [UIColor] = [#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), UIColor.clear, #colorLiteral(red: 0.9688121676, green: 0.9688346982, blue: 0.9688225389, alpha: 1), #colorLiteral(red: 0.9688121676, green: 0.9688346982, blue: 0.9688225389, alpha: 1)]
    
    init(type: HomeTabsType, delegate:  HomeTabsDelegate) {
        self.delegate = delegate
        super.init(frame: UIScreen.main.bounds)
        let view = Bundle.main.loadNibNamed(type.rawValue, owner: self, options: nil)?.first as? UIView
        addSubview(view ?? UIView())
        view?.frame = bounds
        view?.autoresizingMask = [
            UIView.AutoresizingMask.flexibleWidth,
            UIView.AutoresizingMask.flexibleHeight
        ]
        initUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func initUI() {
        containerView?.dropShadow(opacity: 0.96, offSet: CGSize(width: 6, height: 6), radius: 6)
    }
    
    private func updateUI(tag: Int) {
        labels.forEach{$0.textColor = $0.tag == tag ? #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)}
        for (index,item) in images.enumerated() {
            item.image = item.tag == tag ? selectedImages[index]: blackImages[index]
        }
        colorView?.backgroundColor = colors[tag]
    }
    
    func updateLabelsLocalization() {
        labels.forEach {$0.text = $0.localizeKey?.localized()}
    }
    
    @IBAction func addButtonAction(_ sender: UIButton) {
        delegate?.addShop()
    }
    
    @IBAction func openOrdersButtonAction(_ sender: UIButton) {
        updateUI(tag: sender.tag)
        delegate?.openOrders()
    }
    
    @IBAction func openProductsButtonAction(_ sender: UIButton) {
        updateUI(tag: sender.tag)
        delegate?.openProducts()
    }
    
    @IBAction func openSettingsButtonAction(_ sender: UIButton) {
        updateUI(tag: sender.tag)
        delegate?.openSettings()
    }
    
    @IBAction func openHomeButtonAction(_ sender: UIButton) {
        updateUI(tag: sender.tag)
        delegate?.openHome()
    }
    func chageTap(page tag: Int){
        updateUI(tag: tag)

    }
}


