//
//  ArcView.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/24/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

class ArcView: UIView {
    var path: UIBezierPath!
    
    override func draw(_ rect: CGRect) {
        self.createRectangle()
        UIColor.clear.setFill()
        path.fill()
        UIColor.lightGray.setStroke()
        path.stroke()
        dropShadow(opacity: 0.86, offSet: CGSize(width: 1, height: 6), radius: 6)
    }
    
    func createRectangle() {
        path = UIBezierPath()
        path.move(to: CGPoint(x: 0.0, y: 0.0))
        path.addLine(to: CGPoint(x: (self.frame.size.width / 2) - 30, y: 0.0))
        let arcPath = UIBezierPath(arcCenter: CGPoint(x: self.frame.size.width/2, y: 0.0),
                                   radius: 30,
                                   startAngle: CGFloat(180.0).toRadians(),
                                   endAngle: CGFloat(0.0).toRadians(),
                                   clockwise: false)
        path.append(arcPath)
        path.move(to: CGPoint(x: (self.frame.size.width / 2) + 30, y: 0.0))
        path.addLine(to: CGPoint(x: self.frame.size.width, y: 0.0))
    }

}

extension CGFloat {
    func toRadians() -> CGFloat {
        return self * CGFloat(M_PI) / 180.0
    }
}

