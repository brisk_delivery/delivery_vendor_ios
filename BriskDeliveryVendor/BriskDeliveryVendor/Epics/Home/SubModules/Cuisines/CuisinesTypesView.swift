//
//  CuisinesTypesView.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/14/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

protocol CuisinesTypesDelegate: class {
    func didSelectCuisines(Cuisines: [Tag])
    func cuisinesViewheightDidChange(height: CGFloat)
}

class CuisinesTypesView: UIViewController {
    static let identifier = "CuisinesTypesView"
    var isUpdate = false
    var tagsList = [Tag]()
    var selectedTags = [Tag]()
    var selectedIds = [Int]()
    var cuisinesLabelHeight: CGFloat = 25
    var viewModel: CuisinesTypesViewModel!
    var categoryId: Int?
    weak var delegate: CuisinesTypesDelegate!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = CuisinesTypesViewModel(router: RouterManager(self), homeManager: HomeManager())
        getCuisuines(categoryId: categoryId)
        viewModel.tagsResponse.bind { [weak self](tags) in
            self?.tagsList = tags
            self?.updateSelectedWhenUpdate(tags: tags)
            self?.collectionView.reloadData()
            let height =  (self?.collectionView.collectionViewLayout.collectionViewContentSize.height)! + self!.cuisinesLabelHeight
            self?.delegate.cuisinesViewheightDidChange(height: height)
        }
//        if let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout{
//            layout.minimumInteritemSpacing = 5
//            layout.minimumLineSpacing = 5
//        }
    }
    
    func getCuisuines(categoryId: Int?) {
        viewModel.getCuisuinesTypes(categoryId: categoryId)
    }
    
    func updateSelectedWhenUpdate(tags: [Tag]) {
        guard isUpdate else {
            return
        }
        
        self.selectedIds.forEach{ id in
            tags.forEach{
                if $0.tagId == id {selectedTags.append($0)}
            }
        }
        
        if selectedTags.count > 0 {delegate.didSelectCuisines(Cuisines: selectedTags)}
    }
    
    func isSelected(_ tag: Tag) -> Int? {
        if let index = (self.selectedTags.index { (tagType) -> Bool in
            tagType == tag
        }) {
            return index
        } else {
            return nil
        }
    }
    
}

extension CuisinesTypesView: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tagsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let tag = tagsList[indexPath.row]
        let identifier = isSelected(tag) != nil ? "TagSelectedCell": "TagNotSelectedCell"
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as? CuisineCollectionCell {
            cell.configure(tag: tag)
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let payment = tagsList[indexPath.row]
        if let index = isSelected(payment) {
            selectedTags.remove(at: index)
        } else {
            selectedTags.append(payment)
        }
        delegate.didSelectCuisines(Cuisines: selectedTags)
        collectionView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let tag = tagsList[indexPath.row]
        let tagWidth = (tag.tagName ?? "").width(withConstrainedHeight: 40, font: UIFont(name: "Roboto-Bold", size: 15)!)
        return CGSize(width: tagWidth + 25, height: 50)
    }
    
}

