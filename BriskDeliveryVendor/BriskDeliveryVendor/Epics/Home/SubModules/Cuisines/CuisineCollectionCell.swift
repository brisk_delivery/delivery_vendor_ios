//
//  CuisineCollectionCell.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/14/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

class CuisineCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = containerView.frame.height / 2
    }
    
    func configure(tag: Tag) {
        nameLabel.text = tag.tagName ?? ""
    }
    
}

