//
//  CuisinesTypesViewModel.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/15/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class CuisinesTypesViewModel: BaseViewModel {
    let homeManager: HomeManager
    var tagsResponse: Dynamic<[Tag]> = Dynamic([Tag]())
    
    init(router: Router, homeManager: HomeManager) {
        self.homeManager = homeManager
        super.init(router: router, isLoading: false)
    }
    
    func getCuisuinesTypes(categoryId: Int?) {
        
        homeManager.getTagsTypes(categoryId: categoryId, complation: { response in
            if let result = response, let tags = result.data, let status = result.status, status == 1 {
                self.tagsResponse.value = tags
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.generalErrorMessage(error: error)
        }
    }
}

