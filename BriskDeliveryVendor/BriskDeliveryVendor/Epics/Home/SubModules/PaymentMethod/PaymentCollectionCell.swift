//
//  PaymentCollectionCell.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/12/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

class PaymentCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    
    func configure(payment: Payment) {
        nameLabel.text = payment.paymentMethodName ?? ""
    }
    
}
