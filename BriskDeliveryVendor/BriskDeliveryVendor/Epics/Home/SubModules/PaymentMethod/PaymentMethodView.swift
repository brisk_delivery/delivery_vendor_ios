//
//  PaymentMethodView.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/12/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

protocol PaymentTypesDelegate: class {
    func didSelectPayments(payment: [Payment])
    func paymentViewheightDidChange(height: CGFloat)
}

class PaymentMethodView: UIViewController {
    static let identifier = "PaymentMethodView"
    var isUpdate = false
    var selectedIds = [Int]()
    var paymentMethodsList = [Payment]()
    var selectedPayments = [Payment]()
    var viewModel: PaymentMethodViewModel!
    var paymentLabelHeight: CGFloat = 25
    weak var delegate: PaymentTypesDelegate!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = PaymentMethodViewModel(router: RouterManager(self), homeManager: HomeManager())
        viewModel.getPaymentTypes()
        viewModel.paymentResponse.bind { (paymentMethods) in
            self.paymentMethodsList = paymentMethods
            self.updateSelectedWhenUpdate(paymentMethods: paymentMethods)
            self.collectionView.reloadData()
            let height =  (self.collectionView.collectionViewLayout.collectionViewContentSize.height) + self.paymentLabelHeight
            self.delegate.paymentViewheightDidChange(height: height)
            
        }
        collectionView.reloadData()
    }
    
    func updateSelectedWhenUpdate(paymentMethods: [Payment]) {
        guard self.isUpdate else {
            return
        }
        
        self.selectedIds.forEach{ id in
            paymentMethods.forEach{
                if $0.paymentMethodId == id {selectedPayments.append($0)}
            }
        }
        if selectedPayments.count > 0 {delegate.didSelectPayments(payment: selectedPayments)}
    }
    
    func isSelected(_ payment: Payment) -> Int? {
        if let index = (self.selectedPayments.index { (paymentMethod) -> Bool in
            paymentMethod == payment
            }) {
            return index
        } else {
            return nil
        }
    }
    
}

extension PaymentMethodView: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return paymentMethodsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let payment = paymentMethodsList[indexPath.row]
        let identifier = isSelected(payment) != nil ? "PaymentSelectedCell": "PaymentNotSelectedCell"
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as? PaymentCollectionCell {
            cell.configure(payment: payment)
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let payment = paymentMethodsList[indexPath.row]
        if let index = isSelected(payment) {
            selectedPayments.remove(at: index)
        } else {
            selectedPayments.append(payment)
        }
        delegate.didSelectPayments(payment: selectedPayments)
        collectionView.layer.borderColor = #colorLiteral(red: 0.4984278679, green: 0.3929923773, blue: 0.7918989062, alpha: 1)
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/2.15, height: 60)
    }
    
}
