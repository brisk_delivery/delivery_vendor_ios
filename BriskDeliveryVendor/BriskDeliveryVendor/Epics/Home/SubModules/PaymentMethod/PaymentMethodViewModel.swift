//
//  PaymentMethodViewModel.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/15/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class PaymentMethodViewModel: BaseViewModel {
    let homeManager: HomeManager
    var tagsResponse: Dynamic<[Tag]> = Dynamic([Tag]())
    var paymentResponse: Dynamic<[Payment]> = Dynamic([Payment]())
    
    init(router: Router, homeManager: HomeManager) {
        self.homeManager = homeManager
        super.init(router: router, isLoading: false)
    }
    
    func getPaymentTypes() {
        homeManager.getPaymentTypes(complation: { response in
            if let result = response, let paymentMethods = result.data, let status = result.status, status == 1 {
                self.paymentResponse.value = paymentMethods
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.generalErrorMessage(error: error)
        }
    }
}

