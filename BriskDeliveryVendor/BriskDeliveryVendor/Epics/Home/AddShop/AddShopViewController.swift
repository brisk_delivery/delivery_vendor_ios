//
//  AddShopViewController.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/8/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import Localize_Swift
import RSKPlaceholderTextView

class AddShopViewController: UIViewController {
    static var identifier = "AddShopViewController"
    var viewModel: AddShopViewModel!
    var cover: UIImage!
    var logo: UIImage!
    var categories = [Category]()
    var selectedStoreType: Category! {
        didSet {
            storeTypeContainerView.isHidden = true
            storeTypeLabel.text = selectedStoreType.catName
        }
    }
    var cities = [City]()
    
    var selectedCity: City! {
        didSet {
            cityContainerView.isHidden = true
            cityLabel.text = selectedCity.name ?? ""
        }
    }
    
    var currentDistricts = [District]()
    var selectedDistrict: District! {
        didSet {
            districtContainerView.isHidden = true
            districtLabel.text = selectedDistrict.name ?? ""
        }
    }
    
//    var paymentMethods = [Payment]()
    var tagsTypes = [Tag]()
    var cuisinesVC: CuisinesTypesView!
    
    var coverImageData: ImageData!
    var logoImageData: ImageData!
    var selectedLocation: Location! {
        didSet {
            addressLabel.text = selectedLocation.name ?? ""
        }
    }
//    var paymentVC: PaymentMethodView!
    var isUpdate = false
     var shopData = Shop()
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var districtContainerView: UIView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var storeTypeLabel: UILabel!
    @IBOutlet weak var cityContainerView: UIView!
    @IBOutlet weak var storeTypeContainerView: UIView!
    @IBOutlet weak var logoContainerView: UIView!
    @IBOutlet weak var coverContainerView: UIView!
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var districtLabel: UILabel!
    @IBOutlet var containerViews: [UIView]!
    @IBOutlet weak var tagsContainerView: UIView!
    @IBOutlet weak var tagViewHeight: NSLayoutConstraint!
    @IBOutlet var languageViews: [UIView]!
    @IBOutlet var languageLabels: [UILabel]!
    @IBOutlet var shopNameTextField: [UITextField]!
    @IBOutlet var shopDescriptionTextView: [RSKPlaceholderTextView]!
    @IBOutlet var viewsForValidations: [UIView]!
    @IBOutlet var minutesTextField: UITextField!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var shopNameArTextField: UITextField!
    @IBOutlet weak var shopNameEnTextField: UITextField!
    @IBOutlet weak var shopDescArTextView: RSKPlaceholderTextView!
    @IBOutlet weak var shopDescEnTextView: RSKPlaceholderTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = AddShopViewModel(router: RouterManager(self), homeManager: HomeManager())
        initObservables()
        viewModel.getCategories()
        viewModel.getCities()
        initUI()
        LocationManager.manager.initLocationManager()
        checkUpdate()
    }
    
    //MARK:- Observables
    func initObservables() {
        viewModel.isLoading.bindAndFire { shouldLoad in
            shouldLoad ? self.showIndicator() : self.hideIndicator()
        }
        viewModel.categoriesResponse.bindAndFire { [weak self](categories) in
            self?.categories = categories
            self?.setCategoryForUpdate(categories)
        }
        viewModel.citiesResponse.bindAndFire { [weak self](cities) in
            self?.cities = cities
            self?.setCityForUpdate(cities)
        }
        viewModel.shopCreated.bindAndFire {isCreated in
            guard isCreated else {
                return
            }
            if self.isUpdate {
                self.alert(title: "", message: "saveStoreMessage".localized(), ActionButtons: [(title: "ok".localized(),style:.Default, alignment: .center, bgColor: #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1) , textColor: .white, handler:{(action) in
                    self.dismiss(animated: true, completion: nil)
                }) ])
            } else {
                let vc = SuccessCreateShopViewController.create()
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        viewModel.enableEnteraction.bindAndFire{ isEnabled in
            self.view.isUserInteractionEnabled = isEnabled
        }
    }
    
    private func initUI() {
        coverContainerView.dropShadow(opacity: 0.16, offSet: CGSize(width: 1, height: 2), radius: 3)
        logoContainerView.dropShadow(opacity: 0.26, offSet: CGSize(width: 1, height: 2), radius: 3)
        containerViews.forEach {
            $0.dropShadow(opacity: 0.26, offSet: CGSize(width: 1, height: 2), radius: 3)
        }
        createButton.setTitle(isUpdate ? "saveButtonTitle".localized(): "createNewAccount".localized(), for: .normal)
        backImage.image = Localize.currentLanguage() == "ar"  ? #imageLiteral(resourceName: "ic_right_back"): #imageLiteral(resourceName: "ic_left_arrow")
        shopDescriptionTextView.forEach{
            $0.textAlignment = Localize.currentLanguage() == "ar" ? .right : .left
        }
    }
    
    @objc func hideContainerViews() {
        containerViews.forEach{$0.isHidden = true}
    }
    
    private func addSelectOneViewControllerToContainer(list: [GeneralItem], containerView: UIView, didSelectHandler: @escaping (GeneralItem) -> ()){
        let vc = SelectOneViewController.create(list, selectItemHandler: didSelectHandler)
        containerView.addSubview(vc.view)
        self.addChild(vc)
        vc.didMove(toParent: self)
        vc.view.frame = containerView.frame
        containerView.setXIBConstraints(xib: vc.view)
        containerView.isHidden = false
    }
    
    private func initStoreTypeViewController() {
       let list = categories.map{GeneralItem(id: $0.catId ?? 0, title: $0.catName ?? "")}
        addSelectOneViewControllerToContainer(list: list, containerView: storeTypeContainerView, didSelectHandler: storyTypeSelected(item:))
        containerViews.forEach{$0.isHidden = !($0 == storeTypeContainerView)}
    }
    
    private func initCityViewController() {
        let list = cities.map{GeneralItem(id: $0.id ?? 0, title: $0.name ?? "")}
        addSelectOneViewControllerToContainer(list: list, containerView: cityContainerView, didSelectHandler: citySelected(item:))
        containerViews.forEach{$0.isHidden = !($0 == cityContainerView)}
    }
    
    private func initDistrictViewController() {
        if let city = selectedCity, let districts = city.districts {
            let list = districts.map{GeneralItem(id: $0.id ?? 0, title: $0.name ?? "")}
            currentDistricts = districts
            addSelectOneViewControllerToContainer(list: list, containerView: districtContainerView, didSelectHandler: districtDidSelected(item:))
            containerViews.forEach{$0.isHidden = !($0 == districtContainerView)}
        } else {
            self.alert(title: "", message: "selectCityMessage".localized(), ActionButtons: [(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
        }
    }
    
    func storyTypeSelected(item: GeneralItem) {
        if let selected = categories.filter({$0.catId == item.id}).first {
            selectedStoreType = selected
            cuisinesVC.getCuisuines(categoryId: selected.catId)
        }
    }
    
    func citySelected(item: GeneralItem) {
        if let selected = cities.filter({$0.id == item.id}).first {
            selectedCity = selected
        }
    }
    
    func districtDidSelected(item: GeneralItem) {
        if let selected = currentDistricts.filter({$0.id == item.id}).first {
            selectedDistrict = selected
        }
    }
    
    func isValidInputs() -> Bool {
        if cover == nil || (!cover.isSizeValid(type: .cover) && !isUpdate) {
            coverContainerView.layer.borderColor = UIColor.red.cgColor
            coverImage.layer.borderColor = UIColor.red.cgColor
            showValidationMessage("coverImageMessage".localized())
            return false
        }
        
        if logo == nil || (!logo.isSizeValid(type: .profile) && !isUpdate) {
            logoContainerView.layer.borderColor = UIColor.red.cgColor
            showValidationMessage("logoImageMessage".localized())
            return false
        }
        
        if selectedStoreType == nil {
            viewsForValidations[0].layer.borderColor = UIColor.red.cgColor
            showValidationMessage("storeTypeMessage".localized())
            return false
        }
        
        if selectedCity == nil {
            viewsForValidations[1].layer.borderColor = UIColor.red.cgColor
            showValidationMessage("cityeMessage".localized())
            return false
        }
        
        if selectedDistrict == nil {
            viewsForValidations[2].layer.borderColor = UIColor.red.cgColor
            showValidationMessage("districteMessage".localized())
            return false
        }
        
        if selectedLocation == nil {
            viewsForValidations[3].layer.borderColor = UIColor.red.cgColor
            showValidationMessage("locationMessage".localized())
            return false
        }
        
        if let minutes = minutesTextField.text, minutes.isEmpty {
            viewsForValidations[4].layer.borderColor = UIColor.red.cgColor
            showValidationMessage("minutesMessage".localized())
            return false
        }
        
        if tagsTypes.count == 0 {
            self.cuisinesVC.collectionView.layer.borderColor = UIColor.red.cgColor
            showValidationMessage("tagsTypeMessage".localized())
            return false
        }
        
        if let shopName = shopNameEnTextField.text, shopName.isEmpty {
            changeLanguage(tag: 1)
            shopNameEnTextField.layer.borderColor = UIColor.red.cgColor
            showValidationMessage("shopNameMessage".localized())
            return false
        }
        
        if let shopDes = shopDescEnTextView.text, shopDes.isEmpty {
            changeLanguage(tag: 1)
            shopDescEnTextView.layer.borderColor = UIColor.red.cgColor
            showValidationMessage("shopDescMessage".localized())
            return false
        }
        return true
    }
    
    func showValidationMessage(_ message: String) {
        self.alert(title: "", message: message, ActionButtons: [(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red , textColor: .white, handler:{(action) in
//            self.dismiss(animated: true, completion: nil)
        }) ])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "TagsSegue", let cuisineVC = segue.destination as? CuisinesTypesView {
            cuisineVC.delegate = self
            cuisineVC.isUpdate = isUpdate
            if let shop = Global.shop, let category = shop.category, let catId = category.catId {
                cuisineVC.categoryId = catId
            }
            if isUpdate, let tags = Global.shop.tags { cuisineVC.selectedIds = tags }
            self.cuisinesVC = cuisineVC
        }
    }
    
    @IBAction func selectCoverImageButtonAction(_ sender: RoundButton) {
         coverContainerView.layer.borderColor = UIColor.clear.cgColor
        CameraHandler.shared.showActionSheet(vc: self)
        CameraHandler.shared.imagePickedBlock = { (image) in
            self.coverImage.image = image
            self.cover = image
            self.coverImageData = ImageData()
            self.coverImageData.mediaFile = image
            self.coverImageData.mediaType = 1
            self.coverImageData.fileName = UUID().uuidString
            self.coverImageData.mimeType = "image/jpg"
        }
    }
    
    @IBAction func selectLogoButtonAction(_ sender: RoundButton) {
         logoContainerView.layer.borderColor = UIColor.clear.cgColor
        CameraHandler.shared.showActionSheet(vc: self)
        CameraHandler.shared.imagePickedBlock = { (image) in
            self.logoImage.image = image
            self.logo = image
            self.logoImageData = ImageData()
            self.logoImageData.mediaFile = image
            self.logoImageData.mediaType = 1
            self.logoImageData.fileName = UUID().uuidString
            self.logoImageData.mimeType = "image/jpg"
        }
    }
    
    @IBAction func storeTypeButtonAction(_ sender: UIButton) {
        viewsForValidations[0].layer.borderColor = #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1).cgColor
        initStoreTypeViewController()
    }
    
    @IBAction func cityButtonAction(_ sender: UIButton) {
        viewsForValidations[1].layer.borderColor = #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1).cgColor
        initCityViewController()
    }
    
    @IBAction func districtButtonAction(_ sender: UIButton) {
        viewsForValidations[2].layer.borderColor = #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1).cgColor
        initDistrictViewController()
    }
    
    @IBAction func addressButtonAction(_ sender: UIButton) {
        hideContainerViews()
        viewsForValidations[3].layer.borderColor = #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1).cgColor
        navigateToMap()
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
    @IBAction func languageButtonAction(_ sender: UIButton) {
        changeLanguage(tag: sender.tag)
    }
    
    @IBAction func createShopButtonAction(_ sender: UIButton) {
        hideContainerViews()
        guard isValidInputs(), cover != nil, logo != nil else {
            return
        }
        
        if let coverImageResized = cover.resizeImage(newWidth: 250), let coverData = coverImageResized.jpegData(compressionQuality: 1),let logoImageResized = logo.resizeImage(newWidth: 250),
            let logoData = logoImageResized.jpegData(compressionQuality: 1) {
           
            if isUpdate { viewModel.updateShop(categoryId: selectedStoreType.catId ?? 0, cityId: selectedCity.id ?? 0, districtId: selectedDistrict.id ?? 0, shopLat: selectedLocation.latitude ?? 0, shopLng: selectedLocation.longitude ?? 0, timeDelivery: Int(minutesTextField.text!) ?? 0, tags: tagsTypes.map{return $0.tagId ?? 0}, coverImage: coverData, logoImage: logoData, mimeType: "image/jpg", translateArr: createShopTranslateArr())
                
            } else {
                viewModel.addNewShop(categoryId: selectedStoreType.catId ?? 0, cityId: selectedCity.id ?? 0, districtId: selectedDistrict.id ?? 0, shopLat: selectedLocation.latitude ?? 0, shopLng: selectedLocation.longitude ?? 0, timeDelivery: Int(minutesTextField.text!) ?? 0, tags: tagsTypes.map{return $0.tagId ?? 0}, coverImage: coverData, logoImage: logoData, mimeType: "image/jpg", translateArr: createShopTranslateArr())
            }
            
        }
    }
    
    func changeLanguage(tag: Int) {
        languageViews.forEach{
            $0.backgroundColor = $0.tag == tag ? #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1):#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        languageLabels.forEach{
            $0.textColor = $0.tag == tag ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
        shopNameTextField.forEach {
            $0.isHidden = !($0.tag == tag)
        }
        shopDescriptionTextView.forEach {
            $0.isHidden = !($0.tag == tag)
        }
    }
    
    func createShopTranslateArr() -> [ShopTranslate] {
        var translateArr = [ShopTranslate]()
        translateArr.append(ShopTranslate(langId: 1, shopName: shopNameEnTextField.text ?? "", shopDesc:  shopDescEnTextView.text ?? ""))
        translateArr.append(ShopTranslate(langId: 2, shopName: shopNameArTextField.text ?? "", shopDesc:  shopDescArTextView.text ?? ""))
        return translateArr
    }
    
    func navigateToMap() {
        if let viewController = UIStoryboard(name: "Map", bundle: nil).instantiateViewController(withIdentifier: "SelectLocationView") as? SelectLocationView {
            viewController.delegate = self
            viewController.modalPresentationStyle = .fullScreen
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
}

extension AddShopViewController: SelectLocationViewDelegate {
    func didSelectLocation(location: Location) {
        selectedLocation = location
    }
}

extension AddShopViewController: CuisinesTypesDelegate {
    func didSelectCuisines(Cuisines: [Tag]) {
        hideContainerViews()
        tagsTypes = Cuisines
    }
    
    func cuisinesViewheightDidChange(height: CGFloat) {
        tagViewHeight.constant = height
        view.layoutIfNeeded()
    }
}

extension AddShopViewController: UITextFieldDelegate, UITextViewDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        hideContainerViews()
        viewsForValidations.last?.layer.borderColor = #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1)
        textField.layer.borderColor = #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        hideContainerViews()
        textView.layer.borderColor = #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1)
    }
}
