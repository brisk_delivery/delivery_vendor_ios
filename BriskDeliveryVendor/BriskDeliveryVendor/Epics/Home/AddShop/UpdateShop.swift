//
//  UpdateShop.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/23/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import CoreLocation
import Localize_Swift

extension AddShopViewController {
    
    func checkUpdate() {
        if isUpdate {
            setShopData()
        }
    }
    
    func setShopData() {
        guard let shop = Global.shop else {
            return
        }
        coverImage.kf.setImage(with: URL(string: shop.caverPath ?? ""), placeholder: #imageLiteral(resourceName: "brisk_default"), options: [], progressBlock: nil) { (image, _, _, _) in
            if let img = image {self.cover = img}
        }
        logoImage.kf.setImage(with: URL(string: shop.profilePath ?? ""), placeholder: #imageLiteral(resourceName: "brisk_default"), options: [], progressBlock: nil) { (image, _, _, _) in
            if let img = image {self.logo = img}
        }
        minutesTextField.text = shop.timeDelivery ?? ""
        if let translates = shop.translates, translates.count >= 2 {
            shopNameTextField[0].text = translates[0].shopName
            shopNameTextField[1].text = translates[1].shopName
            shopDescriptionTextView[0].text = translates[0].shopDesc
            shopDescriptionTextView[1].text = translates[1].shopDesc
        }
        lookUpCurrentLocation(location: CLLocation(latitude: Double(shop.shopLat ?? "") ?? 0, longitude: Double(shop.shopLng ?? "") ?? 0.0)) { (place) in
            let name = self.getPlaceNameConcatinated(place)
            self.selectedLocation = Location(name: name, longitude: Double(shop.shopLng ?? "") ?? 0, latitude: Double(shop.shopLat ?? "") ?? 0)
            self.addressLabel.text = name
        }
    }
    
    func getPlaceNameConcatinated(_ placeMark: CLPlacemark?) -> String {
        // Location name
        var address = ""
        if let locationName = placeMark?.addressDictionary?["Name"] as? String {
            address += locationName + ", "
        }
        
        // Street address
        if let street = placeMark?.addressDictionary?["Thoroughfare"] as? String {
            address += street + ", "
        }
        
        // City
        if let city = placeMark?.addressDictionary?["City"] as? String {
            address += city + ", "
        }
        
        // Zip code
        if let zip = placeMark?.addressDictionary?["ZIP"] as? String {
            address += zip + ", "
        }
        
        // Country
        if let country = placeMark?.addressDictionary?["Country"] as? String {
            address += country
        }
        return address
    }
    
    func setCategoryForUpdate(_ categories: [Category]) {
        if self.isUpdate, let category = Global.shop.category, let selected = categories.filter({$0.catId ?? 0 == category.catId ?? 0}).first {
            self.selectedStoreType = selected
        }
    }
    
    func setCityForUpdate(_ cities: [City]) {
        if self.isUpdate, let selected = cities.filter({$0.id ?? 0 == Global.shop.cityId ?? 0}).first {
            self.selectedCity = selected
            setDistrictForUpdate(selected)
        }
    }
    
    func setDistrictForUpdate(_ selectedCity: City) {
        if self.isUpdate, let selected = selectedCity.districts?.filter({$0.id ?? 0 == Global.shop.districtId ?? 0}).first {
            self.selectedDistrict = selected
        }
    }
    
    func lookUpCurrentLocation(location: CLLocation, completionHandler: @escaping (CLPlacemark?)
        -> Void ) {
        // Use the last reported location.
//        if let lastLocation = self.locationManager.location {
            let geocoder = CLGeocoder()
            
            // Look up the location and pass it to the completion handler
            geocoder.reverseGeocodeLocation(location,
                                            completionHandler: { (placemarks, error) in
                                                if error == nil {
                                                    let firstLocation = placemarks?[0]
                                                    completionHandler(firstLocation)
                                                }
                                                else {
                                                    // An error occurred during geocoding.
                                                    completionHandler(nil)
                                                }
            })
    }
}
