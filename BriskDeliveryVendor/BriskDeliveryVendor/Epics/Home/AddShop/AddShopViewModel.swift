//
//  AddShopViewModel.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/9/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class AddShopViewModel: BaseViewModel {
    let homeManager: HomeManager
    var enableEnteraction: Dynamic<Bool> = Dynamic(true)
    var shouldShowStoreType: Dynamic<Bool> = Dynamic(false)
    var categoriesResponse: Dynamic<[Category]> = Dynamic([Category]())
    var citiesResponse: Dynamic<[City]> = Dynamic([City]())
    var shopCreated: Dynamic<Bool> = Dynamic(false)
    var shopResponse: Dynamic<Shop> = Dynamic(Shop())
    
    init(router: Router, homeManager: HomeManager) {
        self.homeManager = homeManager
        super.init(router: router, isLoading: false)
    }
    
    
    func getCategories() {
        homeManager.getCategories(complation: { response in
            if let result = response, let categories = result.data, let status = result.status, status == 1{
                self.categoriesResponse.value = categories
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.generalErrorMessage(error: error)
        }
    }
    
    func getCities() {
        homeManager.getCityDistrict(complation: { response in
            if let result = response, let cities = result.data, let status = result.status, status == 1 {
                self.citiesResponse.value = cities
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.generalErrorMessage(error: error)
        }
    }
    
    func addNewShop(categoryId : Int, cityId: Int ,districtId : Int, shopLat: Double , shopLng: Double, timeDelivery: Int, tags: [Int], coverImage: Data, logoImage: Data, mimeType: String, translateArr: [ShopTranslate]) {
        enableEnteraction.value = false
        startLoading()
        homeManager.addShop(categoryId: categoryId, cityId: cityId, districtId: districtId, shopLat: shopLat, shopLng: shopLng, timeDelivery: timeDelivery, tags: tags, coverImage: coverImage, mediaMimeType: mimeType, logoImage: logoImage, complation: { [weak self] response in
            self?.enableEnteraction.value = true
            self?.stopLoading()
            if let responseData = response, responseData.status == 1, let shopData = responseData.data, let shopId = shopData.shopId {
                Global.shopId = shopId
                let updatedTranslateArr: [ShopTranslate] = translateArr.map{ shop in
                    shop.shopId = shopId
                    return shop
                }
                NotificationCenter.default.post(name: .shopAdded, object: nil)
                self?.translateShopData(shopId: shopId, translateArr: updatedTranslateArr)
            }else {
                self?.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
    
    func translateShopData(shopId: Int, translateArr: [ShopTranslate]) {
        enableEnteraction.value = false
        startLoading()
        homeManager.translateShopData(shopId: shopId, translateArr: translateArr, complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
            if let responseData = response, responseData.status == 1, responseData.data != nil {
                self.getShopData(by: shopId)
                self.shopCreated.value = true
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
    
    func getShopData(by shopId: Int) {
        DispatchQueue.global(qos: .background).async {
            self.homeManager.getShop(shopId: Global.shopId, complation: { response in
                if let result = response, let shop = result.data {
                    Global.shop = shop
                }
            }) { error in}
        }
    }
    
    func updateShop(categoryId : Int, cityId: Int ,districtId : Int, shopLat: Double , shopLng: Double, timeDelivery: Int, tags: [Int], coverImage: Data, logoImage: Data, mimeType: String, translateArr: [ShopTranslate]) {
        enableEnteraction.value = false
        startLoading()
        homeManager.updateShop(shopId: Global.shopId, categoryId: categoryId, cityId: cityId, districtId: districtId, shopLat: shopLat, shopLng: shopLng, timeDelivery: timeDelivery, tags: tags, coverImage: coverImage, mediaMimeType: mimeType, logoImage: logoImage, complation: { [weak self] response in
            self?.enableEnteraction.value = true
            self?.stopLoading()
            if let responseData = response, responseData.status == 1, let shopData = responseData.data, let shopId = shopData.shopId {
                Global.shopId = shopId
                let updatedTranslateArr: [ShopTranslate] = translateArr.map{ shop in
                    shop.shopId = shopId
                    return shop
                }
                self?.translateShopData(shopId: shopId, translateArr: updatedTranslateArr)
            }else {
                self?.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }

}


