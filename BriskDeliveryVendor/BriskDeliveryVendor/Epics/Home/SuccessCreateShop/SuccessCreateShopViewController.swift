//
//  SuccessCreateShopViewController.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/16/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

class SuccessCreateShopViewController: UIViewController {
    static let identifier = "SuccessCreateShopViewController"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    static func create() -> SuccessCreateShopViewController {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: identifier) as! SuccessCreateShopViewController
        return viewController
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
