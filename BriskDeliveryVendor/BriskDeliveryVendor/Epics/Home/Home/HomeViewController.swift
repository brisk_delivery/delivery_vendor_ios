//
//  HomeViewController.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/6/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    var viewModel: HomeViewModel!
    weak var listVC: OrdersListViewController!

    static let identifier = "HomeViewController"
    
    @IBOutlet weak var ordersContainerView: UIView!
    
    @IBOutlet var labels: [LocalizeLabel]!
    
    @IBOutlet weak var deliverySatausSwitch: UISwitch!
    @IBOutlet weak var shopStatusSwitchControl: UISwitch!
    @IBOutlet weak var creatShopView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = HomeViewModel(router: RouterManager(self),homeManager: HomeManager())
        viewModel.getShopStatus()
        viewModel.isShopOpened.bindAndFire{ isOpen in            self.shopStatusSwitchControl.isHidden = false
            self.shopStatusSwitchControl.isOn = isOpen
        }
        viewModel.isDeliveryOpened.bindAndFire{ isOpen in            self.deliverySatausSwitch.isHidden = false
                 self.deliverySatausSwitch.isOn = isOpen
             }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initUI()
    }
    
    private func initUI() {
        creatShopView.isHidden = Global.shopId != nil ? true : false
        ordersContainerView.isHidden = Global.shopId != nil ? false : true
        labels.forEach{$0.text = $0.localizeKey?.localized()}
    }
    
    func getOrders() {
        listVC.updateList(status: "all")
    }
    
    func openOrderDetails(orderId: Int)  {
        listVC.openOrderDetails(orderId: orderId)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "OrdersListSegue", let vc = segue.destination as? OrdersListViewController {
            self.listVC = vc
        }
    }
    
    @IBAction func addShopAction(_ sender: UIButton) {
        NotificationCenter.default.post(name: .addShopPopup, object: nil)
    }
    
    @IBAction func changeShopStatusAction(_ sender: UISwitch) {
        viewModel.updateShopStatus()
    }
    @IBAction func changeDeliveryAction(_ sender: Any) {
        viewModel.updateDeliveryStatus()
    }
}


