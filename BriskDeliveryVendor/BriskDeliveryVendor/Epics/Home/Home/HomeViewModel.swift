//
//  HomeViewModel.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/9/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class HomeViewModel: BaseViewModel {
    let homeManager: HomeManager
    var isShopOpened: Dynamic<Bool> = Dynamic(false)
    var isDeliveryOpened: Dynamic<Bool> = Dynamic(false)

    init(router: Router, homeManager: HomeManager) {
        self.homeManager = homeManager
        super.init(router: router, isLoading: false)
    }
    
    func getShopStatus() {
        if let shopId = Global.shopId {
        homeManager.getShopStatus(shopId: shopId, complation: { response in
            if let result = response, let data = result.data, let busy = data.busy {
                Global.shopStatus = busy
                Global.DeliveryStatus = data.delivery!
                self.isShopOpened.value = busy == 0 ? false : true
                self.isDeliveryOpened.value = data.delivery == 0 ? false : true

            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.generalErrorMessage(error: error)
            }
        }
    }
    
    
    func updateShopStatus() {
        if let shopId = Global.shopId {
            homeManager.updateShopStatus(shopId: shopId, complation: { response in
            if let result = response, let status = result.status, status == 1 {
//                UserDefaultsHelpers.set(value: Global.shopStatus == 0 ? 1 : 0, key: UserDefaultsKeys.shopStatus.rawValue)
                
            Global.shopStatus = Global.shopStatus == 0 ? 1 : 0
            self.isShopOpened.value = Global.shopStatus == 0 ? false : true
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.generalErrorMessage(error: error)
            }
        }
    }
     func updateDeliveryStatus() {
            if let shopId = Global.shopId {
                homeManager.updateDeliveryStatus(shopId: shopId, complation: { response in
                if let result = response, let status = result.status, status == 1 {
    //                UserDefaultsHelpers.set(value: Global.shopStatus == 0 ? 1 : 0, key: UserDefaultsKeys.shopStatus.rawValue)
                    
                Global.DeliveryStatus = Global.DeliveryStatus == 0 ? 1 : 0
                self.isDeliveryOpened.value = Global.DeliveryStatus == 0 ? false : true
                }else {
                    self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
                }
            }) { error in
                self.generalErrorMessage(error: error)
                }
            }
        }
}

