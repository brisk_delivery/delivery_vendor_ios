//
//  MainViewController.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/13/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import Localize_Swift
import EasyTipView

class MainViewController: UIViewController {
    static let identifier = "MainViewController"
    var currentPage = 0
    var viewModel: MainViewModel!
    var homeVC: HomeViewController!
    var homeTabsView: HomeTabsWithAddView!
    var orderId: Int!
    var easyTipView: EasyTipView?
    
    @IBOutlet weak var tabsContainerView: UIView!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var homeContainerView: UIView!
    @IBOutlet var tabsView: [UIView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTabs()
        viewModel = MainViewModel(router: RouterManager(self), homeManager: HomeManager())
        NotificationCenter.default.addObserver(self, selector: #selector(updateHome), name: .shopAdded, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(shopPopup), name: .addShopPopup, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(setSemantic), name: .languageChanged, object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSemantic()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        openOrderDetailsFromNotification()
    }
    
    func openOrderDetailsFromNotification() {
        if let id = orderId {
            homeVC.openOrderDetails(orderId: id)
            orderId = nil
        }
    }
    
    @objc func shopPopup(){
        easyTipView?.dismiss()
        easyTipView = nil
        var preferences = EasyTipView.Preferences()
        preferences.drawing.font = UIFont(name: "Futura-Medium", size: 15)!
        preferences.drawing.foregroundColor = UIColor.white
        preferences.drawing.backgroundColor = #colorLiteral(red: 0.4984278679, green: 0.3929923773, blue: 0.7918989062, alpha: 1)
        preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.bottom
        easyTipView = EasyTipView(text: "addShopPopup".localized(), preferences: preferences, delegate: nil)
        easyTipView?.show(forView: self.popupView)
    }
    
    @objc func updateHome() {
        homeVC.getOrders()
        addTabs()
    }
    
    @objc func setSemantic() {
        UIView.appearance().semanticContentAttribute = Localize.currentLanguage() == "ar"  ? .forceRightToLeft : .forceLeftToRight
        homeTabsView?.updateLabelsLocalization()
    }
    
    func addTabs() {
        removeHomeTabs()
        let type: HomeTabsType = Global.shopId != nil ? .HomeTabs : .HomeTabsWithAddView
        homeTabsView = HomeTabsWithAddView(type: type, delegate: self)
        tabsContainerView.addSubview(homeTabsView)
        tabsContainerView.setXIBConstraints(xib: homeTabsView)
    }
    
    func removeHomeTabs() {
        if homeTabsView != nil {
            homeTabsView.removeFromSuperview()
            homeTabsView = nil
        }
    }
    
    @IBAction func swipeAction(_ sender: UISwipeGestureRecognizer) {
        currentShownViewSetting(sender.direction)
    }
    
    func currentShownViewSetting(_ direction: UISwipeGestureRecognizer.Direction) {
        if Localize.currentLanguage() == "ar"{
            if direction == .left, currentPage != 0 {
                currentPage -= 1
            } else if direction == .right, currentPage != 3 {
                currentPage += 1
            }
        }else{
            if direction == .left, currentPage != 3 {
                currentPage += 1
            } else if direction == .right, currentPage != 0 {
                currentPage -= 1
            }
        }
        changeTabsUI()
    }
    
    func changeTabsUI() {
        tabsView.forEach{ $0.isHidden = !($0.tag == currentPage)}
        homeTabsView.chageTap(page: currentPage)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "HomeSegue", let vc = segue.destination as? HomeViewController {
            homeVC = vc
        }
    }
    
}

extension MainViewController: HomeTabsDelegate {
    func addShop() {
        easyTipView?.dismiss()
        if checkPermission(), let vc = storyboard?.instantiateViewController(withIdentifier: "AddShopViewControllerNavigation") {
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func openOrders() {
        easyTipView?.dismiss()
        currentPage = 1
        changeTabsUI()
    }
    
    func openProducts() {
        easyTipView?.dismiss()
        currentPage = 2
        changeTabsUI()
    }
    
    func openSettings() {
        easyTipView?.dismiss()
        currentPage = 3
        changeTabsUI()
    }
    
    func openHome() {
        easyTipView?.dismiss()
        currentPage = 0
        changeTabsUI()
    }
    
}
