//
//  MainViewModel.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/14/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class MainViewModel: BaseViewModel {
    let homeManager: HomeManager
    
    init(router: Router, homeManager: HomeManager) {
        self.homeManager = homeManager
        super.init(router: router, isLoading: false)
    }
    
}
