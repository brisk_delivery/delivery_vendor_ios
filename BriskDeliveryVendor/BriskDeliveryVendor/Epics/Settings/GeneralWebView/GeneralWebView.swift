//
//  GeneralWebView.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 5/9/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import WebKit

enum WebViewType {
    case aboutUs
    case termsConditions
}

class GeneralWebView: UIViewController, WKNavigationDelegate {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    var webView: WKWebView!
    static let identifier = "GeneralWebView"
    var type: WebViewType = .aboutUs
    var viewModel: GeneralWebViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView = WKWebView()
        webView.navigationDelegate = self
        viewModel = GeneralWebViewModel(router: RouterManager(self), settingManager: SettingManager())
        type == .aboutUs ? viewModel.getAboutUs() : viewModel.getTermsCondition()
        initUI()
        initObservables()
    }
    
    func initObservables() {
        viewModel.isLoading.bindAndFire { shouldLoad in
            shouldLoad ? self.showIndicator() : self.hideIndicator()
        }
        viewModel.enableEnteraction.bindAndFire{ isEnabled in
            self.view.isUserInteractionEnabled = isEnabled
        }
        viewModel.response.bindAndFire { data in
            self.titleLabel.text = data.pageTitle ?? ""
            self.webView.loadHTMLString(data.pageBody ?? "", baseURL: nil)
        }
    }
    
    static func create(type: WebViewType) -> GeneralWebView {
        let storyBoard = UIStoryboard(name: "Settings", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: identifier) as! GeneralWebView
        vc.type = type
        return vc
    }

    func initUI() {
        webView.scrollView.bounces = false
        containerView.addSubview(webView)
        containerView.bringSubviewToFront(webView)
        containerView.setXIBConstraints(xib: webView)
    }

    @IBAction func backAction(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }

}

