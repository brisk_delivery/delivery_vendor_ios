//
//  GeneralWebViewModel.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 5/9/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class GeneralWebViewModel: BaseViewModel {
    let settingManager: SettingManager
    var successUpdate: Dynamic<Bool> = Dynamic(false)
    var enableEnteraction: Dynamic<Bool> = Dynamic(true)
    var response: Dynamic<AboutUsData> = Dynamic(AboutUsData())
    
    init(router: Router, settingManager: SettingManager) {
        self.settingManager = settingManager
        super.init(router: router, isLoading: false)
    }
    
    func getAboutUs(){
        enableEnteraction.value = false
        startLoading()
        settingManager.getAboutUs(complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
            if let result = response, let data = result.data {
                self.response.value = data
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "someThingWentWrong".localized(), ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
    
    func getTermsCondition() {
        enableEnteraction.value = false
        startLoading()
        settingManager.getTerms(complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
            if let result = response, let data = result.data {
                self.response.value = data
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "someThingWentWrong".localized(), ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
}
