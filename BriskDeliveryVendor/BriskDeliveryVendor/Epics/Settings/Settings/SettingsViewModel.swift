//
//  SettingsViewModel.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/22/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation
import Localize_Swift

class SettingsViewModel: BaseViewModel {
    let settingManager: SettingManager
    let homeManager: HomeManager
    var successLogout: Dynamic<Bool> = Dynamic(false)
    var enableEnteraction: Dynamic<Bool> = Dynamic(true)
    var isToOpenShop: Dynamic<Bool> = Dynamic(false)
    
    init(router: Router, settingManager: SettingManager, homeManager: HomeManager) {
        self.settingManager = settingManager
        self.homeManager = homeManager
        super.init(router: router, isLoading: false)
    }
    
    func performLogout() {
        enableEnteraction.value = false
        startLoading()
        settingManager.logout(complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
            if let result = response, let status = result.status, status != 1 {
                self.showAlertMessage(title: "", message: response?.message ?? "someThingWentWrong".localized(), ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
            Global.remove()
            self.successLogout.value = true
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
            Global.remove()
            self.successLogout.value = true
        }
    }
    
    func getShopData() {
        guard let shopId = checkForShopId() else  {
            return
        }
        enableEnteraction.value = false
        startLoading()
        homeManager.getShop(shopId: shopId, complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
            if let result = response, let shop = result.data {
                Global.shop = shop
                self.isToOpenShop.value = false
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
}


