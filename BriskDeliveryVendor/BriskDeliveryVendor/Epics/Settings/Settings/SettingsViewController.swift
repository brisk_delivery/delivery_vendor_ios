//
//  SettingsViewController.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/5/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import Localize_Swift

class SettingsViewController: UIViewController {
    var viewModel: SettingsViewModel!
    
    @IBOutlet var arrowImages: [UIImageView]!
    @IBOutlet var labels: [LocalizeLabel]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = SettingsViewModel(router: RouterManager(self), settingManager: SettingManager(), homeManager: HomeManager())
        initObservables()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setStyle()
    }
    
    func setStyle()  {
        UIView.appearance().semanticContentAttribute = Localize.currentLanguage() == "ar"  ? .forceRightToLeft : .forceLeftToRight
        arrowImages.forEach{ $0.image = Localize.currentLanguage() == "ar" ? #imageLiteral(resourceName: "ic_arrow_left") : #imageLiteral(resourceName: "ic_arrow_right") }
        labels.forEach {$0.text = $0.localizeKey?.localized()}
    }
    
    private func initObservables() {
        viewModel.isLoading.bindAndFire { shouldLoad in
            shouldLoad ? self.showIndicator() : self.hideIndicator()
        }
        viewModel.enableEnteraction.bindAndFire{ isEnabled in
            self.view.isUserInteractionEnabled = isEnabled
        }
        viewModel.isToOpenShop.bindAndFire { isToOpenShop in
            if isToOpenShop {self.openShop()}
        }
        viewModel.successLogout.bindAndFire{ isLogout in
            if isLogout {MainRouterManager.launchApplication(openLanguage: false)}
        }
    }
    
    func openShop() {
        if let navigationVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddShopViewControllerNavigation") as? UINavigationController, let vc = navigationVC.viewControllers[0] as? AddShopViewController {
            vc.isUpdate = true
            navigationVC.modalPresentationStyle = .fullScreen
            self.present(navigationVC, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func storeButtonAction(_ sender: UIButton) {
        guard checkPermission() else {
            return
        }
        if Global.shop != nil {
            openShop()
        } else {
            viewModel.getShopData()
        }
    }
    
    @IBAction func languageButtonAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Signup", bundle: nil)
        if let vc = storyBoard.instantiateViewController(withIdentifier: LanguageViewController.identifier) as? LanguageViewController {
            vc.commingFrom = .settings
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func logoutButtonAction(_ sender: UIButton) {
        viewModel.performLogout()
    }
    
    
    @IBAction func aboutUsButtonAction(_ sender: UIButton) {
        let vc = GeneralWebView.create(type: .aboutUs)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func termsButtonAction(_ sender: UIButton) {
        let vc = GeneralWebView.create(type: .termsConditions)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func shareButtonAction(_ sender: UIButton) {
        generalSharing(shareText: "Brisk Eats")
    }
    
   @IBAction func personalDataButtonAction(_ sender: UIButton) {
    if checkPermission() {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "PersonalDataViewController") {
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
   }
    
    @IBAction func workingHoursButtonAction(_ sender: UIButton) {
     if checkPermission() {
         if let vc = storyboard?.instantiateViewController(withIdentifier: "WorkingHoursView") {
             vc.modalPresentationStyle = .fullScreen
             self.present(vc, animated: true, completion: nil)
         }
     }
    }
    
}
