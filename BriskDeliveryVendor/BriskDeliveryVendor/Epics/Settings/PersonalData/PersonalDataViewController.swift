//
//  PersonalDataViewController.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/22/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import Localize_Swift

class PersonalDataViewController: UIViewController {
    var viewModel: PersonalDataViewModel!
    var personalImage: UIImage!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var personalImageView: UIImageView!
    @IBOutlet weak var personalImageContainerView: UIView!
    @IBOutlet weak var backImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = PersonalDataViewModel(router: RouterManager(self), settingManager: SettingManager())
        initObservables()
        setData()
        setStyle()
    }
    
    private func setStyle()  {
        backImage.image = Localize.currentLanguage() == "ar"  ? #imageLiteral(resourceName: "ic_right_back"): #imageLiteral(resourceName: "ic_left_arrow")
    }
    
    func setData()  {
        if let user = Global.user {
            firstNameTextField.text = user.firstName ?? ""
            lastNameTextField.text = user.lastName ?? ""
            phoneNumberTextField.text = user.mobileNumber ?? ""
            emailTextField.text = user.email ?? ""
            personalImageView.kf.setImage(with: URL(string: user.profilePath ?? ""), placeholder: #imageLiteral(resourceName: "brisk_default"), options: [], progressBlock: nil) { (image, _, _, _) in
                if let img = image {self.personalImage = img}
            }
        }
    }
    
    func isValidInputs() -> Bool {
        var result = true
        
        if let firstName = firstNameTextField.text, firstName.isEmpty {
            firstNameTextField.layer.borderColor = UIColor.red.cgColor
            result = false
        }
        
        if let lastName = lastNameTextField.text, lastName.isEmpty {
            lastNameTextField.layer.borderColor = UIColor.red.cgColor
            result = false
        }
        
        if let phoneNumber = phoneNumberTextField.text, phoneNumber.isEmpty {
            phoneNumberTextField.layer.borderColor = UIColor.red.cgColor
            result = false
        }
        
        if let email = emailTextField.text, email.isEmpty {
            emailTextField.layer.borderColor = UIColor.red.cgColor
            result = false
        }
        
        if let password = passwordTextField.text, password.isEmpty {
            passwordTextField.layer.borderColor = UIColor.red.cgColor
            result = false
        }
        
        if let password = oldPasswordTextField.text, password.isEmpty {
            oldPasswordTextField.layer.borderColor = UIColor.red.cgColor
            result = false
        }
        
        if personalImage == nil {
            personalImageContainerView.layer.borderColor = UIColor.red.cgColor
        }
        
        return result
    }
    
    private func initObservables() {
        viewModel.isLoading.bindAndFire { shouldLoad in
            shouldLoad ? self.showIndicator() : self.hideIndicator()
        }
        viewModel.enableEnteraction.bindAndFire{ isEnabled in
            self.view.isUserInteractionEnabled = isEnabled
        }
        viewModel.successUpdate.bindAndFire { isSuccess in
            if isSuccess {
                self.alert(title: "", message: "updateProfileMessage".localized(), ActionButtons: [(title: "ok".localized(),style:.Default, alignment: .center, bgColor: #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1) , textColor: .white, handler:{(action) in
                    self.dismiss(animated: true, completion: nil)
                }) ])
            }
        }
    }
   
    @IBAction func personalImageButtonAction(_ sender: UIButton) {
        personalImageContainerView.layer.borderColor = #colorLiteral(red: 0.9786400199, green: 0.7947712541, blue: 0.3013051748, alpha: 1)
        CameraHandler.shared.showActionSheet(vc: self)
        CameraHandler.shared.imagePickedBlock = { (image) in
            self.personalImageView.image = image
            self.personalImage = image
        }
    }
    
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func saveButtonAction(_ sender: UIButton) {
        
        guard isValidInputs(), personalImage != nil else {
            return
        }
        if let profileImageResized = personalImage.resizeImage(newWidth: 250), let personalImageData = profileImageResized.jpegData(compressionQuality: 1){
            viewModel.updateProfile(firstName: firstNameTextField.text!, lastName: lastNameTextField.text!, mobileNumber: phoneNumberTextField.text!, email: emailTextField.text!, password: passwordTextField.text!, oldPassword: oldPasswordTextField.text!, profileImage: personalImageData)
        }
    }
}

