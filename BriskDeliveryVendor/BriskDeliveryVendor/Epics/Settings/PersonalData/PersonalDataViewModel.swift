//
//  PersonalDataViewModel.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/22/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class PersonalDataViewModel: BaseViewModel {
    let settingManager: SettingManager
    var successUpdate: Dynamic<Bool> = Dynamic(false)
    var enableEnteraction: Dynamic<Bool> = Dynamic(true)
    
    init(router: Router, settingManager: SettingManager) {
        self.settingManager = settingManager
        super.init(router: router, isLoading: false)
    }
    
    func updateProfile(firstName: String, lastName: String, mobileNumber: String, email: String, password: String, oldPassword: String, profileImage: Data){
        enableEnteraction.value = false
        startLoading()
        settingManager.updateProfile(firstName: firstName, lastName: lastName, mobileNumber: mobileNumber, email: email, password: password, oldPassword: oldPassword, profileImage: profileImage, mediaMimeType: "image/jpg",complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
            if let result = response, let status = result.status, status == 1, let user = result.data{
                Global.user = user
                self.successUpdate.value = true
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "someThingWentWrong".localized(), ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
}
