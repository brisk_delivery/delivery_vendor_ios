//
//  DayCollectionViewCell.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 7/17/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

class DayCollectionViewCell: UICollectionViewCell {
//    static let identifier = "DayCollectionViewCell"
    
    @IBOutlet weak var dayLabel: UILabel!
//    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        containerView.layer.cornerRadius = containerView.frame.height / 2
    }
    
    func configure(day: ShopDay) {
        dayLabel.text = day.dayName ?? ""
    }
    
}


