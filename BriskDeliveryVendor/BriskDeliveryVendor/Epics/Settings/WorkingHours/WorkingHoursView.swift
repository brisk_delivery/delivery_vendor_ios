//
//  WorkingHoursView.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 7/4/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import DatePickerDialog
import Localize_Swift

class WorkingHoursView: UIViewController {
    var viewModel: WorkingHoursViewModel!
    var daysArr = [ShopDay]()
    var workingHoursArr = [WorkingHour]()
    var selectedDay: ShopDay! {
        didSet{
            daysCollectionView.reloadData()
        }
    }
    var fromTime: String?
    var toTime: String?
    var formatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        return formatter
    }

    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var hoursTableView: UITableView!
    @IBOutlet weak var daysCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = WorkingHoursViewModel(router: RouterManager(self), settingManager: SettingManager())
        viewModel.getWorkingHours()
        viewModel.getDays()
        initObservables()
        setStyle()
    }
    
    private func setStyle()  {
        backImage.image = Localize.currentLanguage() == "ar"  ? #imageLiteral(resourceName: "ic_right_back"): #imageLiteral(resourceName: "ic_left_arrow")
    }
    
    private func initObservables() {
        viewModel.isLoading.bindAndFire { shouldLoad in
            shouldLoad ? self.showIndicator() : self.hideIndicator()
        }
        viewModel.enableEnteraction.bindAndFire{ isEnabled in
            self.view.isUserInteractionEnabled = isEnabled
        }
        viewModel.daysResponse.bind { days in
            self.daysArr = days
            self.selectedDay = self.daysArr[0]
            self.daysCollectionView.reloadData()
        }
        viewModel.workingHoursResponse.bind { hours in
            self.workingHoursArr = hours
            self.hoursTableView.reloadData()
            self.fromLabel.text = self.getFromTime()
            self.toLabel.text = self.getToTime()
        }
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
@IBAction func fromButtonAction(_ sender: UIButton) {
        let start = Calendar.current.date(byAdding: .day, value: 1, to: Date())
        let end = Calendar.current.date(byAdding: .year, value: 1, to: Date())
    DatePickerDialog().show("DatePicker", doneButtonTitle: "ok".localized(), cancelButtonTitle: "cancel".localized(), minimumDate: start, maximumDate: end, datePickerMode: .time) { (date) -> Void in
            if let date = date {
                let stringDate = self.formatter.string(from: date)
                self.fromLabel.text = stringDate
                self.fromTime = stringDate
                self.fromLabel.textColor = #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1)
            }
        }
}
    
    @IBAction func toButtonAction(_ sender: UIButton) {
            let start = Calendar.current.date(byAdding: .day, value: 1, to: Date())
            let end = Calendar.current.date(byAdding: .year, value: 1, to: Date())
        DatePickerDialog().show("DatePicker", doneButtonTitle: "ok".localized(), cancelButtonTitle: "cancel".localized(), minimumDate: start, maximumDate: end, datePickerMode: .time) { (date) -> Void in
                if let date = date {
                    let stringDate = self.formatter.string(from: date)
                    self.toLabel.text = stringDate
                    self.toTime = stringDate
                    self.toLabel.textColor = #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1)
                }
            }
    }
    
    @IBAction func saveTimeButtonAction(_ sender: UIButton) {
        let time = TimeRequest(shopId: Global.shopId ?? 0, dayId: selectedDay.dayId ?? 0, timeFrom: fromTime ?? "08:00", timeTo: toTime ?? "23:55")
        viewModel.saveTime(time: time)
        fromTime = nil
        toTime = nil
    }
}

extension WorkingHoursView: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return daysArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let day = daysArr[indexPath.row]
        let identifier = isSelected(day: day) ? "DaySelectedCell": "DayCell"
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as? DayCollectionViewCell {
            cell.configure(day: day)
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            selectedDay = daysArr[indexPath.row]
            self.fromLabel.text = self.getFromTime()
            self.toLabel.text = self.getToTime()
            self.fromLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            self.toLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 50)
    }
    
    func isSelected(day: ShopDay) -> Bool {
        guard let selected = selectedDay, let dayId = selected.dayId, dayId == day.dayId  else { return false }
        return true
    }
    
    func getFromTime() -> String {
        guard let selected = selectedDay, let selectedDayName = selected.dayName, let day = workingHoursArr.filter({$0.dayName == selectedDayName}).first, let timeFrom = day.timeFrom
        else {return "08:00"}
        return timeFrom
    }
    
    func getToTime() -> String {
        guard let selected = selectedDay, let selectedDayName = selected.dayName, let day = workingHoursArr.filter({$0.dayName == selectedDayName}).first, let timeTo = day.timeTo
        else {return "23:55"}
        return timeTo
    }
}

extension WorkingHoursView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return workingHoursArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let hour = workingHoursArr[indexPath.row]
        let identifier = indexPath.row % 2 == 0 ? "HourRightCell": "HourLeftCell"
        if let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? HoursTableViewCell {
            cell.configure(workingHour: hour)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            viewModel.deleteTime(timeId: workingHoursArr[indexPath.row].shopTimeId ?? 0)
        }
    }
    
}

