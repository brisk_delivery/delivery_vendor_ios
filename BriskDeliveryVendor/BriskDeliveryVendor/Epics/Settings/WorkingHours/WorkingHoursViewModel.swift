//
//  WorkingHoursViewModel.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 7/4/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class WorkingHoursViewModel: BaseViewModel {
    let settingManager: SettingManager
    var daysResponse: Dynamic<[ShopDay]> = Dynamic([ShopDay]())
    var workingHoursResponse: Dynamic<[WorkingHour]> = Dynamic([WorkingHour]())
    var enableEnteraction: Dynamic<Bool> = Dynamic(true)
    
    init(router: Router, settingManager: SettingManager) {
        self.settingManager = settingManager
        super.init(router: router, isLoading: false)
    }
    
    func getWorkingHours(){
        guard let shopId = checkForShopId() else  {
            return
        }
        enableEnteraction.value = false
        startLoading()
        settingManager.getWorkingHours(shopId: "\(shopId)", complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
            if let result = response, let status = result.status, status == 1, let hours = result.data{
                if hours.count > 0 {self.workingHoursResponse.value = hours}
//                print(response)
//                self.successUpdate.value = true
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "someThingWentWrong".localized(), ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
    
    func getDays(){
            enableEnteraction.value = false
            startLoading()
        settingManager.getDays(complation: { response in
                self.enableEnteraction.value = true
                self.stopLoading()
            if let result = response, let status = result.status, status == 1, let days = result.data, days.count > 0 {
                    print(days)
                    self.daysResponse.value = days
    //                self.successUpdate.value = true
                }else {
                    self.showAlertMessage(title: "", message: response?.message ?? "someThingWentWrong".localized(), ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
                }
            }) { error in
                self.enableEnteraction.value = true
                self.stopLoading()
                self.generalErrorMessage(error: error)
            }
        }
    
    func saveTime(time: TimeRequest){
            enableEnteraction.value = false
            startLoading()
        settingManager.saveTime(time: time, complation: { response in
                self.enableEnteraction.value = true
                self.stopLoading()
            if let result = response, let status = result.status, status == 1 {
                self.getWorkingHours()
                }else {
                    self.showAlertMessage(title: "", message: response?.message ?? "someThingWentWrong".localized(), ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
                }
            }) { error in
                self.enableEnteraction.value = true
                self.stopLoading()
                self.generalErrorMessage(error: error)
            }
        }
    
    func deleteTime(timeId: Int){
        guard let shopId = checkForShopId() else  {
            return
        }
        enableEnteraction.value = false
        startLoading()
        settingManager.deleteTime(shopId: "\(shopId)", timeId: "\(timeId)", complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
        if let result = response, let status = result.status, status == 1 {
            self.getWorkingHours()
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "someThingWentWrong".localized(), ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
}
