//
//  HoursTableViewCell.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 7/18/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

class HoursTableViewCell: UITableViewCell {
//    static let identifier = "DayCollectionViewCell"
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
//    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        containerView.layer.cornerRadius = containerView.frame.height / 2
    }
    
    func configure(workingHour: WorkingHour) {
        dayLabel.text = workingHour.dayName ?? ""
        fromLabel.text = workingHour.timeFrom ?? ""
        toLabel.text = workingHour.timeTo ?? ""
    }
    
}

