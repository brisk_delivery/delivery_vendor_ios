//
//  OnBoardingViewController.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/23/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

class OnBoardingViewController: UIViewController {
    var viewModel: OnBoardingViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = OnBoardingViewModel(router: RouterManager(self),homeManager: HomeManager(), authenticationManager: AuthenticationManager())
        viewModel.getShopData()
        viewModel.updateToken()
        openApp()
        LocationManager.manager.initLocationManager()
    }
    
    func openApp() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            MainRouterManager.launchApplication(openLanguage: Global.languageId == nil ? true : false)
        }
        
    }
}


