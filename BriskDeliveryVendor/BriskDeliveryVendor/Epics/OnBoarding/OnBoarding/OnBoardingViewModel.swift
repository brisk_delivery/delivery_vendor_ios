//
//  OnBoardingViewModel.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/23/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class OnBoardingViewModel: BaseViewModel {
    let homeManager: HomeManager
    let authenticationManager: AuthenticationManager
    var enableEnteraction: Dynamic<Bool> = Dynamic(true)
    var shopResponse: Dynamic<Shop> = Dynamic(Shop())
    
    init(router: Router, homeManager: HomeManager, authenticationManager: AuthenticationManager) {
        self.homeManager = homeManager
        self.authenticationManager = authenticationManager
        super.init(router: router, isLoading: false)
    }

    func getShopData() {
        guard let shopId = Global.shopId  else {
            return
        }
        DispatchQueue.global(qos: .background).async {
            self.homeManager.getShop(shopId: shopId, complation: { response in
                if let result = response, let shop = result.data {
                  Global.shop = shop
                }
            }) { error in }
        }
    }
    
    func updateToken() {
        guard Global.user != nil, Global.accesstoken != nil  else {
            return
        }
         DispatchQueue.global(qos: .background).async {
            self.authenticationManager.updateToken(complation: { response in
            }) { error in }
       }
    }
    
}
