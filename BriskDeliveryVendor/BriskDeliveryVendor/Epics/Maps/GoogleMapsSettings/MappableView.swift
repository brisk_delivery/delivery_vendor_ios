//
//  MappableView.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/11/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import GoogleMaps

protocol MappableView: class {
    var mapView: GMSMapView! { get set }
    var mapContainerView: UIView! { get set }
    func loadMap()
    func updateNavigation()
}

struct MapConstants {
    static let cameraZoom: Float = 10.0
    static let defaltLatitude = 25.266666
    static let defaltLongitude = 55.316666
}


extension MappableView {

    func loadMap() {
        let frame = CGRect(x: 0, y: 0, width: mapContainerView.frame.width, height: mapContainerView.frame.height)
        mapView = GMSMapView.map(withFrame: frame, camera: GMSMapView.getCameraPosition())
        mapView.styleMap()
        mapContainerView.addSubview(mapView)
        setConstraints(subview: mapView, parentView: mapContainerView)
    }

    func updateNavigation() {}

    private func setConstraints(subview: UIView, parentView: UIView) {
        subview.translatesAutoresizingMaskIntoConstraints = false
        subview.heightAnchor.constraint(equalTo: (parentView.heightAnchor), constant: 0).isActive = true
        subview.widthAnchor.constraint(equalTo: (parentView.widthAnchor), constant: 0).isActive = true
        subview.leadingAnchor.constraint(equalTo: parentView.leadingAnchor, constant: 0).isActive = true
        subview.trailingAnchor.constraint(equalTo: parentView.trailingAnchor, constant: 0).isActive = true
    }
}

extension GMSMapView {
    func styleMap() {
        do {
            if let styleURL = Bundle.main.url(forResource: "mapstyle", withExtension: "json") {
                self.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                print("Unable to find mapstyle.json")
            }
        } catch {
            print("One or more of the map styles failed to load. \(error)")
        }
    }

   static func getCameraPosition() -> GMSCameraPosition {
        var camera: GMSCameraPosition!
        LocationManager.manager.getLocation(type: .current, data: "", completionHandler: { (currentLocation) in
            camera = GMSCameraPosition.camera(withLatitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude, zoom: MapConstants.cameraZoom)
        }) {
            camera = GMSCameraPosition.camera(withLatitude: MapConstants.defaltLatitude, longitude: MapConstants.defaltLongitude, zoom: MapConstants.cameraZoom)
        }
        return camera
    }
}

