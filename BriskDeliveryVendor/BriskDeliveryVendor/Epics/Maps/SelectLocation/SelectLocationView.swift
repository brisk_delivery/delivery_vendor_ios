//
//  SelectLocationView.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 12/2/19.
//  Copyright © 2019 Raye7. All rights reserved.
//

import UIKit
import GooglePlaces
import CoreLocation
import Localize_Swift


struct NearbyPlace {
    var placeName: String
    var address: String
    var placeId: String
    var coordinate: CLLocationCoordinate2D?
    var counter: Int
}

class Location {
var name: String?
var longitude, latitude: Double?
    init(name: String, longitude: Double, latitude: Double) {
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
    }
}

protocol SelectLocationViewDelegate: class {
    func didSelectLocation(location: Location)
}

class SelectLocationView: UIViewController {
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var searchView: UIView!
    
    var mapView: CurrentLocationMapView?
    lazy var geocoder = CLGeocoder()
    var confirmationView: LocationConfirmationPopup!
    weak var delegate: SelectLocationViewDelegate?
    var markerCurrentLocation: NearbyPlace!
    let searchController = UISearchController(searchResultsController: nil)
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setStyle()
    }
    
    private func setStyle()  {
        backImage.image = Localize.currentLanguage() == "ar"  ? #imageLiteral(resourceName: "ic_right_back"): #imageLiteral(resourceName: "ic_left_arrow")
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MapSegue" {
            if let mapView = segue.destination as? CurrentLocationMapView {
                self.mapView = mapView
                mapView.delegate = self
            }
        }
    }

    @IBAction func openAutoCompleteButtonAction(_ sender: UIButton) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) | UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.coordinate.rawValue)|UInt(GMSPlaceField.formattedAddress.rawValue))!
        autocompleteController.placeFields = fields
        autocompleteController.modalPresentationStyle = .popover
        present(autocompleteController, animated: true, completion: nil)
    }

    @IBAction func backButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func confirmButtonAction(_ sender: UIButton) {
        didSelectMarker()
    }
}

extension SelectLocationView: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        viewController.dismiss(animated: true, completion: nil)
        let place = NearbyPlace(placeName: place.name ?? "", address: place.formattedAddress ?? "", placeId: place.placeID ?? "", coordinate: CLLocationCoordinate2D(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude), counter: 2)
        mapView?.set(markerPlace: place)
        mapView?.update(location: place.coordinate)
    }

    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }

    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }

}

extension SelectLocationView: CurrentLocationMapViewDelegate {
    func markerLocationDidChange(place: NearbyPlace) {
       markerCurrentLocation = place
    }
        
    func didSelectMarker() {
        guard let place = markerCurrentLocation else {
            return
        }
        self.showIndicator()
        if place.address == "" {
            let location = CLLocation(latitude: place.coordinate?.latitude ?? 0, longitude: place.coordinate?.longitude ?? 0)
            geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
                self.processResponse(withPlacemarks: placemarks, error: error)
            }
        } else {
            self.hideIndicator()
            confirmationPopup(place: place)
        }
    }

    private func processResponse(withPlacemarks placemarks: [CLPlacemark]?, error: Error?) {
        self.hideIndicator()
        if error == nil {
            if let placemarks = placemarks, let placemark = placemarks.first {
                let place = NearbyPlace(placeName: placemark.name ?? "", address: placemark.compactAddress ?? "", placeId: "", coordinate: CLLocationCoordinate2D(latitude: placemark.location?.coordinate.latitude ?? 0, longitude: placemark.location?.coordinate.longitude ?? 0), counter: 0)
                confirmationPopup(place: place)
            }
        }
    }
}

extension CLPlacemark {

    var compactAddress: String? {
        if let name = name {
            var result = name

            if let street = thoroughfare {
                result += ", \(street)"
            }

            if let city = locality {
                result += ", \(city)"
            }

            if let country = country {
                result += ", \(country)"
            }

            return result
        }

        return nil
    }

}

extension SelectLocationView: LocationConfirmationPopupProtocol {
    func selectPlace(place: NearbyPlace) {
        delegate?.didSelectLocation(location: Location(name: place.placeName, longitude: place.coordinate?.longitude ?? 0, latitude: place.coordinate?.latitude ?? 0))
        dismiss(animated: true, completion: nil)
    }

    func cancelPlace() {
        if confirmationView != nil {
            confirmationView.removeFromSuperview()
            confirmationView = nil
        }
    }

    func confirmationPopup(place: NearbyPlace) {
        if confirmationView == nil {
            confirmationView = LocationConfirmationPopup(place: place)
            confirmationView.delegate = self
            view.addSubview(confirmationView)
            view.setXIBConstraints(xib: confirmationView)
        }
    }

}
extension SelectLocationView: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    // TODO
  }
}
