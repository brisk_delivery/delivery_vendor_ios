//
//  LocationConfirmationPopup.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 12/3/19.
//  Copyright © 2019 Raye7. All rights reserved.
//

import UIKit
import Kingfisher

protocol LocationConfirmationPopupProtocol: class {
    func selectPlace(place: NearbyPlace)
    func cancelPlace()
}

class LocationConfirmationPopup: UIView {
    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var placeAddressLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!

    var place: NearbyPlace!
    weak var delegate: LocationConfirmationPopupProtocol?

    init(place: NearbyPlace) {
        let frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        super.init(frame: frame)
        commonInit()
        setPlaceData(place)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private func commonInit() {
        let view = Bundle.main.loadNibNamed("LocationConfirmationPopup", owner: self, options: nil)?.first as? UIView
        addSubview(view ?? UIView())
        view?.frame = bounds
        view?.autoresizingMask = [
            UIView.AutoresizingMask.flexibleWidth,
            UIView.AutoresizingMask.flexibleHeight
        ]
        initUI()
    }
    
    private func initUI() {
        cancelButton.dropShadow(opacity: 0.26, offSet: CGSize(width: 2, height: 2), radius: 3)
    }

    func setPlaceData(_ place: NearbyPlace) {
        self.place = place
        placeNameLabel.text = place.placeName
        placeAddressLabel.text = place.address
    }

    func createMarker(lat: Double, long: Double) -> String {
        return "size:mid|color:green|\(lat),\(long)"
    }

    @IBAction func okButtonAction(_ sender: UIButton) {
        delegate?.selectPlace(place: place)
    }

    @IBAction func cancelButtonAction(_ sender: UIButton) {
        delegate?.cancelPlace()
    }
}
