//
//  CurrentLocationMapView.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 12/3/19.
//  Copyright © 2019 Raye7. All rights reserved.
//

import UIKit
import GoogleMaps

protocol CurrentLocationMapViewDelegate: class {
    func markerLocationDidChange(place: NearbyPlace)
}

class CurrentLocationMapView: UIViewController, MappableView {
    @IBOutlet weak var mapContainerView: UIView!
    var mapView: GMSMapView!
    private var loaded = false
    weak var delegate: CurrentLocationMapViewDelegate?
    private var markerPlace: NearbyPlace?

    override func viewDidLoad() {
        super.viewDidLoad()
        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: GMSMapView.getCameraPosition())
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !loaded {
            update()
        }

    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    func update(location: CLLocationCoordinate2D? = nil) {
        if !loaded {
            loaded = true
            loadMap()
        }
        mapView.clear()
        mapView.delegate = self
        updateMarkers(location: location)
    }

    func set(markerPlace: NearbyPlace) {
        self.markerPlace = markerPlace
    }

    func updateMarkers(location: CLLocationCoordinate2D? = nil) {
        DispatchQueue.main.async {
            let bounds = location != nil ? self.addMarker(location: location!):  self.setCurrentLocationMarker()
            let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: (self.view.frame.height * 0.12) + 100, left: 50, bottom: 20, right: 50))
            self.mapView.animate(with: update)
            self.mapView.animate(toZoom: 13)
            if location != nil {
                self.mapView.animate(toLocation: location!)
            }
        }
    }

    func setCurrentLocationMarker() -> GMSCoordinateBounds {
        var bounds = GMSCoordinateBounds()
        LocationManager.manager.getLocation(type: .current, data: "", completionHandler: { (currentLocation) in
            let position = CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
            self.markerPlace = NearbyPlace(placeName: "", address: "", placeId: "", coordinate: position, counter: 0)
            bounds = self.addMarker(location: position)
            self.delegate?.markerLocationDidChange(place: self.markerPlace!)
        }) {
            let position = CLLocationCoordinate2D(latitude: MapConstants.defaltLatitude, longitude: MapConstants.defaltLongitude)
            self.markerPlace = NearbyPlace(placeName: "", address: "", placeId: "", coordinate: position, counter: 0)
            bounds = self.addMarker(location: position)
            self.delegate?.markerLocationDidChange(place: self.markerPlace!)
        }
        
        return bounds
    }

    func addMarker(location: CLLocationCoordinate2D) -> GMSCoordinateBounds {
        let bounds = GMSCoordinateBounds()
        bounds.includingCoordinate(location)
        let marker = GMSMarker()
        marker.position = location
        marker.title = "Current Location"
        marker.icon = #imageLiteral(resourceName: "ic_marker_big")
        marker.map = mapView
        
        return bounds
    }

}

extension CurrentLocationMapView: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        mapView.clear()
        markerPlace = NearbyPlace(placeName: "", address: "", placeId: "", coordinate: coordinate, counter: 0)
        _ = addMarker(location: coordinate)
        delegate?.markerLocationDidChange(place: markerPlace!)
    }
}
