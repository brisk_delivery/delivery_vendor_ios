//
//  ProductAdditionViewModel.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/30/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class ProductAdditionViewModel: BaseViewModel {
    let productManager: ProductManager
    var enableEnteraction: Dynamic<Bool> = Dynamic(true)
    var additionCreated: Dynamic<Bool> = Dynamic(false)
     var additionUpdated: Dynamic<Bool> = Dynamic(false)
    
    init(router: Router, productManager: ProductManager) {
        self.productManager = productManager
        super.init(router: router, isLoading: false)
    }
    
    func addAddition(productId: Int, addition: Addition) {
        enableEnteraction.value = false
        startLoading()
        productManager.addProductAddition(productId: productId, addition: addition, complation: { response in
            self.enableEnteraction.value = false
            self.stopLoading()
            if let result = response, result.data != nil {
                self.additionCreated.value = true
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
    
    func updateAddition(productId: Int, additionId: Int, addition: Addition) {
        enableEnteraction.value = false
        startLoading()
        productManager.updateProductAddition(additionId: additionId, productId: productId, addition: addition, complation: { response in
            self.enableEnteraction.value = false
            self.stopLoading()
            if let result = response, result.data != nil {
                self.additionUpdated.value = true
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
}

