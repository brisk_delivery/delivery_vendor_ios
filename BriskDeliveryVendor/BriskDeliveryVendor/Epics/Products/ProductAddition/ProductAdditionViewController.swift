//
//  ProductAdditionViewController.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/30/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import Localize_Swift

class ProductAdditionViewController: UIViewController {
    static let identifier = "ProductAdditionViewController"
    var productId: Int!
    var addition: ProductAddition!
    var isUpdate = false
    var viewModel: ProductAdditionViewModel!
    
    @IBOutlet weak var additionNameEnTextField: UITextField!
    @IBOutlet weak var additionNameArTextField: UITextField!
    @IBOutlet weak var additionPriceTextField: UITextField!
    @IBOutlet weak var backImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ProductAdditionViewModel(router: RouterManager(self), productManager: ProductManager())
        initObservables()
        setData()
        setStyle()
    }
    
    private func setStyle()  {
        backImage.image = Localize.currentLanguage() == "ar"  ? #imageLiteral(resourceName: "ic_right_back"): #imageLiteral(resourceName: "ic_left_arrow")
    }
    
    func initObservables() {
        viewModel.isLoading.bindAndFire { shouldLoad in
            shouldLoad ? self.showIndicator() : self.hideIndicator()
        }
        viewModel.enableEnteraction.bindAndFire{ isEnabled in
            self.view.isUserInteractionEnabled = isEnabled
        }
        viewModel.additionCreated.bind{ isCreated in
            if isCreated {
                self.alert(title: "", message: "productAdditionAddedMessage".localized(),  ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1), textColor: .white, handler:{(action) in
                    self.dismiss(animated: true, completion: nil)
                }) ])
                }
        }
        
        viewModel.additionUpdated.bind{ isCreated in
            if isCreated {
                self.alert(title: "", message: "productAdditionUpdatedMessage".localized(),  ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1), textColor: .white, handler:{(action) in
                    self.dismiss(animated: true, completion: nil)
                }) ])
                }
        }
    }
    
    func isValidInputs() -> Bool {
        var result = true
        
        if let nameEn = additionNameEnTextField.text, nameEn.isEmpty {
            additionNameEnTextField.layer.borderColor = UIColor.red.cgColor
            result = false
        }
        
        if let nameAr = additionNameArTextField.text, nameAr.isEmpty {
            additionNameArTextField.layer.borderColor = UIColor.red.cgColor
            result = false
        }
        
        if let price = additionPriceTextField.text, price.isEmpty {
            additionPriceTextField.layer.borderColor = UIColor.red.cgColor
            result = false
        }

        return result
    }
    
    func setData() {
        guard isUpdate, let addition = self.addition else {
            return
        }
        
        if let stringAddition = addition.productAdditionPrice, let additionPrice =  Float(stringAddition) {
            additionPriceTextField.text = "\(additionPrice.cleanValue)"
        } else {
            additionPriceTextField.text = ""
        }
        
        if let translate = addition.translate, translate.count >= 2 {
            additionNameEnTextField.text = translate[0].productAdditionName
            additionNameArTextField.text = translate[1].productAdditionName
        }
        
    }
    
    func createAddition() -> Addition {
        let additionTranslate = [AdditionTranslate(langId: 1, productAdditionName: additionNameEnTextField.text ?? ""),AdditionTranslate(langId: 2, productAdditionName: additionNameArTextField.text ?? "")]
    
        return Addition(productAdditionPrice: (additionPriceTextField.text ?? "").toInt(), translate: additionTranslate)
    }
    
    @IBAction func saveButtonAction(_ sender: UIButton) {
        
        if isValidInputs() {
            if isUpdate, let additionId = addition.productAdditionId {
                viewModel.updateAddition(productId: productId ?? 0, additionId: additionId, addition: createAddition())
            } else {
                viewModel.addAddition(productId: productId ?? 0, addition: createAddition())
            }
        }
        
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension ProductAdditionViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.setTextFieldBorder(borderColor: #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1), borderWidth: 1)
    }
}

