//
//  AddOptionViewController.swift
//  BriskDeliveryVendor
//
//  Created by Amal Elgalant on 12/12/2020.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import Localize_Swift

class AddOptionViewController: UIViewController {
    static let identifier = "AddOptionViewController"
    var productId: Int!
    var option: ProductOptions!
    var isUpdate = false
    var viewModel: AddOptionsViewModel!
    @IBOutlet weak var optionNameEnTextField: UITextField!
    @IBOutlet weak var categoryView: UIStackView!
    @IBOutlet weak var optionNameArTextField: UITextField!
    @IBOutlet weak var optionPriceTextField: UITextField!
    @IBOutlet weak var backImage: UIImageView!
    var optionCategory = [OptionCategory]()
    var categoryID = 0
    @IBOutlet weak var optionsCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = AddOptionsViewModel(router: RouterManager(self), productManager: ProductManager())
        initObservables()
        viewModel.getCategories(by:  Global.user.catID ?? 0)

        setData()
        setStyle()
    }
    
    private func setStyle()  {
        backImage.image = Localize.currentLanguage() == "ar"  ? #imageLiteral(resourceName: "ic_right_back"): #imageLiteral(resourceName: "ic_left_arrow")
    }
    
    func initObservables() {
        viewModel.isLoading.bindAndFire { shouldLoad in
            shouldLoad ? self.showIndicator() : self.hideIndicator()
        }
        viewModel.enableEnteraction.bindAndFire{ isEnabled in
            self.view.isUserInteractionEnabled = isEnabled
        }
        viewModel.optionCreated.bind{ isCreated in
            if isCreated {
                self.alert(title: "", message: "productOptionAddedMessage".localized(),  ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1), textColor: .white, handler:{(action) in
                    self.dismiss(animated: true, completion: nil)
                }) ])
                }
        }
        
        viewModel.optionUpdated.bind{ isCreated in
            if isCreated {
                self.alert(title: "", message: "productOptionUpdatedMessage".localized(),  ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1), textColor: .white, handler:{(action) in
                    self.dismiss(animated: true, completion: nil)
                }) ])
                }
        }
        viewModel.categoryOptionResponse.bind{ categoryOption in
            self.optionCategory = categoryOption
            self.setDataSource(optionCategoryList: categoryOption)
        }
        
    }
    
    func isValidInputs() -> Bool {
        var result = true
        
        if let nameEn = optionNameEnTextField.text, nameEn.isEmpty {
            optionNameEnTextField.layer.borderColor = UIColor.red.cgColor
            result = false
        }
        
        if let nameAr = optionNameArTextField.text, nameAr.isEmpty {
            optionNameArTextField.layer.borderColor = UIColor.red.cgColor
            result = false
        }
        
        if let price = optionPriceTextField.text, price.isEmpty {
            optionPriceTextField.layer.borderColor = UIColor.red.cgColor
            result = false
        }

        return result
    }
    
    func setData() {
        guard isUpdate, let options = self.option else {
            return
        }
        
        if  option.price != nil{
            optionPriceTextField.text = "\(option.price ?? 0.0)"
        } else {
            optionPriceTextField.text = ""
        }
        
        if let translate = options.tranlation, translate.count >= 2 {
            optionNameEnTextField.text = translate[0].name
            optionNameArTextField.text = translate[1].name
        }
       
    }
    func setDataSource(optionCategoryList: [OptionCategory]) {
        self.optionCategory = optionCategoryList
        optionsCollectionView.reloadData()
//        noProductsView.isHidden = self.produstsList.count > 0
        if optionCategoryList.count > 0{
            if isUpdate{
                let index = optionCategoryList.firstIndex(where: {$0.id == option.categoryId}) ?? 0
                optionsCollectionView.selectItem(at: IndexPath(row: index, section: 0), animated: true, scrollPosition: .centeredHorizontally)
                categoryID = option.categoryId!


            }else{
        optionsCollectionView.selectItem(at: [0,0], animated: true, scrollPosition: .centeredHorizontally)
            categoryID = optionCategoryList[0].id ?? 0
            }
            categoryView.isHidden = false

        }
        else{
            self.alert(title: "", message: "Options not supported".localized(),  ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1), textColor: .white, handler:{(action) in
                self.dismiss(animated: true, completion: nil)
            }) ])
            
            categoryView.isHidden = true
        }
        optionsCollectionView.es.stopPullToRefresh(ignoreDate: true)
    }
    func createOption() -> ProductOptions {
        let optionsTranslate = [ProductOptionsTranslation(lang_id: 1, name: optionNameEnTextField.text ?? ""),ProductOptionsTranslation(lang_id: 2, name: optionNameArTextField.text ?? "")]
        
    
       
        return  ProductOptions( price: Double(optionPriceTextField.text ?? "0.00")!, tranlation: optionsTranslate, categoryId: categoryID, shopId: Global.user.shopId ?? 0)
        
    }
    
    @IBAction func saveButtonAction(_ sender: UIButton) {
        
        if isValidInputs() {
            if isUpdate, let optionId = option.id {
                viewModel.updateOption(productId: productId ?? 0, optionId: option.id!, option: createOption())
            } else {
                
                viewModel.addOption(productId: productId ?? 0, option: createOption())
            }
        }
        
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension AddOptionViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.setTextFieldBorder(borderColor: #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1), borderWidth: 1)
        
    }
}
extension AddOptionViewController: UICollectionViewDataSource, UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return optionCategory.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OptionsCategoriesCollectionViewCell", for: indexPath) as! OptionsCategoriesCollectionViewCell
        cell.setData(category: self.optionCategory[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        categoryID = optionCategory[indexPath.row].id ?? 0
    }
}
