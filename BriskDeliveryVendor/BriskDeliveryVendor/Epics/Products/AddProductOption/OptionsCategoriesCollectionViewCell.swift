//
//  OptionsCategoriesCollectionViewCell.swift
//  BriskDeliveryVendor
//
//  Created by Amal Elgalant on 17/12/2020.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
class OptionsCategoriesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var categoryImageView: UIImageView!
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var nameLbl: UILabel!
    
    func setData(category: OptionCategory){
        categoryImageView.kf.setImage(with: URL(string: category.imagePath ?? ""), placeholder: #imageLiteral(resourceName: "logo"), options: [], progressBlock: nil) { (image, _, _, _) in
        }
        self.nameLbl.text = category.name

    }
    override var isSelected: Bool {
         didSet {
            self.categoryImageView.borderColor = isSelected ? #colorLiteral(red: 0.4446130991, green: 0.2663431466, blue: 0.7844645381, alpha: 1) : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            self.nameLbl.textColor = isSelected ? #colorLiteral(red: 0.4446130991, green: 0.2663431466, blue: 0.7844645381, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
         }
       }
}
