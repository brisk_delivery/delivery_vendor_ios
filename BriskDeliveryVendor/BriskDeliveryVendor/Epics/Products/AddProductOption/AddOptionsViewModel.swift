//
//  AddOptionsViewModel.swift
//  BriskDeliveryVendor
//
//  Created by Amal Elgalant on 12/12/2020.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

class AddOptionsViewModel:  BaseViewModel {
    let productManager: ProductManager
    var enableEnteraction: Dynamic<Bool> = Dynamic(true)
    var optionCreated: Dynamic<Bool> = Dynamic(false)
     var optionUpdated: Dynamic<Bool> = Dynamic(false)
    var categoryOptionResponse: Dynamic<[OptionCategory]> = Dynamic([OptionCategory]())

    init(router: Router, productManager: ProductManager) {
        self.productManager = productManager
        super.init(router: router, isLoading: false)
    }
    
    func addOption(productId: Int, option: ProductOptions) {
        enableEnteraction.value = false
        startLoading()
        productManager.addProductOption(productId: productId, option: option, complation: { response in
            self.enableEnteraction.value = false
            self.stopLoading()
            if let result = response, result.data != nil {
                self.optionCreated.value = true
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
    
    func updateOption(productId: Int, optionId: Int, option: ProductOptions) {
        enableEnteraction.value = false
        startLoading()
        productManager.updateProductOption(optionId: optionId, productId: productId, option: option, complation: { response in
            self.enableEnteraction.value = false
            self.stopLoading()
            if let result = response, result.data != nil {
                self.optionUpdated.value = true
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
    func getCategories(by categoryId: Int) {
      
        if checkPermission() {
            enableEnteraction.value = false
            startLoading()
            //TODO: - adding shop & ctaegory
            productManager.getCategoryOptions(categoryId: categoryId, complation: { response in
                self.enableEnteraction.value = true
                self.stopLoading()
                if let result = response, let categoryOptionResponse = result.data {
                    self.categoryOptionResponse.value = categoryOptionResponse
                }else {
                    self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
                }
            }) { error in
                self.enableEnteraction.value = true
                self.stopLoading()
                self.generalErrorMessage(error: error)
            }
        }
    }
}

