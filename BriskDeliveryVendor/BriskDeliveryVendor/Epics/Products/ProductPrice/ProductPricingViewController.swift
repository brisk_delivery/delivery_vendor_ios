//
//  ProductPricingViewController.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/18/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import Localize_Swift

class ProductPricingViewController: UIViewController {
    static let identifier = "ProductPricingViewController"
    private var product: AddProductRequest!
    private var productImage: Data!
    private var mediaMimeType: String!
    var viewModel: ProductPricingViewModel!
    
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var sizeNameEnTextField: UITextField!
    @IBOutlet weak var sizeNameArTextField: UITextField!
    @IBOutlet weak var sizePriceTextField: UITextField!
    @IBOutlet weak var additionNameEnTextField: UITextField!
    @IBOutlet weak var additionNameArTextField: UITextField!
    @IBOutlet weak var additionPriceTextField: UITextField!
    
    static func create(product: AddProductRequest,productImage: Data, mediaMimeType: String) -> ProductPricingViewController {
        let storyBoard = UIStoryboard(name: "Products", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: identifier) as! ProductPricingViewController
        vc.product = product
        vc.productImage = productImage
        vc.mediaMimeType = mediaMimeType
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ProductPricingViewModel(router: RouterManager(self), productManager: ProductManager())
        initObservables()
        setStyle()
    }
    
    private func setStyle()  {
        backImage.image = Localize.currentLanguage() == "ar"  ? #imageLiteral(resourceName: "ic_right_back"): #imageLiteral(resourceName: "ic_left_arrow")
    }
    
    func initObservables() {
        viewModel.isLoading.bindAndFire { shouldLoad in
            shouldLoad ? self.showIndicator() : self.hideIndicator()
        }
        viewModel.enableEnteraction.bindAndFire{ isEnabled in
            self.view.isUserInteractionEnabled = isEnabled
        }
        viewModel.productCreated.bind{ isCreated in
            if isCreated {
                self.alert(title: "", message: "productCreateMessage".localized(),  ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1), textColor: .white, handler:{(action) in
                    self.dismiss(animated: true, completion: nil)
                }) ])
                }
        }
    }
    
    func isValidInputs() -> Bool {
        var result = true
        
        if let nameEN = sizeNameEnTextField.text, nameEN.isEmpty {
            sizeNameEnTextField.layer.borderColor = UIColor.red.cgColor
            result = false
        }
        
        if let sizeEn = sizePriceTextField.text, sizeEn.isEmpty {
            sizePriceTextField.layer.borderColor = UIColor.red.cgColor
            result = false
        }
        return result
    }
    
    func updateProduct() {
        let sizeTranslate = [SizeTranslate(langId: 1, productSize: sizeNameEnTextField.text ?? ""), SizeTranslate(langId: 2, productSize: sizeNameArTextField.text ?? "")]
        product.prices = Price(productPrice: (sizePriceTextField.text ?? "").toInt(), translate: sizeTranslate)
        
        if additionNameEnTextField.text != "", additionPriceTextField.text != "" {
            let additionTranslate = [AdditionTranslate(langId: 1, productAdditionName: additionNameEnTextField.text ?? ""),AdditionTranslate(langId: 2, productAdditionName: additionNameArTextField.text ?? "")]
            
            product.addition = Addition(productAdditionPrice: (additionPriceTextField.text ?? "").toInt(), translate: additionTranslate)
        }
    }
    
    @IBAction func saveButtonAction(_ sender: UIButton) {
        if isValidInputs() {
            updateProduct()
            viewModel.addProduct(product: product, productImage: productImage, mediaMimeType: mediaMimeType)
        }
        
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension ProductPricingViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.setTextFieldBorder(borderColor: #colorLiteral(red: 0.4984278679, green: 0.3929923773, blue: 0.7918989062, alpha: 1), borderWidth: 1)
    }
}
