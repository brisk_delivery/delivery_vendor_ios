//
//  DeletePopupView.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 5/13/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import Localize_Swift

protocol DeletePopupViewDelegate: class {
    func deleteAction(id: Int)
    func cancelDeleteAction()
}

class DeletePopupView: UIView {
    @IBOutlet weak var confirmationLabel: UILabel!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var popupContainerView: CustomView!
    
    private weak var delegate: DeletePopupViewDelegate!
    var idToDelete: Int!
    
    init(idToDelete: Int, confirmationText: String, delegate: DeletePopupViewDelegate) {
        self.delegate = delegate
        self.idToDelete = idToDelete
        super.init(frame: UIScreen.main.bounds)
        let view = Bundle.main.loadNibNamed("DeletePopupView", owner: self, options: nil)?.first as? UIView
             addSubview(view ?? UIView())
             view?.frame = bounds
             view?.autoresizingMask = [
                UIView.AutoresizingMask.flexibleWidth,
                UIView.AutoresizingMask.flexibleHeight
             ]
        initUI(confirmationText: confirmationText)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func initUI(confirmationText: String) {
        confirmationLabel.text = confirmationText
        backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTappedOutside)))
        popupContainerView.corners = Localize.currentLanguage() == "ar"  ? "2 3 4" : "1 3 4"
    }

    @objc func handleTappedOutside() {
        delegate.cancelDeleteAction()
    }

    @IBAction func deleteButtonAction(_ sender: UIButton) {
        delegate.deleteAction(id: idToDelete)
        handleTappedOutside()
    }
    
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        handleTappedOutside()
    }
}
