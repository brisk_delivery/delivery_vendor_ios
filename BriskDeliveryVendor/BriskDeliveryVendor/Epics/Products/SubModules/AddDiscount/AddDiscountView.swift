//
//  AddDiscountView.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 5/14/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import Localize_Swift
import DatePickerDialog

protocol AddDiscountViewDelegate: class {
    func saveDiscount(discount: DiscountRequest)
    func cancelAction()
    func showError(message: String)
}

class AddDiscountView: UIView {
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var discounttextField: UITextField!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var popupContainerView: CustomView!
    @IBOutlet weak var startDateView: UIView!
    @IBOutlet weak var endDateView: UIView!
    
    private weak var delegate: AddDiscountViewDelegate!
    var discount: DiscountRequest
    var isUpdate: Bool
    
    var formatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter
    }
    
    
    init(discount: DiscountRequest, delegate: AddDiscountViewDelegate, isUpdate: Bool) {
        self.delegate = delegate
        self.discount = discount
        self.isUpdate = isUpdate
        super.init(frame: UIScreen.main.bounds)
        let view = Bundle.main.loadNibNamed("AddDiscountView", owner: self, options: nil)?.first as? UIView
        addSubview(view ?? UIView())
        view?.frame = bounds
        view?.autoresizingMask = [
            UIView.AutoresizingMask.flexibleWidth,
            UIView.AutoresizingMask.flexibleHeight
        ]
        initUI()
    }
    
    required init?(coder: NSCoder) {
        discount = DiscountRequest(priceId: 0, productId: 0)
        isUpdate = false
        super.init(coder: coder)
    }
    
    private func initUI() {
        backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hidePopupView)))
        popupContainerView.corners = Localize.currentLanguage() == "ar"  ? "2 3 4" : "1 3 4"
        handleUpdate()
    }
    
    func handleUpdate()  {
        if isUpdate {
            discounttextField.text = "\(discount.discountValue ?? 0)"
            startDateLabel.text = discount.startDate ?? ""
            endDateLabel.text = discount.endDate ?? ""
        }
    }
    
    @objc func hidePopupView() {
        delegate.cancelAction()
    }
    
    func isValidInputs() -> Bool {
        var result = true
        
        if let discount = discounttextField.text, discount.isEmpty {
            discounttextField.layer.borderColor = UIColor.red.cgColor
            result = false
        }
        
        if discount.startDate == nil {
            startDateView.layer.borderColor = UIColor.red.cgColor
            result = false
        }
        
        if discount.endDate == nil {
            endDateView.layer.borderColor = UIColor.red.cgColor
            result = false
        }
        return result
    }
    
    @IBAction func saveButtonAction(_ sender: UIButton) {
        if isValidInputs() {
            
            
            delegate.saveDiscount(discount: createDiscount())
            hidePopupView()
        }
    }
    
    func createDiscount() -> DiscountRequest {
        return DiscountRequest(offerId: discount.offerId,priceId: discount.priceId, productId: discount.productId, startDate: discount.startDate, endDate: discount.endDate, discountValue: Int(discounttextField.text!) ?? 0)
    }
    
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        hidePopupView()
    }
    
    @IBAction func selectFromButtonAction(_ sender: UIButton) {
        startDateView.layer.borderColor = #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1)
        let startDate = Calendar.current.date(byAdding: .weekday, value: -5, to: Date())
        let endDate = Calendar.current.date(byAdding: .year, value: 1, to: Date())
        DatePickerDialog().show("DatePicker", doneButtonTitle: "ok".localized(), cancelButtonTitle: "cancel".localized(), minimumDate: startDate, maximumDate: endDate, datePickerMode: .dateAndTime) { (date) -> Void in
            if let date = date {
                let stringDate = self.formatter.string(from: date)
                self.startDateLabel.text = stringDate
                self.discount.startDate = stringDate
            }
        }
    }
    
    @IBAction func selectEndButtonAction(_ sender: UIButton) {
        endDateView.layer.borderColor = #colorLiteral(red: 0.4988232255, green: 0.375476867, blue: 0.8019280434, alpha: 1)
        if let startDateString = discount.startDate, let startDate = formatter.date(from: startDateString) {
            
            let start = Calendar.current.date(byAdding: .day, value: 1, to: Date())
            let end = Calendar.current.date(byAdding: .year, value: 1, to: startDate)
            DatePickerDialog().show("DatePicker", doneButtonTitle: "ok".localized(), cancelButtonTitle: "cancel".localized(), minimumDate: start, maximumDate: end, datePickerMode: .dateAndTime) { (date) -> Void in
                if let date = date {
                    let stringDate = self.formatter.string(from: date)
                    self.endDateLabel.text = stringDate
                    self.discount.endDate = stringDate
                }
            }
        } else {
            delegate?.showError(message: "selectDateMessage".localized())
        }
    }
}

extension AddDiscountView: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.layer.borderColor = #colorLiteral(red: 0.4988232255, green: 0.375476867, blue: 0.8019280434, alpha: 1)
    }
}


