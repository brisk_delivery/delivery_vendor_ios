//
//  PriceOptionsView.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 5/18/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import Localize_Swift

protocol PriceOptionsViewDelegate: class {
    func editPrice(price: ProductDetailsPrice)
    func deletePrice(price: ProductDetailsPrice)
    func discountPrice(price: ProductDetailsPrice)
}

class PriceOptionsView: UIView {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var arrowImageview: UIImageView!
    
    var price: ProductDetailsPrice
    weak var delegate: PriceOptionsViewDelegate!
    
    init(price: ProductDetailsPrice, delegate: PriceOptionsViewDelegate) {
        self.price = price
        self.delegate = delegate
        super.init(frame: CGRect(x: 0, y: 0, width: 130, height: 150))
        let view = Bundle.main.loadNibNamed("PriceOptionsView", owner: self, options: nil)?.first as? UIView
             addSubview(view ?? UIView())
        setXIBConstraints(xib: view ?? UIView())
             view?.frame = bounds
             view?.autoresizingMask = [
                UIView.AutoresizingMask.flexibleWidth,
                UIView.AutoresizingMask.flexibleHeight
             ]
        initUI()
    }
    
    required init?(coder: NSCoder) {
        price = ProductDetailsPrice()
        delegate = nil
        super.init(coder: coder)
    }
    
    private func initUI() {
        containerView.dropShadow(opacity: 0.26, offSet: CGSize(width: 0, height: 2), radius: 2)
        
        arrowImageview.image = Localize.currentLanguage() == "ar"  ? #imageLiteral(resourceName: "ic_arrow_white_left"): #imageLiteral(resourceName: "ic_white_arrow")
    }

    @IBAction func editButtonAction(_ sender: UIButton) {
        delegate.editPrice(price: price)
    }
    
    @IBAction func deleteButtonAction(_ sender: UIButton) {
        delegate.deletePrice(price: price)
    }
    
    @IBAction func discountButtonAction(_ sender: UIButton) {
        delegate.discountPrice(price: price)
    }
}


