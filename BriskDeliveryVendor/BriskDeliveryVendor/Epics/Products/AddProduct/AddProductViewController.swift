//
//  AddProductViewController.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/18/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import Localize_Swift

class AddProductViewController: UIViewController {
    static let identifier = "AddProductViewController"
    var viewModel: AddProductViewModel!
    var productImage: UIImage!
    var categories = [Category]()
    var isUpdate = false
    var productDetails: ProductDetails!
    var selectedProductType: Category! {
        didSet {
            productTypeContainerView.isHidden = true
            productTypeLabel.text = selectedProductType.catName
        }
    }
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var productTypeLabel: UILabel!
    @IBOutlet weak var productNameEnTextField: UITextField!
    @IBOutlet weak var productNameArTextField: UITextField!
    @IBOutlet weak var productDescEnTextField: UITextField!
    @IBOutlet weak var productDescArTextField: UITextField!
    @IBOutlet weak var featureItemImage: UIImageView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productTypeContainerView: UIView!
    @IBOutlet weak var productTypeView: UIView!
    @IBOutlet weak var productImageContainerView: UIView!
    @IBOutlet weak var continueButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = AddProductViewModel(router: RouterManager(self), productManager: ProductManager())
        initObservables()
        viewModel.getProductType()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.hideProductTypeView))
        tap.delegate = self
        view.addGestureRecognizer(tap)
        if isUpdate {
            setData()
        }
    }
    
    private func setStyle()  {
        productTypeContainerView.dropShadow(opacity: 0.26, offSet: CGSize(width: 1, height: 2), radius: 3)
        backImage.image = Localize.currentLanguage() == "ar"  ? #imageLiteral(resourceName: "ic_right_back"): #imageLiteral(resourceName: "ic_left_arrow")
        titleLabel.text = isUpdate ? "editProduct".localized(): "addProduct".localized()
        let buttontitle = isUpdate ? "edit".localized() : "continue".localized()
        continueButton.setTitle(buttontitle, for: .normal)
        UIView.appearance().semanticContentAttribute = Localize.currentLanguage() == "ar"  ? .forceRightToLeft : .forceLeftToRight
        productTypeLabel.semanticContentAttribute = Localize.currentLanguage() == "ar"  ? .forceRightToLeft : .forceLeftToRight
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setStyle()
    }
    
    //MARK:- Observables
    func initObservables() {
        viewModel.isLoading.bindAndFire { shouldLoad in
            shouldLoad ? self.showIndicator() : self.hideIndicator()
        }
        viewModel.enableEnteraction.bindAndFire{ isEnabled in
            self.view.isUserInteractionEnabled = isEnabled
        }
        viewModel.categoriesResponse.bind{ categories in
            self.categories = categories
        }
        viewModel.productUpdated.bind{ isCreated in
            if isCreated {
                self.alert(title: "", message: "productUpdateMessage".localized(),  ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1), textColor: .white, handler:{(action) in
                    self.dismiss(animated: true, completion: nil)
                }) ])
                }
        }
    }
    
    @objc func hideProductTypeView() {
        productTypeContainerView.isHidden = true
    }
    
    private func initProductTypeViewController() {
        let list = categories.map{GeneralItem(id: $0.catId ?? 0, title: $0.catName ?? "")}
        addSelectOneViewControllerToContainer(list: list, containerView: productTypeContainerView, didSelectHandler: productTypeSelected(item:))
    }
    
    func productTypeSelected(item: GeneralItem) {
        if let selected = categories.filter({$0.catId == item.id}).first {
            selectedProductType = selected
        }
    }
    
    private func addSelectOneViewControllerToContainer(list: [GeneralItem], containerView: UIView, didSelectHandler: @escaping (GeneralItem) -> ()){
        let vc = SelectOneViewController.create(list, selectItemHandler: didSelectHandler)
        containerView.addSubview(vc.view)
        self.addChild(vc)
        vc.didMove(toParent: self)
        vc.view.frame = containerView.frame
        containerView.setXIBConstraints(xib: vc.view)
        containerView.isHidden = false
    }
    
    
    func isValidInputs() -> Bool {
        var result = true
        
        if productImage == nil || (!productImage.isSizeValid(type: .product) && !isUpdate) {
            productImageContainerView.layer.borderColor = UIColor.red.cgColor
            self.alert(title: "", message: "productImageMessage".localized(), ActionButtons: [(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red , textColor: .white, handler:{(action) in }) ])
            result = false
        }
        
        if selectedProductType == nil {
            productTypeView.layer.borderColor = UIColor.red.cgColor
            result = false
        }
        
        if let nameEN = productNameEnTextField.text, nameEN.isEmpty {
            productNameEnTextField.layer.borderColor = UIColor.red.cgColor
            result = false
        }
        
//        if let nameAr = productNameArTextField.text, nameAr.isEmpty {
//            productNameArTextField.layer.borderColor = UIColor.red.cgColor
//            result = false
//        }
        
        if let descEn = productDescEnTextField.text, descEn.isEmpty {
            productDescEnTextField.layer.borderColor = UIColor.red.cgColor
            result = false
        }
        
//        if let descAr = productDescArTextField.text, descAr.isEmpty {
//            productDescArTextField.layer.borderColor = UIColor.red.cgColor
//            result = false
//        }
                
        return result
    }
    
    func createProductRequest() -> AddProductRequest {
        let productTranslate = [ProductTranslate(langId: 1, productDesc: productDescEnTextField.text ?? "", productName: productNameEnTextField.text ?? ""), ProductTranslate(langId: 2, productDesc: productDescArTextField.text ?? "", productName: productNameArTextField.text ?? "")]
        //TODO: - remove fixed shop id
        return AddProductRequest(shopId: Global.shopId, catId: selectedProductType.catId ?? 0, translate: productTranslate, isFeature: featureItemImage.isHidden ? 0 : 1)
    }
    
    @IBAction func continueButtonAction(_ sender: UIButton) {
        guard isValidInputs(), let productImageResized = productImage.resizeImage(newWidth: 250), let productImageData = productImageResized.jpegData(compressionQuality: 1) else {
            return
        }
        if isUpdate {
            viewModel.updateProduct(productId: productDetails.productId ?? 0,product: createUpdateProductRequest() , productImage: productImageData, mediaMimeType: "image/jpg")
            
        } else {
        let vc = ProductPricingViewController.create(product: createProductRequest(), productImage: productImageData, mediaMimeType: "image/jpg")
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func productImageButtonAction(_ sender: UIButton) {
        productImageContainerView.layer.borderColor = #colorLiteral(red: 0.9786400199, green: 0.7947712541, blue: 0.3013051748, alpha: 1)
        CameraHandler.shared.showActionSheet(vc: self)
        CameraHandler.shared.imagePickedBlock = { (image) in
            self.productImageView.image = image
            self.productImage = image
        }
    }
    
    @IBAction func featureItemButtonAction(_ sender: Any) {
        featureItemImage.isHidden = !featureItemImage.isHidden
    }
    
    
    @IBAction func productTypeButtonAction(_ sender: UIButton) {
        productTypeContainerView.isHidden ? initProductTypeViewController() : hideProductTypeView()
    }
}

extension AddProductViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        productTypeContainerView.isHidden = true
        textField.setTextFieldBorder(borderColor: #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1), borderWidth: 1)
    }
}

extension AddProductViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isDescendant(of: productTypeContainerView) == true {
            return false
        }
        return true
    }
}
