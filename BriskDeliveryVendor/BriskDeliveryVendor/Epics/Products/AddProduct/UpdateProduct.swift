//
//  UpdateProduct.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/29/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import Localize_Swift

extension AddProductViewController {
    func setData() {
        guard isUpdate, let product = productDetails else {
            return
        }
        productImageView.kf.setImage(with: URL(string: product.profilePath ?? ""), placeholder: #imageLiteral(resourceName: "ic_product_default"), options: [], progressBlock: nil) { (image, _, _, _) in
            if let img = image {self.productImage = img}
        }
        if let category = product.category {
            selectedProductType = category
        }
        if let isFeatured = product.isFeatured, isFeatured == 1 {
            featureItemImage.isHidden = false
        } else {
            featureItemImage.isHidden = true
        }
        if let translateArr = product.translates, translateArr.count >= 2 {
            productNameEnTextField.text = translateArr[0].productName ?? ""
            productNameArTextField.text = translateArr[1].productName ?? ""
            
            productDescEnTextField.text = translateArr[0].productDesc ?? ""
            productDescArTextField.text = translateArr[1].productDesc ?? ""
        }
    }
    
    func createUpdateProductRequest() -> UpdateProductRequest {
        let productTranslate = [ProductTranslate(langId: 1, productDesc: productDescEnTextField.text ?? "", productName: productNameEnTextField.text ?? ""), ProductTranslate(langId: 2, productDesc: productDescArTextField.text ?? "", productName: productNameArTextField.text ?? "")]

        return UpdateProductRequest(shopId:  Global.shopId, catId: selectedProductType.catId ?? 0, translate: productTranslate, isFeature: featureItemImage.isHidden ? 0 : 1)
    }
}
