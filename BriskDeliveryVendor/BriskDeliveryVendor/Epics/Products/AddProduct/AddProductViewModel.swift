//
//  AddProductViewModel.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/19/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class AddProductViewModel: BaseViewModel {
    let productManager: ProductManager
    var enableEnteraction: Dynamic<Bool> = Dynamic(true)
    var categoriesResponse: Dynamic<[Category]> = Dynamic([Category]())
    var productUpdated: Dynamic<Bool> = Dynamic(false)
    
    init(router: Router, productManager: ProductManager) {
        self.productManager = productManager
        super.init(router: router, isLoading: false)
    }
    
    //TODO: remove fixed shop id
    func getProductType() {
        guard let shop = Global.shop, let category = shop.category, let categoryId = category.catId else  {
            return
        }
        productManager.getSubCategories(categoryId: categoryId,complation: { response in
            if let result = response, let categories = result.data {
                self.categoriesResponse.value = categories
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.generalErrorMessage(error: error)
        }
    }
    
    func updateProduct(productId: Int,product: UpdateProductRequest, productImage: Data, mediaMimeType: String) {
        enableEnteraction.value = false
        startLoading()
        productManager.updateProduct(productId: productId,product: product, complation: { response in
            self.enableEnteraction.value = false
            self.startLoading()
            if let result = response, let data = result.data {
                self.updateProductImage(productId: data.productId ?? 0, productImage: productImage, mediaMimeType: mediaMimeType)
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
    
    func updateProductImage(productId: Int, productImage: Data, mediaMimeType: String) {
        enableEnteraction.value = false
        startLoading()
        productManager.updateProductImage(productId: productId, productImage: productImage, mediaMimeType: mediaMimeType, complation: { response in
            self.enableEnteraction.value = false
            self.stopLoading()
            if let result = response, result.data != nil {
                NotificationCenter.default.post(name: .refreshProducts, object: nil)
                self.productUpdated.value = true
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
    
}

