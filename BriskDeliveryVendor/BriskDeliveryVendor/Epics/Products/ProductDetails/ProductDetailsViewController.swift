//
//  ProductDetailsViewController.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/27/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import Localize_Swift

@objc enum ProductPopups: Int {
    case options
    case discount
}

class ProductDetailsViewController: UIViewController {
    @IBOutlet weak var unavailableSwitch: UISwitch!
    var viewModel: ProductDetailsViewModel!
    var productId: Int!
    var productDetails: ProductDetails!
    var productImage: UIImage!
    var priceArr = [ProductDetailsPrice]()
    var additionArr = [ProductAddition]()
    var optionsArr = [ProductOptions]()
    var discountView: AddDiscountView!
    var optionsView: PriceOptionsView!
    
    @IBOutlet weak var optionsTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var optionsTableView: UITableView!
    
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var containerView: UIScrollView!
    @IBOutlet weak var addOnsContainerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var priceContainerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var nameArContainerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var nameEnContainerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var addOnsTableView: UITableView!
    @IBOutlet weak var priceTableView: UITableView!
    @IBOutlet weak var productDescEnLabel: UILabel!
    @IBOutlet weak var productNameArLabel: UILabel!
    @IBOutlet weak var productDescArLabel: UILabel!
    @IBOutlet weak var productNameEnLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ProductDetailsViewModel(router: RouterManager(self), productManager: ProductManager())
         view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(removePopupView(_:))))
        initObservables()
    }
    
    private func setStyle()  {
        backImage.image = Localize.currentLanguage() == "ar"  ? #imageLiteral(resourceName: "ic_right_back"): #imageLiteral(resourceName: "ic_left_arrow")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.getProduct(productId: productId ?? 0)
        viewModel.getProductStatus(productId: productId ?? 0)

        setStyle()
    }
    
    
    //MARK:- Observables
    func initObservables() {
        viewModel.isLoading.bindAndFire { shouldLoad in
            shouldLoad ? self.showIndicator() : self.hideIndicator()
        }
        viewModel.enableEnteraction.bindAndFire{ isEnabled in
            self.view.isUserInteractionEnabled = isEnabled
        }
        viewModel.productResponse.bind{ product in
            self.containerView.isHidden = false
            self.productDetails = product
            self.setData()
        }
        viewModel.productStatusResponse.bind{ productStatus in
            self.containerView.isHidden = false
            self.unavailableSwitch.isOn = productStatus.notAvailable == 0 ? false : true
            
        }
        viewModel.updateStatusResponse.bind{ productStatus in
            self.containerView.isHidden = false
            if !productStatus{
                self.unavailableSwitch.isOn = !self.unavailableSwitch.isOn

            }
            
        }
        viewModel.priceDeleted.bindAndFire{ isDeleted in
            if isDeleted {
                self.alert(title: "", message: "priceDeleted".localized(), ActionButtons: [(title: "ok".localized(),style:.Default, alignment: .center, bgColor: #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1), textColor: .white, handler:{(action) in }) ])
            }
        }
        
        viewModel.additionDeleted.bindAndFire{ isDeleted in
            if isDeleted {
                self.alert(title: "", message: "additionDeleted".localized(), ActionButtons: [(title: "ok".localized(),style:.Default, alignment: .center, bgColor: #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1), textColor: .white, handler:{(action) in }) ])
            }
        }
        viewModel.optionDeleted.bindAndFire{ isDeleted in
            if isDeleted {
                self.alert(title: "", message: "optionDeleted".localized(), ActionButtons: [(title: "ok".localized(),style:.Default, alignment: .center, bgColor: #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1), textColor: .white, handler:{(action) in }) ])
            }
        }
        viewModel.offerAdded.bindAndFire{ isDeleted in
            if isDeleted {
                self.alert(title: "", message: "offerAdded".localized(), ActionButtons: [(title: "ok".localized(),style:.Default, alignment: .center, bgColor: #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1), textColor: .white, handler:{(action) in }) ])
            }
        }
    }
    
    func setData() {
        guard let product = productDetails  else {
            return
        }
        productImageView.kf.setImage(with: URL(string: product.profilePath ?? ""), placeholder: #imageLiteral(resourceName: "ic_product_default"), options: [], progressBlock: nil) { (image, _, _, _) in
            if let img = image {self.productImage = img}
        }
        if let translateArr = product.translates, translateArr.count >= 2 {
            nameEnContainerViewHeight.constant = getTextHeight(translateArr[0].productDesc ?? "")
            nameArContainerViewHeight.constant = getTextHeight(translateArr[1].productDesc ?? "")
            productNameEnLabel.text = translateArr[0].productName
            productNameArLabel.text = translateArr[1].productName
            productDescEnLabel.text = translateArr[0].productDesc
            productDescArLabel.text = translateArr[1].productDesc
        }
        
        if let priceArr = product.prices {
            self.priceArr = priceArr
            self.priceContainerViewHeight.constant = CGFloat(priceArr.count * 50)
            priceTableView.reloadData()
        }
        
        if let additionArr = product.addition {
            self.additionArr = additionArr
            addOnsContainerViewHeight.constant = CGFloat(additionArr.count * 60)
            addOnsTableView.reloadData()
        }
        if let optionsArr = product.options {
            self.optionsArr = optionsArr
            optionsTableViewHeight.constant = CGFloat(optionsArr.count * 60)
            optionsTableView.reloadData()
        }
        
    }
    
    func getTextHeight(_ text: String) -> CGFloat {
        return text.height(withConstrainedWidth: view.frame.size.width - 40, font: UIFont(name: "Roboto-Regular", size: 15)!) + 25
    }
    
    func updateProduct()  {
        if let vc = storyboard?.instantiateViewController(withIdentifier: AddProductViewController.identifier) as? AddProductViewController {
            vc.productDetails = productDetails
            vc.isUpdate = true
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func editProductButtonAction(_ sender: UIButton) {
        updateProduct() 
    }
    
    
    @IBAction func addPriceButtonAction(_ sender: UIButton) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: AddProductSizeViewController.identifier) as? AddProductSizeViewController, let product = productDetails, let productId = product.productId {
            vc.productId = productId
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func unavalibleCheckBtnAction(_ sender: UISwitch) {
        viewModel.updateProductStatus(productId: productId ?? 0, available: sender.isOn ? 1 : 0)
    }
    @IBAction func newAddOnsButtonAction(_ sender: UIButton) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: ProductAdditionViewController.identifier) as? ProductAdditionViewController, let product = productDetails, let productId = product.productId {
            vc.productId = productId
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func newOptionsBtnAction(_ sender: Any) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: AddOptionViewController.identifier) as? AddOptionViewController, let product = productDetails, let productId = product.productId {
            vc.productId = productId
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true, completion: nil)
        }
        
    }
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func showDiscountPopup(priceId: Int) {
        discountView = AddDiscountView(discount: DiscountRequest(priceId: priceId, productId: productId ?? 0), delegate: self, isUpdate: false)
        view.addSubview(discountView)
        view.setXIBConstraints(xib: discountView)
    }
    
    @objc func removePopupView(_ popup: ProductPopups) {
        if popup == .options, optionsView != nil {
            optionsView.removeFromSuperview()
        } else if popup == .discount, discountView != nil {
            discountView.removeFromSuperview()
        } else {
            optionsView?.removeFromSuperview()
            discountView?.removeFromSuperview()
        }
    }
}

extension ProductDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == priceTableView {
            return priceArr.count
        }else if tableView == optionsTableView{
            return optionsArr.count

            }else {
            return additionArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == priceTableView {
            if let cell = tableView.dequeueReusableCell(withIdentifier: ProductDetailsPriceCell.identifier, for: indexPath) as? ProductDetailsPriceCell {
                cell.delegate = self
                cell.configure(price: priceArr[indexPath.row])
                return cell
            }
        }else if tableView == optionsTableView{
            if let cell = tableView.dequeueReusableCell(withIdentifier: ProductDetailsOptionsCell.identifier, for: indexPath) as? ProductDetailsOptionsCell {
                cell.delegate = self
                cell.configure(option: optionsArr[indexPath.row])
                return cell
            }
            }else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: ProductDetailsAddOnsCell.identifier, for: indexPath) as? ProductDetailsAddOnsCell {
                cell.delegate = self
                cell.configure(addOns: additionArr[indexPath.row])
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
extension ProductDetailsViewController: ProductDetailsCellDelegate {
    
    
    func openOptions(cell: ProductDetailsPriceCell) {
        removePopupView(.options)
        if let indexPath = priceTableView.indexPath(for: cell) {
        
        optionsView = PriceOptionsView(price: priceArr[indexPath.row], delegate: self)
        view.addSubview(optionsView)
        optionsView.translatesAutoresizingMaskIntoConstraints = false
        optionsView.heightAnchor.constraint(equalToConstant: 120).isActive = true
        optionsView.widthAnchor.constraint(equalToConstant: 125).isActive = true
        optionsView.trailingAnchor.constraint(equalTo: cell.optionsButton.leadingAnchor, constant: 15).isActive = true
        optionsView.topAnchor.constraint(equalTo: cell.optionsButton.topAnchor, constant: -8).isActive = true
        }
    }
    
    func deleteProductAddOns(additionId: Int) {
        viewModel.deleteAddition(productId: productDetails.productId ?? 0, additionId: additionId)
    }
    
    func editProductAddOns(addition: ProductAddition) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: ProductAdditionViewController.identifier) as? ProductAdditionViewController, let product = productDetails, let productId = product.productId {
            vc.productId = productId
            vc.isUpdate = true
            vc.addition = addition
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true, completion: nil)
        }
    }
    
    func deleteProductOptions(optionId: Int) {
        viewModel.deleteOption(productId: productDetails.productId ?? 0, optionId: optionId)
    }
    func editProductOptions(option: ProductOptions) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: AddOptionViewController.identifier) as? AddOptionViewController, let product = productDetails, let productId = product.productId {
            vc.productId = productId
            vc.isUpdate = true
            vc.option = option
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true, completion: nil)
        }
    }
    
}

extension ProductDetailsViewController: AddDiscountViewDelegate {
    func saveDiscount(discount: DiscountRequest) {
        viewModel.saveDiscount(productId: discount.productId, priceId: discount.priceId, discount: discount.discountValue, startDate: discount.startDate, endDate: discount.endDate)
    }
    
    func cancelAction() {
        removePopupView(.discount)
    }
    
    func showError(message: String) {
        self.alert(title: "", message: message, ActionButtons: [(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
    }
    
}

extension ProductDetailsViewController: PriceOptionsViewDelegate {
    func editPrice(price: ProductDetailsPrice) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: AddProductSizeViewController.identifier) as? AddProductSizeViewController, let product = productDetails, let productId = product.productId {
            vc.productId = productId
            vc.isUpdate = true
            vc.price = price
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true, completion: nil)
        }
        removePopupView(.options)
    }
    
    func deletePrice(price: ProductDetailsPrice) {
        viewModel.deletePrice(productId: productDetails.productId ?? 0, priceId: price.priceId ?? 0)
        removePopupView(.options)
    }
    
    func discountPrice(price: ProductDetailsPrice) {
        showDiscountPopup(priceId: price.priceId ?? 0)
        removePopupView(.options)
    }
    
}


