//
//  DetailsTableViewCell.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/27/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

protocol ProductDetailsCellDelegate: class {
    func editProductAddOns(addition: ProductAddition)
    func deleteProductAddOns(additionId: Int)
    func editProductOptions(option: ProductOptions)
    func deleteProductOptions(optionId: Int)
    func openOptions(cell: ProductDetailsPriceCell)
}

class ProductDetailsAddOnsCell: UITableViewCell {
    static let identifier = "ProductDetailsAddOnsCell"
    weak var delegate: ProductDetailsCellDelegate?
    var addition: ProductAddition!
    
    @IBOutlet weak var nameEnLabel: UILabel!
    @IBOutlet weak var nameArLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    func configure(addOns: ProductAddition) {
        self.addition = addOns
        nameEnLabel.text = addOns.translate?[0].productAdditionName ?? ""
        nameArLabel.text = addOns.translate?[1].productAdditionName ?? ""
        priceLabel.text = "\(addOns.productAdditionPrice ?? "") \("aed".localized())"
    }
        
    @IBAction func editAddOnsButtonAction(_ sender: UIButton) {
        delegate?.editProductAddOns(addition: addition)
    }
    
    @IBAction func deleteAddOnsButtonAction(_ sender: UIButton) {
        delegate?.deleteProductAddOns(additionId: addition.productAdditionId ?? 0)
    }
    
}
class ProductDetailsOptionsCell: UITableViewCell {
    static let identifier = "ProductDetailsOptionsCell"
    weak var delegate: ProductDetailsCellDelegate?
    var option: ProductOptions!
    
    @IBOutlet weak var nameEnLabel: UILabel!
    @IBOutlet weak var nameArLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    func configure(option: ProductOptions) {
        self.option = option
        nameEnLabel.text = option.tranlation?[0].name ?? ""
        nameArLabel.text = option.tranlation?[1].name ?? ""
        priceLabel.text = "\(option.price ?? 0.00) \("aed".localized())"
    }
        
    @IBAction func editAddOnsButtonAction(_ sender: UIButton) {
        delegate?.editProductOptions(option: option)
    }
    
    @IBAction func deleteAddOnsButtonAction(_ sender: UIButton) {
        delegate?.deleteProductOptions(optionId:  option.id ?? 0)
    }
    
}

class ProductDetailsPriceCell: UITableViewCell {
    static let identifier = "ProductDetailsPriceCell"
    weak var delegate: ProductDetailsCellDelegate?
    var price: ProductDetailsPrice!
    
    @IBOutlet weak var nameEnLabel: UILabel!
    @IBOutlet weak var nameArLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var optionsButton: UIButton!
    
    func configure(price: ProductDetailsPrice) {
        self.price = price
        nameEnLabel.text = price.translate?[0].productSize ?? ""
        nameArLabel.text = price.translate?[1].productSize ?? ""
        priceLabel.text = "\(price.productPrice ?? "") \("aed".localized())"
    }
    
    @IBAction func optionsButtonAction(_ sender: UIButton) {
        delegate?.openOptions(cell: self)
    }
}
