//
//  ProductDetailsViewModel.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/27/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class ProductDetailsViewModel: BaseViewModel {
    let productManager: ProductManager
    var productResponse: Dynamic<ProductDetails> = Dynamic(ProductDetails())
    var productStatusResponse: Dynamic<ProductStatus> = Dynamic(ProductStatus())
    var updateStatusResponse: Dynamic<Bool> = Dynamic(false)

    var enableEnteraction: Dynamic<Bool> = Dynamic(true)
    var priceDeleted: Dynamic<Bool> = Dynamic(false)

    var additionDeleted: Dynamic<Bool> = Dynamic(false)
    var optionDeleted: Dynamic<Bool> = Dynamic(false)

    var offerAdded: Dynamic<Bool> = Dynamic(false)
    
    init(router: Router, productManager: ProductManager) {
        self.productManager = productManager
        super.init(router: router, isLoading: false)
    }
    
    func getProduct(productId: Int) {
        guard let shopId = checkForShopId() else  {
            return
        }
        enableEnteraction.value = false
        startLoading()
        productManager.getProduct(shopId: shopId, productId: productId, complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
            if let result = response, let productResponse = result.data {
                self.productResponse.value = productResponse
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
    func getProductStatus(productId: Int) {
        guard let shopId = checkForShopId() else  {
            return
        }
        enableEnteraction.value = false
        startLoading()
        productManager.getProductStatus(shopId: shopId, productId: productId, complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
            if let result = response, let productStatusResponse = result.data {
                self.productStatusResponse.value = productStatusResponse
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
    func updateProductStatus(productId: Int, available: Int) {
        guard let shopId = checkForShopId() else  {
            return
        }
        enableEnteraction.value = false
        startLoading()
        productManager.updateProductStatus(shopId: shopId, productId: productId,available: available, complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
            if let result = response, let updateStatus = result.code == 200 ? true : false{
                self.updateStatusResponse.value = updateStatus
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
    func deletePrice(productId: Int, priceId: Int) {
        enableEnteraction.value = false
        startLoading()
        productManager.deleteProductPrice(productId: productId, priceId: priceId, complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
            if let result = response, let status = result.status, status == 1 {
                self.priceDeleted.value = true
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
    
    func deleteAddition(productId: Int, additionId: Int) {
        enableEnteraction.value = false
        startLoading()
        productManager.deleteProductAddition(productId: productId, additionId: additionId, complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
            if let result = response, let status = result.status, status == 1 {
                self.additionDeleted.value = true
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
    func deleteOption(productId: Int, optionId: Int) {
        enableEnteraction.value = false
        startLoading()
        productManager.deleteProductOption(productId: productId, optionId: optionId, complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
            if let result = response, let status = result.status, status == 1 {
                self.optionDeleted.value = true
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
    
    func saveDiscount(productId: Int, priceId: Int, discount: Int,
        startDate: String, endDate: String) {
        guard let shopId = checkForShopId() else  {
            return
        }
        enableEnteraction.value = false
        startLoading()
        productManager.saveOffer(shopId: shopId, productId: productId, priceId: priceId, discount: discount, startDate: startDate, endDate: endDate, complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
            if let result = response, let status = result.status, status == 1 {
                self.offerAdded.value = true
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
    
}


