//
//  ProductCollectionCell.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/18/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

protocol ProductCellDelegate: class {
    func deleteProduct(productId: Int)
    func openOffers(product: Product)
}

class ProductCollectionCell: UICollectionViewCell {
    static let identifier = "ProductCollectionCell"
    weak var delegate: ProductCellDelegate?
    var product: Product!
    
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var priceBeforeLabel: UILabel!
    @IBOutlet weak var priceAfterLabel: UILabel!
    @IBOutlet weak var imageContainerView: RoundView!
    @IBOutlet weak var containerView: UIView!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        imageContainerView.dropShadow(opacity: 0.26, offSet: CGSize(width: 4, height: 4), radius: 4)
        containerView.dropShadow(opacity: 0.16, offSet: CGSize(width: 1, height: 1), radius: 1)
    }
    
    func configure(product: Product) {
        self.product = product
        nameLabel.text = product.productName ?? ""
        descLabel.text = product.productDesc ?? ""
        
        productImage.kf.setImage(with: URL(string: product.profilePath ?? ""), placeholder: #imageLiteral(resourceName: "ic_product_default"))
        guard let priceAfter = product.productPrices?[0].productPriceAfter,
            let priceBefore = product.productPrices?[0].productPriceBefore,
        priceAfter != priceBefore
            else {
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\(product.productPrices?[0].productPriceBefore ?? "") \(product.productPrices?[0].currency ?? "")")
               
                priceBeforeLabel.attributedText = attributeString
            priceAfterLabel.text = ""
                return
        }
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\(product.productPrices?[0].productPriceBefore ?? "0")")
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        priceBeforeLabel.attributedText = attributeString
        priceAfterLabel.text = "\(priceAfter) \(product.productPrices?[0].currency ?? "")"
    }
    
    @IBAction func deleteProductButtonAction(_ sender: UIButton) {
        delegate?.deleteProduct(productId: product.productId ?? 0)
    }
    
    @IBAction func offersButtonAction(_ sender: UIButton) {
        delegate?.openOffers(product: product)
    }
}

