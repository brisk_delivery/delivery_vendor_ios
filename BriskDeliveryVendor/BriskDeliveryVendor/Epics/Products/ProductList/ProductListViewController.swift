//
//  ProductListViewController.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/18/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import ESPullToRefresh

protocol ProductListDelegate: class {
    func deleteProduct(productId: Int)
    func didSelectProduct(productId: Int)
    func refreshProducts()
}

class ProductListViewController: UIViewController {
    weak var delegate: ProductListDelegate?
    var deletePopupView: DeletePopupView!
    private var produstsList = [Product]() {
        didSet{
            noProductsView.isHidden = produstsList.count == 0 ? false : true
            collectionView.isHidden = produstsList.count == 0 ? true : false
        }
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var noProductsView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        layout.minimumInteritemSpacing = 2
        collectionView.collectionViewLayout = layout
        collectionView.es.addPullToRefresh {
            self.delegate?.refreshProducts()
        }
    }
    
    func setDataSource(produstsList: [Product]) {
        self.produstsList = produstsList
        collectionView.reloadData()
        noProductsView.isHidden = self.produstsList.count > 0
        collectionView.es.stopPullToRefresh(ignoreDate: true)
    }
    
}

extension ProductListViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return produstsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let product = produstsList[indexPath.row]
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCollectionCell.identifier, for: indexPath) as? ProductCollectionCell {
            cell.configure(product: product)
            cell.delegate = self
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let product = produstsList[indexPath.row]
        delegate?.didSelectProduct(productId: product.productId ?? 0)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width/2) - 18, height: 220)
    }
    
}

extension ProductListViewController: ProductCellDelegate {
    func openOffers(product: Product) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: OffersViewController.identifier) as? OffersViewController {
            vc.product = product
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true, completion: nil)
        }
    }
    
    func deleteProduct(productId: Int) {
        showDeletePopup(productId: productId)
    }
}

extension ProductListViewController: DeletePopupViewDelegate {
    func deleteAction(id: Int) {
        delegate?.deleteProduct(productId: id)
    }
    
    func cancelDeleteAction() {
        if deletePopupView != nil {
            deletePopupView.removeFromSuperview()
        }
    }
    
    func showDeletePopup(productId: Int) {
        deletePopupView = DeletePopupView(idToDelete: productId, confirmationText: "deleteProduct".localized(), delegate: self)
        view.addSubview(deletePopupView)
        view.setXIBConstraints(xib: deletePopupView)
    }
}
