//
//  MainProductsViewModel.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/18/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation
import Localize_Swift

class MainProductsViewModel: BaseViewModel {
    let productManager: ProductManager
    var productResponse: Dynamic<[Product]> = Dynamic([Product]())
    var enableEnteraction: Dynamic<Bool> = Dynamic(true)
    
    init(router: Router, productManager: ProductManager) {
        self.productManager = productManager
        super.init(router: router, isLoading: false)
    }
    
    func getProducts() {
        guard let shopId = checkForShopId() else  {
            productResponse.value = [Product]()
            return
        }
        if checkPermission() {
            enableEnteraction.value = false
            startLoading()
            //TODO: - adding shop & ctaegory
            productManager.getProducts(shopId: shopId, complation: { response in
                self.enableEnteraction.value = true
                self.stopLoading()
                if let result = response, let productResponse = result.data {
                    self.productResponse.value = productResponse
                }else {
                    self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
                }
            }) { error in
                self.enableEnteraction.value = true
                self.stopLoading()
                self.generalErrorMessage(error: error)
            }
        }
    }
    
    func deleteProduct(productId: Int) {
        enableEnteraction.value = false
        startLoading()
        productManager.deleteProduct(productId: productId, complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
            if let result = response, let status = result.status, status == 1  {
                self.showAlertMessage(title: "", message: "productDeletedMessage".localized(), ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: #colorLiteral(red: 0.4988232255, green: 0.375476867, blue: 0.8019280434, alpha: 1), textColor: .white, handler:{(action) in  self.getProducts()}) ])
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in self.getProducts()}) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
}


