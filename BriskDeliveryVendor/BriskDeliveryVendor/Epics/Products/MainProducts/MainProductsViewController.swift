//
//  ProductsViewController.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/18/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import Localize_Swift

class MainProductsViewController: UIViewController {
    var viewModel: MainProductsViewModel!
    var productsVC: ProductListViewController!
    var produstsList = [Product]()
    var isFilter = true
    var filteredProdusts = [Product]()
    
    var isSearchBarEmpty: Bool {
      return searchTextField.text?.isEmpty ?? true
    }
    
    @IBOutlet weak var searchTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = MainProductsViewModel(router: RouterManager(self), productManager: ProductManager())
        initObservables()
        viewModel.getProducts()
        NotificationCenter.default.addObserver(self, selector: #selector(refreshProducts), name: .refreshProducts, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(setSemantic), name: .languageChanged, object: nil)
        searchTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSemantic()
    }
    
    //MARK:- Observables
    func initObservables() {
        viewModel.isLoading.bindAndFire { shouldLoad in
            shouldLoad ? self.showIndicator() : self.hideIndicator()
        }
        viewModel.enableEnteraction.bindAndFire{ isEnabled in
            self.view.isUserInteractionEnabled = isEnabled
        }
        viewModel.productResponse.bind{ products in
            self.produstsList = products
            self.productsVC.setDataSource(produstsList: products)
        }
    }
    
    @objc func setSemantic() {
        UIView.appearance().semanticContentAttribute = Localize.currentLanguage() == "ar"  ? .forceRightToLeft : .forceLeftToRight
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ProductListSegue", let vc = segue.destination as? ProductListViewController {
            vc.delegate = self
            self.productsVC = vc
        } else if segue.identifier == "ProductDetailsSegue", let vc = segue.destination as? ProductDetailsViewController, let productId = sender as? Int {
            vc.productId = productId
        }
    }
    
    @IBAction func addButtonAction(_ sender: UIButton) {
        guard Global.shopId != nil else  {
            self.alert(title: "", message: "addShopMessage".localized(), ActionButtons: [(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            return
        }
        
        if checkPermission() {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "AddProductNavigation")
            viewController?.modalPresentationStyle = .fullScreen
            present(viewController!, animated: true, completion: nil)
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text!.hasNoCharchters() {
            self.isFilter = false
            self.productsVC.setDataSource(produstsList: produstsList)
            searchTextField.resignFirstResponder()
        }else{
            self.isFilter = true
            self.filterContentForSearchText(textField.text!)
        }
    }
    
}

extension MainProductsViewController: ProductListDelegate {
    func didSelectProduct(productId: Int) {
        self.performSegue(withIdentifier: "ProductDetailsSegue", sender: productId)
    }
    
    func deleteProduct(productId: Int) {
        viewModel.deleteProduct(productId: productId)
    }
    
    @objc func refreshProducts() {
        viewModel.getProducts()
    }
}

extension MainProductsViewController: UITextFieldDelegate {
    
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    
    func filterContentForSearchText(_ searchText: String) {
      filteredProdusts = produstsList.filter { (product: Product) -> Bool in
        return (product.productName ?? "").lowercased().contains(searchText.lowercased())
      }
      self.productsVC.setDataSource(produstsList: filteredProdusts)
    }
}
