//
//  AddProductSizeViewModel.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/29/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class AddProductSizeViewModel: BaseViewModel {
    let productManager: ProductManager
    var enableEnteraction: Dynamic<Bool> = Dynamic(true)
    var categoriesResponse: Dynamic<[Category]> = Dynamic([Category]())
    var priceCreated: Dynamic<Bool> = Dynamic(false)
     var priceUpdated: Dynamic<Bool> = Dynamic(false)
    
    init(router: Router, productManager: ProductManager) {
        self.productManager = productManager
        super.init(router: router, isLoading: false)
    }
    
    func addSize(productId: Int,price: Price) {
        enableEnteraction.value = false
        startLoading()
        productManager.addProductPrice(productId: productId, price: price, complation: { response in
            self.enableEnteraction.value = false
            self.stopLoading()
            if let result = response, result.data != nil {
                self.priceCreated.value = true
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
    
    func updateSize(productId: Int, priceRequestId: Int, price: UpdatePriceRequest) {
        enableEnteraction.value = false
        startLoading()
        productManager.updateProductPrice(priceRequestId: priceRequestId, productId: productId, priceRequest: price, complation: { response in
            self.enableEnteraction.value = false
            self.stopLoading()
            if let result = response, result.data != nil {
                self.priceUpdated.value = true
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
}

