//
//  AddProductSizeViewController.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/29/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import Localize_Swift

class AddProductSizeViewController: UIViewController {
    static let identifier = "AddProductSizeViewController"
    var productId: Int!
    var price: ProductDetailsPrice!
    var isUpdate = false
    var viewModel: AddProductSizeViewModel!
    
    @IBOutlet weak var sizeNameEnTextField: UITextField!
    @IBOutlet weak var sizeNameArTextField: UITextField!
    @IBOutlet weak var sizePriceTextField: UITextField!
    @IBOutlet weak var backImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = AddProductSizeViewModel(router: RouterManager(self), productManager: ProductManager())
        initObservables()
        setData()
        setStyle() 
    }
    
    private func setStyle()  {
        backImage.image = Localize.currentLanguage() == "ar"  ? #imageLiteral(resourceName: "ic_right_back"): #imageLiteral(resourceName: "ic_left_arrow")
    }
    
    func initObservables() {
        viewModel.isLoading.bindAndFire { shouldLoad in
            shouldLoad ? self.showIndicator() : self.hideIndicator()
        }
        viewModel.enableEnteraction.bindAndFire{ isEnabled in
            self.view.isUserInteractionEnabled = isEnabled
        }
        viewModel.priceCreated.bind{ isCreated in
            if isCreated {
                self.alert(title: "", message: "productSizeAddedMessage".localized(),  ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1), textColor: .white, handler:{(action) in
                    self.dismiss(animated: true, completion: nil)
                }) ])
                }
        }
        
        viewModel.priceUpdated.bind{ isCreated in
            if isCreated {
                self.alert(title: "", message: "productSizeUpdatedMessage".localized(),  ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1), textColor: .white, handler:{(action) in
                    self.dismiss(animated: true, completion: nil)
                }) ])
                }
        }
    }
    
    func isValidInputs() -> Bool {
        var result = true
        
        if let nameEN = sizeNameEnTextField.text, nameEN.isEmpty {
            sizeNameEnTextField.layer.borderColor = UIColor.red.cgColor
            result = false
        }
        
        if let nameAr = sizeNameArTextField.text, nameAr.isEmpty {
            sizeNameArTextField.layer.borderColor = UIColor.red.cgColor
            result = false
        }
        
        if let sizeEn = sizePriceTextField.text, sizeEn.isEmpty {
            sizePriceTextField.layer.borderColor = UIColor.red.cgColor
            result = false
        }
        return result
    }
    
    func setData() {
        guard isUpdate, let price = self.price else {
            return
        }
        
        if let stringPrice = price.productPrice, let productPrice =  Float(stringPrice) {
            sizePriceTextField.text = "\(productPrice.cleanValue)"
        } else {
            sizePriceTextField.text = ""
        }
        
        if let translate = price.translate, translate.count >= 2 {
            sizeNameEnTextField.text = translate[0].productSize
            sizeNameArTextField.text = translate[1].productSize
        }
        
    }
    
    func createPrice() -> Price {
        let sizeTranslate = [SizeTranslate(langId: 1, productSize: sizeNameEnTextField.text ?? ""), SizeTranslate(langId: 2, productSize: sizeNameArTextField.text ?? "")]
        
        return Price(productPrice: Int(sizePriceTextField.text ?? "") ?? 0, translate: sizeTranslate)
    }
    
    func createUpdatePrice() -> UpdatePriceRequest {
        let sizeTranslate = [SizeTranslate(langId: 1, productSize: sizeNameEnTextField.text ?? ""), SizeTranslate(langId: 2, productSize: sizeNameArTextField.text ?? "")]
        
        return UpdatePriceRequest(shopId:Global.shopId, productPrice: (sizePriceTextField.text ?? "").toInt(), translate: sizeTranslate)
    }
    
    @IBAction func saveButtonAction(_ sender: UIButton) {
        
        if isValidInputs() {
            if isUpdate, let priceId = price.priceId {
                viewModel.updateSize(productId: productId ?? 0, priceRequestId: priceId, price: createUpdatePrice())
            } else {
                viewModel.addSize(productId: productId ?? 0, price: createPrice())
            }
        }
        
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension AddProductSizeViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.setTextFieldBorder(borderColor: #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1), borderWidth: 1)
    }
}

