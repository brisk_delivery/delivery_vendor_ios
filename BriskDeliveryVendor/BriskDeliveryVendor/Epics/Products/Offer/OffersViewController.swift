//
//  OffersViewController.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 5/17/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit
import Localize_Swift

class OffersViewController: UIViewController {
    static let identifier = "OffersViewController"
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: LocalizeLabel!
    @IBOutlet weak var offersTableview: UITableView!
    @IBOutlet weak var prodcutDescLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var containerView: UIScrollView!
    @IBOutlet weak var imageContainerView: UIView!
    var deletePopupView: DeletePopupView!
    
    var viewModel: OffersViewModel!
    var offers = [Offer]()
    var product: Product!
    var discountView: AddDiscountView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = OffersViewModel(router: RouterManager(self), productManager: ProductManager())
        viewModel.getPrices(productId: product.productId ?? 0)
        initObservables()
        setData()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setStyle()
    }

    private func setStyle()  {
        backImage.image = Localize.currentLanguage() == "ar"  ? #imageLiteral(resourceName: "ic_right_back"): #imageLiteral(resourceName: "ic_left_arrow")
        imageContainerView.dropShadowWithPath(opacity: 0.36, offSet: CGSize(width: 5, height: 5), radius: 5, frame: imageContainerView.bounds)
    }
    
    //MARK:- Observables
    func initObservables() {
        viewModel.isLoading.bindAndFire { shouldLoad in
            shouldLoad ? self.showIndicator() : self.hideIndicator()
        }
        viewModel.enableEnteraction.bindAndFire{ isEnabled in
            self.view.isUserInteractionEnabled = isEnabled
        }
        viewModel.pricesResponse.bind{ prices in
            self.containerView.isHidden = false
            self.self.offersTableview.isHidden = prices.count > 0 ? false: true
            self.offers = prices
            self.offersTableview.reloadData()
        }
        viewModel.offerDeleted.bindAndFire { isDeleted in
            if isDeleted {
                self.viewModel.getPrices(productId: self.product.productId ?? 0)
                self.alert(title: "", message: "offerDeleted".localized(), ActionButtons: [(title: "ok".localized(),style:.Default, alignment: .center, bgColor: #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1), textColor: .white, handler:{(action) in }) ])
            }
        }
        
        viewModel.offerUpdated.bindAndFire { isUpdated in
            if isUpdated {
                self.viewModel.getPrices(productId: self.product.productId ?? 0)
                self.alert(title: "", message: "offerUpdated".localized(), ActionButtons: [(title: "ok".localized(),style:.Default, alignment: .center, bgColor: #colorLiteral(red: 0.4196078431, green: 0.2745098039, blue: 0.7568627451, alpha: 1), textColor: .white, handler:{(action) in }) ])
            }
        }
    }
    
    func setData() {
        guard let product = product else {
            return
        }
        productImage.kf.setImage(with: URL(string: product.profilePath ?? ""), placeholder: #imageLiteral(resourceName: "ic_product_default"))
        productName.text = product.productName ?? ""
        prodcutDescLabel.text = product.productDesc ?? ""
    }

    @IBAction func backButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension OffersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return offers.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: OfferFirstCell.identifier, for: indexPath)
                return cell
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: OfferTableViewCell.identifier, for: indexPath) as? OfferTableViewCell {
                cell.configure(offer: offers[indexPath.row - 1], delegate: self)
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
}

extension OffersViewController: OffersCellDelegate {
    func editOffer(offer: Offer) {
        showDiscountPopup(offer: offer)
    }
    
    func deleteOffer(offerId: Int) {
        showDeletePopup(offerId: offerId)
    }
    
    func showDiscountPopup(offer: Offer) {
        let discount = offer.toDiscount()
        discountView = AddDiscountView(discount: discount, delegate: self, isUpdate: true)
        view.addSubview(discountView)
        view.setXIBConstraints(xib: discountView)
    }
}

extension OffersViewController: AddDiscountViewDelegate {
    func saveDiscount(discount: DiscountRequest) {
        viewModel.updateDiscount(offerId: discount.offerId ?? 0, productId: discount.productId, priceId: discount.priceId, discount: discount.discountValue, startDate: discount.startDate, endDate: discount.endDate)
    }
    
    func cancelAction() {
        if discountView != nil {
            discountView.removeFromSuperview()
        }
    }
    
    func showError(message: String) {
        self.alert(title: "", message: message, ActionButtons: [(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
    }

}

extension OffersViewController: DeletePopupViewDelegate {
    func deleteAction(id: Int) {
        viewModel.deleteOffer(offerId: id)
    }
    
    func cancelDeleteAction() {
        if deletePopupView != nil {
            deletePopupView.removeFromSuperview()
        }
    }
    
    func showDeletePopup(offerId: Int) {
        deletePopupView = DeletePopupView(idToDelete: offerId, confirmationText: "deleteOffer".localized(), delegate: self)
        view.addSubview(deletePopupView)
        view.setXIBConstraints(xib: deletePopupView)
    }
}

