//
//  OffersViewModel.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 5/17/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class OffersViewModel: BaseViewModel {
    let productManager: ProductManager
    var pricesResponse: Dynamic<[Offer]> = Dynamic([Offer]())
    var enableEnteraction: Dynamic<Bool> = Dynamic(true)
    var offerDeleted: Dynamic<Bool> = Dynamic(false)
    var offerUpdated: Dynamic<Bool> = Dynamic(false)
    
    init(router: Router, productManager: ProductManager) {
        self.productManager = productManager
        super.init(router: router, isLoading: false)
    }
    
    func getPrices(productId: Int) {
        enableEnteraction.value = false
        startLoading()
        productManager.getOffers(productId: productId, complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
            if let result = response, let pricesResponse = result.data {
                self.pricesResponse.value = pricesResponse
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
    
    func deleteOffer(offerId: Int) {
        enableEnteraction.value = false
        startLoading()
        productManager.deleteOffer(offerId: offerId, complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
            if let result = response, let status = result.status, status == 1 {
                self.offerDeleted.value = true
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
    
    func updateDiscount(offerId: Int, productId: Int, priceId: Int, discount: Int,
        startDate: String, endDate: String) {
        guard let shopId = checkForShopId() else  {
            return
        }
        enableEnteraction.value = false
        startLoading()
        productManager.updateOffer(offerId: offerId,shopId: shopId, productId: productId, priceId: priceId, discount: discount, startDate: startDate, endDate: endDate, complation: { response in
            self.enableEnteraction.value = true
            self.stopLoading()
            if let result = response, let status = result.status, status == 1 {
                self.offerUpdated.value = true
            }else {
                self.showAlertMessage(title: "", message: response?.message ?? "", ActionButtons:[(title: "ok".localized(),style:.Default, alignment: .center, bgColor: .red, textColor: .white, handler:{(action) in }) ])
            }
        }) { error in
            self.enableEnteraction.value = true
            self.stopLoading()
            self.generalErrorMessage(error: error)
        }
    }
}


