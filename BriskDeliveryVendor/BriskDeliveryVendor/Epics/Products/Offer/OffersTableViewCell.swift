//
//  OffersTableViewCell.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 5/17/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import UIKit

protocol OffersCellDelegate: class {
    func editOffer(offer: Offer)
    func deleteOffer(offerId: Int)
}

class OfferTableViewCell: UITableViewCell {
    static let identifier = "OfferTableViewCell"
    weak var delegate: OffersCellDelegate?
    var offer: Offer!
    
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var offerLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    func configure(offer: Offer, delegate: OffersCellDelegate) {
        self.delegate = delegate
        self.offer = offer
        sizeLabel.text = offer.productSize ?? ""
        priceLabel.text = offer.productPrice ?? ""
        offerLabel.text = "\(offer.discount ?? 0)"
    }
    
    @IBAction func editOfferButtonAction(_ sender: UIButton) {
        delegate?.editOffer(offer: offer)
    }
    
    @IBAction func deleteOfferButtonAction(_ sender: UIButton) {
        delegate?.deleteOffer(offerId: offer.shopOfferId ?? 0)
    }
}

class OfferFirstCell: UITableViewCell {
    static let identifier = "OfferFirstCell"
}
