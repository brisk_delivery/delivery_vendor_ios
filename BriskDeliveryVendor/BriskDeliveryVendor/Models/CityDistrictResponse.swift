//
//  CityDistrictResponse.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/9/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class CityDistrictResponse : Codable {
    
    let code : Int?
    let data : [City]?
    let message : String?
    let status : Int?
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case message = "Message"
        case status = "Status"
    }
}

class City : Codable {
    
    let districts : [District]?
    let id : Int?
    let name : String?
    
    enum CodingKeys: String, CodingKey {
        case districts = "districts"
        case id = "id"
        case name = "name"
    }
    
}

class District : Codable {
    
    let id : Int?
    let name : String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
    }
}
