//
//  ShopStatusResponse.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 7/29/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class ShopStatusResponse : Codable {

    let code : Int?
    let data : ShopStatus?
    let message : String?
    let status : Int?

    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case message = "Message"
        case status = "Status"
    }
}

struct ShopStatus : Codable {

    let busy : Int?
    let delivery : Int?

    enum CodingKeys: String, CodingKey {
        case busy = "busy"
        case delivery  = "deliver_by_shop"

    }
    
}
