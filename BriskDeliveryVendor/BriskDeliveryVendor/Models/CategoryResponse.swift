//
//  CategoryResponse.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/8/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class CategoryResponse : Codable {
    
    let code : Int?
    let data : [Category]?
    let message : String?
    let status : Int?
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case message = "Message"
        case status = "Status"
    }
}

class Category : Codable {
    
    var catId : Int?
    var catImg : String?
    var catName : String?
    
    enum CodingKeys: String, CodingKey {
        case catId = "cat_id"
        case catImg = "cat_img"
        case catName = "cat_name"
    }
}
