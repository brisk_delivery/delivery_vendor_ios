//
//  PaymentTypesResponse.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/11/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class PaymentTypesResponse : Codable {
    
    let code : Int?
    let data : [Payment]?
    let message : String?
    let status : Int?
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case message = "Message"
        case status = "Status"
    }
}

class Payment : Codable, Equatable {
    
    let path : String?
    let paymentMethodId : Int?
    let paymentMethodName : String?
    let paymentType : String?
    
    enum CodingKeys: String, CodingKey {
        case path = "path"
        case paymentMethodId = "payment_method_id"
        case paymentMethodName = "payment_method_name"
        case paymentType = "payment_type"
    }
    
    static func == (lhs: Payment, rhs: Payment) -> Bool {
        return lhs.paymentMethodId == rhs.paymentMethodId
    }
}
