//
//  ProductDetailsResponse.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/27/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class ProductDetailsResponse : Codable {

    let code : Int?
    let data : ProductDetails?
    let message : String?
    let status : Int?

    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case message = "Message"
        case status = "Status"
    }
}

class ProductDetails : Codable {

    var addition : [ProductAddition]?
    var catId : Int?
    var category : Category?
    var isFeatured : Int?
    var prices : [ProductDetailsPrice]?
    var options : [ProductOptions]?

    var productId : Int?
    var profilePath : String?
    var translates : [ProductTranslate]?


    enum CodingKeys: String, CodingKey {
        case addition = "addition"
        case catId = "cat_id"
        case category
        case isFeatured = "is_featured"
        case prices = "prices"
        case productId = "product_id"
        case profilePath = "profile_path"
        case translates = "translates"
        case options = "options"
    }
}

class ProductAddition : Codable {

    var productAdditionId : Int?
    var productAdditionPrice : String?
    var productId : Int?
    var translate : [ProductAdditionTranslate]?


    enum CodingKeys: String, CodingKey {
        case productAdditionId = "product_addition_id"
        case productAdditionPrice = "product_addition_price"
        case productId = "product_id"
        case translate = "translate"
    }
    
}

class ProductAdditionTranslate : Codable {

    var additionCurrency : String?
    var additionTransId : Int?
    var langId : Int?
    var productAdditionName : String?


    enum CodingKeys: String, CodingKey {
        case additionCurrency = "addition_currency"
        case additionTransId = "addition_trans_id"
        case langId = "lang_id"
        case productAdditionName = "product_addition_name"
    }
    
}

class ProductOptions: Codable {
    let categoryId: Int?
    let id: Int?
    let price: Double?
    let tranlation: [ProductOptionsTranslation]?
    let shopId: Int?
   
    enum CodingKeys: String, CodingKey {
        case categoryId = "category_option_id"
        case id = "pro_cat_opt_id"
        case price = "option_price"
        case tranlation = "translate"
        case shopId = "shop_id"
       
        
    }
    init(price: Double, tranlation: [ProductOptionsTranslation], categoryId: Int, shopId: Int) {
        self.price = price
        self.tranlation = tranlation
        self.categoryId = categoryId
        self.id = nil
        self.shopId = shopId
    }
}
class ProductOptionsTranslation: Codable {
    let lang_id: Int?
    let name: String?
    enum CodingKeys: String, CodingKey {
        case lang_id = "lang_id"
        case name = "option_name"
      
    }
    init(lang_id: Int, name: String) {
        self.lang_id = lang_id
        self.name = name
    }
}
class ProductDetailsPrice : Codable, Equatable {
    var priceId : Int?
    var productId : Int?
    var productPrice : String?
    var translate : [ProductDetailsPriceTranslate]?


    enum CodingKeys: String, CodingKey {
        case priceId = "price_id"
        case productId = "product_id"
        case productPrice = "product_price"
        case translate = "translate"
    }
    
    func toPrice() -> Price {
        var sizeTranslateArr = [SizeTranslate]()
        if let translate = self.translate {
        for item in translate {
            sizeTranslateArr.append(SizeTranslate(langId: item.langId ?? 0, productSize: item.productSize ?? ""))
            }}
        return Price(productPrice: Int(self.productPrice ?? "") ?? 0, translate: sizeTranslateArr)
    }
    
    static func == (lhs: ProductDetailsPrice, rhs: ProductDetailsPrice) -> Bool {
        return lhs.priceId == rhs.priceId
    }

}
class ProductDetailsPriceTranslate : Codable {

    var currency : String?
    var productSize : String?
    var langId : Int?
    var priceTransId : Int?

    enum CodingKeys: String, CodingKey {
        case langId = "lang_id"
        case currency = "currency"
        case priceTransId = "price_trans_id"
        case productSize = "product_size"
    }
    
}
