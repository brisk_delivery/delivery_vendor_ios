//
//  ShopResponse.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/16/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class ShopResponse : Codable {
    
    var code : Int?
    var data : Shop?
    var message : String?
    var status : Int?
    
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case message = "Message"
        case status = "Status"
    }
   
}

class Shop : Codable {
    
    var category : Category?
    var caverPath : String?
    var cityId : Int?
    var districtId : Int?
    var paymentMethod : [Int]?
    var profilePath : String?
    var shopId : Int?
    var shopLat : String?
    var shopLng : String?
    var tags : [Int]?
    var timeDelivery : String?
    var translates : [ShopTranslate]?
    var userId : Int?

    enum CodingKeys: String, CodingKey {
        case category
        case caverPath = "caver_path"
        case cityId = "city_id"
        case districtId = "district_id"
        case paymentMethod = "payment_method"
        case profilePath = "profile_path"
        case shopId = "shop_id"
        case shopLat = "shop_lat"
        case shopLng = "shop_lng"
        case tags = "tags"
        case timeDelivery = "time_delivery"
        case translates = "translates"
        case userId = "user_id"
    }
    
}
