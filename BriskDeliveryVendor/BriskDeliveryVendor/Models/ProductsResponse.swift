//
//  ProductsResponse.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/18/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class ProductsResponse : Codable {
    var code : Int?
    var data : [Product]?
    var message : String?
    var status : Int?
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case message = "Message"
        case status = "Status"
    }
}

class Product : Codable {
    var catId: Int?
    var productDesc: String?
    var productId: Int?
    var productName: String?
    var productPrices: [ProductPrice]?
    var profilePath: String?
    var shopId: Int?
    
    enum CodingKeys: String, CodingKey {
        case catId = "cat_id"
        case productDesc = "product_desc"
        case productId = "product_id"
        case productName = "product_name"
        case productPrices = "product_prices"
        case profilePath = "profile_path"
        case shopId = "shop_id"
    }
}

class ProductPrice : Codable {
    var currency: String?
    var discount: Int?
    var productPriceAfter: String?
    var productPriceBefore: String?
    var productPriceId: Int?
    var productSize: String?
    
    enum CodingKeys: String, CodingKey {
        case currency = "currency"
        case discount = "discount"
        case productPriceAfter = "product_price_after"
        case productPriceBefore = "product_price_before"
        case productPriceId = "product_price_id"
        case productSize = "product_size"
    }
}
