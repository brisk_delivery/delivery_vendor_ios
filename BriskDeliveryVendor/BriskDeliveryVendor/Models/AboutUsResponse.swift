//
//  AboutUsResponse.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 5/9/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class AboutUsResponse : Codable {
    let code : Int?
    let data : AboutUsData?
    let message : String?
    let status : Int?
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case message = "Message"
        case status = "Status"
    }
}

class AboutUsData : Codable {
    
    var pageBody : String?
    var pageId : Int?
    var pageTitle : String?
    
    enum CodingKeys: String, CodingKey {
        case pageBody = "page_body"
        case pageId = "page_id"
        case pageTitle = "page_title"
    }
}
