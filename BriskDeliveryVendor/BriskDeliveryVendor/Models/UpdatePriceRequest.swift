//
//  UpdatePriceRequest.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/30/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class UpdatePriceRequest : Codable {
    
    var productPrice : Int?
    var shopId: Int?
    var translate : [SizeTranslate]?
    
    
    enum CodingKeys: String, CodingKey {
        case productPrice = "product_price"
        case translate = "translate"
        case shopId = "shop_id"
    }
    init(shopId: Int, productPrice: Int, translate: [SizeTranslate]) {
        self.shopId = shopId
        self.productPrice = productPrice
        self.translate = translate
    }
}
