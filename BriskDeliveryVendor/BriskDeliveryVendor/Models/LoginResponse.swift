//
//  VerificationCodeResponse.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/4/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class LoginResponse : Codable {
    
    let code : Int?
    let data : AuthUser?
    let message : String?
    let status : Int?
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case message = "Message"
        case status = "Status"
    }
}

class AuthUser : Codable {
    let token : String?
    var user : User?
    
    enum CodingKeys: String, CodingKey {
        case token = "token"
        case user
    }
}

class User : Codable {
    let accountNumber : String?
    let email : String?
    let firstName : String?
    let identificationImage : String?
    let identificationNumber : String?
    let isActive : Int?
    let isVerified : Int?
    let lastName : String?
    let mobileNumber : String?
    let profilePath : String?
    let userId : Int?
    let userType : String?
    var shopNotCompleted: Int?
    var shopId : Int?
    var catID: Int?
    
    enum CodingKeys: String, CodingKey {
        case accountNumber = "account_number"
        case email = "email"
        case firstName = "first_name"
        case identificationImage = "identification_image"
        case identificationNumber = "identification_number"
        case isActive = "is_active"
        case isVerified = "is_verified"
        case lastName = "last_name"
        case mobileNumber = "mobile_number"
        case profilePath = "profile_path"
        case userId = "user_id"
        case userType = "user_type"
        case shopNotCompleted = "shop_not_completed"
        case shopId = "shop_id"
        case catID = "cat_id"

    }
}
