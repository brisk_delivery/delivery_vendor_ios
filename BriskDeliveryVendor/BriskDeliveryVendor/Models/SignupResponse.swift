//
//  SignupResponse.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/2/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation
class SignupResponse : Codable {
    let code: Int?
    let data: SignupResponseData?
    let message: String?
    let status: Int?
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case message = "Message"
        case status = "Status"
    }
}

class SignupResponseData : Codable {
    
    let code : Int?
    let userId : Int?
    
    enum CodingKeys: String, CodingKey {
        case code = "code"
        case userId = "user_id"
    }
}
