//
//  UpdateProfileResponse.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/25/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

struct UpdateProfileResponse : Codable {
    
    let code : Int?
    let data : User?
    let message : String?
    let status : Int?
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case message = "Message"
        case status = "Status"
    }
    
}
