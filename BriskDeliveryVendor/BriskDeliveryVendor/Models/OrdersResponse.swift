//
//  OrdersResponse.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/20/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class OrdersResponse : Codable {
    
    let code : Int?
    let data : [Order]?
    let message : String?
    let status : Int?
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case message = "Message"
        case status = "Status"
    }
}

struct Order : Codable {
    
    let createdAt : String?
    let image : String?
    let orderCode : Int?
    let orderDate : String?
    let orderId : Int?
    let orderPaymentStatus : String?
    let orderStatus : String?
    let orderTime : String?
    let shopId : Int?
    let shopName : String?
    let totalPrice : Double?
    let userMobile : String?
    let userName : String?
    
    
    enum CodingKeys: String, CodingKey {
        case createdAt = "created_at"
        case image = "image"
        case orderCode = "order_code"
        case orderDate = "order_date"
        case orderId = "order_id"
        case orderPaymentStatus = "order_payment_status"
        case orderStatus = "order_status"
        case orderTime = "order_time"
        case shopId = "shop_id"
        case shopName = "shop_name"
        case totalPrice = "total_price"
        case userMobile = "user_mobile"
        case userName = "user_name"
    }
}
