//
//  TimeRequest.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 7/20/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class TimeRequest : Codable {

    var shopId : Int?
    var dayId : Int?
    var timeFrom : String?
    var timeTo : String?

    enum CodingKeys: String, CodingKey {
        case shopId = "shop_id"
        case dayId = "day_id"
        case timeFrom = "time_from"
        case timeTo = "time_to"
    }
    
    init(shopId: Int,dayId: Int, timeFrom: String, timeTo: String) {
        self.shopId = shopId
        self.dayId = dayId
        self.timeFrom = timeFrom
        self.timeTo = timeTo
    }
    
}
