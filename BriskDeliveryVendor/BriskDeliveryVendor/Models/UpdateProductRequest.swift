//
//  UpdateProductRequest.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/29/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class UpdateProductRequest : Codable {

    var catId : Int?
    var shopId : Int?
    var translate : [ProductTranslate]?
    var isFeature : Int?

    enum CodingKeys: String, CodingKey {
        case catId = "cat_id"
        case shopId = "shop_id"
        case translate = "translate"
        case isFeature = "is_featured"
    }
    
    init(shopId: Int, catId: Int, translate: [ProductTranslate], isFeature : Int) {
        self.shopId = shopId
        self.catId = catId
        self.translate = translate
        self.isFeature = isFeature
    }
}
