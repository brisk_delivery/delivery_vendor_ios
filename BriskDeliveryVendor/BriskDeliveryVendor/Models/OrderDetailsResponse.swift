//
//  OrderDetailsResponse.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/21/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class OrderDetailsResponse : Codable {
    
    var code : Int?
    var data : OrderDetails?
    var message : String?
    var status : Int?
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case message = "Message"
        case status = "Status"
    }
}

class OrderDetails : Codable {
    
    var createdAt : String?
    var currency : String?
    var discount : Int?
    var image : String?
    var orderDate : String?
    var orderId : Int?
    var orderPaymentMethod : String?
    var orderPaymentStatus : String?
    var orderRated : Int?
    var orderSelectedDayTime : String?
    var orderStatus : OrderStatus?
    var orderTime : String?
    var paidFromWallet : Int?
    var paymentMethodId : Int?
    var priceAfterDiscount : Double?
    var priceBeforeDiscount : Double?
    var priceDelivery : Int?
    var products : [OrderProduct]?
    var promoCodeValue : Int?
    var remainingToBePaid : Int?
    var salesTax : Int?
    var serviceCharges : Int?
    var shopId : Int?
    var shopImage : String?
    var shopName : String?
    var timeDelivery : Int?
    var timeDown : Int?
    var totalPrice : Double?
    var userAddress : String?
    var userAddressLat : String?
    var userAddressLng : String?
    var userComment : String?
    var userMobile : String?
    var userName : String?
    
    enum CodingKeys: String, CodingKey {
        case createdAt = "created_at"
        case currency = "currency"
        case discount = "discount"
        case image = "image"
        case orderDate = "order_date"
        case orderId = "order_id"
        case orderPaymentMethod = "order_payment_method"
        case orderPaymentStatus = "order_payment_status"
        case orderRated = "order_rated"
        case orderSelectedDayTime = "order_selected_day_time"
        case orderStatus = "order_status"
        case orderTime = "order_time"
        case paidFromWallet = "paid_from_wallet"
        case paymentMethodId = "payment_method_id"
        case priceAfterDiscount = "price_after_discount"
        case priceBeforeDiscount = "price_before_discount"
        case priceDelivery = "price_delivery"
        case products = "products"
        case promoCodeValue = "promo_code_value"
        case remainingToBePaid = "remaining_to_be_paid"
        case salesTax = "sales_tax"
        case serviceCharges = "service_charges"
        case shopId = "shop_id"
        case shopImage = "shop_image"
        case shopName = "shop_name"
        case timeDelivery = "time_delivery"
        case timeDown = "time_down"
        case totalPrice = "total_price"
        case userAddress = "user_address"
        case userAddressLat = "user_address_lat"
        case userAddressLng = "user_address_lng"
        case userComment = "user_comment"
        case userMobile = "user_mobile"
        case userName = "user_name"
    }
    
}

class OrderStatus : Codable {
    
    var color : String?
    var name : String?
    var title : String?
    
    enum CodingKeys: String, CodingKey {
        case color, name, title
    }
    
}

class OrderProduct : Codable {
    
    var addition : [OrderAddition]?
    var catName : String?
    var count : Int?
    var imgPath : String?
    var productId : Int?
    var productImgId : Int?
    var productName : String?
    var productPrice : String?
    var productSize : String?
    var totalPriceAddition : String?
    var options: [OptionCategory]?
    
    
    enum CodingKeys: String, CodingKey {
        case options = "options"
        case addition = "addition"
        case catName = "cat_name"
        case count = "count"
        case imgPath = "img_path"
        case productId = "product_id"
        case productImgId = "product_img_id"
        case productName = "product_name"
        case productPrice = "product_price"
        case productSize = "product_size"
        case totalPriceAddition = "total_price_addition"
    }
    
}

class OrderAddition : Codable {
    
    var productAdditionCount : Int?
    var productAdditionId : Int?
    var productAdditionName : String?
    var productAdditionPrice : String?
    var productId : Int?
    
    enum CodingKeys: String, CodingKey {
        case productAdditionCount = "product_addition_count"
        case productAdditionId = "product_addition_id"
        case productAdditionName = "product_addition_name"
        case productAdditionPrice = "product_addition_price"
        case productId = "product_id"
    }
    
}
class OrderOptions: Codable {
    let id: Int?
    let count: Int?
    let price: Double?
    let currency: String?
    let optionName: String?
    enum CodingKeys: String, CodingKey {
        case id = "order_option_id"
        case price = "option_price"
        case currency = "currency"
        case optionName = "option_name"
        case count = "options_count"
    }
}
