//
//  DiscountRequest.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 5/27/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

struct DiscountRequest {
    var offerId: Int!
    var priceId: Int
    var productId: Int
    var startDate: String!
    var endDate: String!
    var discountValue: Int!
    
    init(priceId: Int, productId: Int) {
        self.priceId = priceId
        self.productId = productId
    }
    
    init(offerId: Int?, priceId: Int, productId: Int, startDate: String, endDate: String, discountValue: Int) {
        self.offerId = offerId
        self.priceId = priceId
        self.productId = productId
        self.startDate = startDate
        self.endDate = endDate
        self.discountValue = discountValue
    }
}
