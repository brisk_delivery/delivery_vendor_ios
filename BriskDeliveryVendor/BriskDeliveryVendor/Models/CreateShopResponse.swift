//
//  CreateShopResponse.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/15/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class CreateShopResponse: Codable {
    
    let code : Int?
    let data : CreateShop?
    let message : String?
    let status : Int?
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case message = "Message"
        case status = "Status"
    }
}

class CreateShop : Codable {
    
    let shopId : Int?
    
    enum CodingKeys: String, CodingKey {
        case shopId = "shop_id"
    }
   
}
