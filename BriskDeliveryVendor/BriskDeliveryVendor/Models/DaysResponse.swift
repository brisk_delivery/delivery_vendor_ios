//
//  DaysResponse.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 7/4/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

struct DaysResponse : Codable {
    let code : Int?
    let data : [ShopDay]?
    let message : String?
    let status : Int?

    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case message = "Message"
        case status = "Status"
    }
}

struct ShopDay : Codable {

    let dayId : Int?
    let dayName : String?

    enum CodingKeys: String, CodingKey {
        case dayId = "day_id"
        case dayName = "day_name"
    }
    
}
