//
//  ProductStatusResponse.swift
//  BriskEatVendor
//
//  Created by Amal Elgalant on 10/28/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class ProductStatusResponse : Codable {

    let code : Int?
    let data : ProductStatus?
    let message : String?
    let status : Int?

    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case message = "Message"
        case status = "Status"
    }
}

class ProductStatus : Codable {

    var notAvailable: Int?


    enum CodingKeys: String, CodingKey {
        case notAvailable = "not_available"
      
    }
}

