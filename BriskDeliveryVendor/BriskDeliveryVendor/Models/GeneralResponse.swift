//
//  GeneralResponse.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/5/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class GeneralResponse : Codable {
    
    let code : Int?
    let status : Int?
    let message : String?
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case status = "Status"
        case message = "message"
    }
}
