//
//  TagsResponse.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/14/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class TagsResponse: Codable {
    
    let code: Int?
    let data: [Tag]?
    let message: String?
    let status: Int?
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case message = "Message"
        case status = "Status"
    }
}

class Tag : Codable, Equatable {
    
    let imgPath : String?
    let tagId : Int?
    let tagName : String?
    
    
    enum CodingKeys: String, CodingKey {
        case imgPath = "img_path"
        case tagId = "tag_id"
        case tagName = "tag_name"
    }

    static func == (lhs: Tag, rhs: Tag) -> Bool {
        return lhs.tagId == rhs.tagId
    }
}
