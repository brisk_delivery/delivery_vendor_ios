//
//  ResendVerificationResponse.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/5/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class ResendVerificationResponse : Codable {
    
    let code : Int?
    let message : String?
    let status : String?
    let verificationCode : Int?
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case message = "Message"
        case status = "Status"
        case verificationCode = "verification_code"
    }

}
