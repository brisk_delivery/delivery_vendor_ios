//
//  WorkingHoursResponse.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 7/4/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

struct WorkingHoursResponse : Codable {

    let code : Int?
    let data : [WorkingHour]?
    let message : String?
    let status : Int?

    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case message = "Message"
        case status = "Status"
    }
    
}

struct WorkingHour : Codable {

    let createdAt : String?
    let dayId : Int?
    let dayName : String?
    let dayNumber : Int?
    let daysTransId : Int?
    let deletedAt : String?
    let langId : Int?
    let shopId : Int?
    let shopTimeId : Int?
    let timeFrom : String?
    let timeTo : String?
    let updatedAt : String?


    enum CodingKeys: String, CodingKey {
        case createdAt = "created_at"
        case dayId = "day_id"
        case dayName = "day_name"
        case dayNumber = "day_number"
        case daysTransId = "days_trans_id"
        case deletedAt = "deleted_at"
        case langId = "lang_id"
        case shopId = "shop_id"
        case shopTimeId = "shop_time_id"
        case timeFrom = "time_from"
        case timeTo = "time_to"
        case updatedAt = "updated_at"
    }

}
