//
//  AddProductRequest.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/19/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class AddProductRequest : Codable {
    
    var addition : Addition?
    var catId : Int?
    var prices : Price?
    var shopId : Int?
    var translate : [ProductTranslate]?
    var isFeature : Int?
    
    enum CodingKeys: String, CodingKey {
        case addition
        case catId = "cat_id"
        case prices
        case shopId = "shop_id"
        case translate = "translate"
        case isFeature = "is_featured"
    }
    
    init(shopId: Int, catId: Int, translate: [ProductTranslate], isFeature: Int) {
        self.shopId = shopId
        self.catId = catId
        self.translate = translate
        self.isFeature = isFeature
    }
}

class Price : Codable {
    
    var productPrice : Int?
    var translate : [SizeTranslate]?
    
    
    enum CodingKeys: String, CodingKey {
        case productPrice = "product_price"
        case translate = "translate"
    }
    init(productPrice: Int, translate: [SizeTranslate]) {
        self.productPrice = productPrice
        self.translate = translate
    }
}

class Addition : Codable {
    
    var productAdditionPrice : Int?
    var translate : [AdditionTranslate]?
    
    enum CodingKeys: String, CodingKey {
        case productAdditionPrice = "product_addition_price"
        case translate = "translate"
    }
    init(productAdditionPrice: Int, translate: [AdditionTranslate]) {
        self.productAdditionPrice = productAdditionPrice
        self.translate = translate
    }
}

class ProductTranslate : Codable {
    
    var langId : Int?
    var productDesc : String?
    var productName : String?
    
    enum CodingKeys: String, CodingKey {
        case langId = "lang_id"
        case productDesc = "product_desc"
        case productName = "product_name"
    }
    
    init(langId: Int, productDesc: String, productName: String) {
        self.langId = langId
        self.productDesc = productDesc
        self.productName = productName
    }
}

class SizeTranslate : Codable {
    
    var langId : Int?
    var productSize : String?

    enum CodingKeys: String, CodingKey {
        case langId = "lang_id"
        case productSize = "product_size"
    }
    
    init(langId: Int, productSize: String) {
        self.langId = langId
        self.productSize = productSize
    }
}
    
class AdditionTranslate : Codable {
    
    var langId : Int?
    var productAdditionName : String?
    
    enum CodingKeys: String, CodingKey {
        case langId = "lang_id"
        case productAdditionName = "product_addition_name"
    }
    
    init(langId: Int, productAdditionName: String) {
        self.langId = langId
        self.productAdditionName = productAdditionName
    }
}
