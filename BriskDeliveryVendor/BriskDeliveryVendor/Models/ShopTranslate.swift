//
//  ShopTranslate.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/16/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class ShopTranslate : Codable {
    
    var shopId : Int?
    var langId : Int?
    var shopName : String?
    var shopDesc : String?
    var shopTransId: Int?
    
    
    
    enum CodingKeys: String, CodingKey {
        case shopId = "shop_id"
        case langId = "lang_id"
        case shopName = "shop_name"
        case shopDesc = "shop_desc"
        case shopTransId = "shop_trans_id"
    }
    
    init(langId: Int,shopName: String, shopDesc: String) {
        self.shopId = 0
        self.langId = langId
        self.shopName = shopName
        self.shopDesc = shopDesc
    }
    
}
