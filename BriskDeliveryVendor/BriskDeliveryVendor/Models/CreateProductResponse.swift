//
//  CreateProductResponse.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 4/18/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class CreateProductResponse : Codable {
    
    let code : Int?
    let data : CreateProduct?
    let message : String?
    let status : Int?
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case message = "Message"
        case status = "Status"
    }
}

class CreateProduct : Codable {
    
    let productId : Int?
    
    enum CodingKeys: String, CodingKey {
        case productId = "product_id"
    }
}
