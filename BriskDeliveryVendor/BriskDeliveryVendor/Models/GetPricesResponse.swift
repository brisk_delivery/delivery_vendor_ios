//
//  GetPricesResponse.swift
//  BriskDeliveryVendor
//
//  Created by huda elhady on 5/17/20.
//  Copyright © 2020 huda.elhady. All rights reserved.
//

import Foundation

class GetOffersResponse : Codable {

    let code : Int?
    let data : [Offer]?
    let message : String?
    let status : Int?

    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case message = "Message"
        case status = "Status"
    }
}

class Offer : Codable {

    let discount : Int?
    let endDate : String?
    let path : String?
    let priceId : Int?
    let productId : Int?
    let productName : String?
    let productPrice : String?
//    let productPriceAfter : String?
    let productSize : String?
    let shopId : Int?
    let shopOfferId : Int?
    let shopOfferName : String?
    let shopOfferText : String?
    let startDate : String?

    enum CodingKeys: String, CodingKey {
        case discount = "discount"
        case endDate = "end_date"
        case path = "path"
        case priceId = "price_id"
        case productId = "product_id"
        case productName = "product_name"
        case productPrice = "product_price"
//        case productPriceAfter = "product_price_after"
        case productSize = "product_size"
        case shopId = "shop_id"
        case shopOfferId = "shop_offer_id"
        case shopOfferName = "shop_offer_name"
        case shopOfferText = "shop_offer_text"
        case startDate = "start_date"
    }
    
    func toDiscount() -> DiscountRequest {
       return DiscountRequest(offerId: self.shopOfferId ?? 0, priceId: self.priceId ?? 0, productId: self.productId ?? 0, startDate: self.startDate ?? "", endDate: self.endDate ?? "", discountValue: self.discount ?? 0)
    }
}
